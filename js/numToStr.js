/** Devuelve cantidad base en forma de cadena de caracteres según argumento numérico real. 
* Ver también: {@link numToStr}, {@link nTSCentavos}, {@link nTSMillones}, {@link nTSCientosDeMiles}, 
* {@link nTSMiles}, {@link nTSCientosX}, {@link nTSCientos} y {@link nTSDecimos}.
* @param num float Argumento numérico real.
* @return string Cantidad base en forma de cadena de caracteres.*/
function nTSBase(num){
	endd=num-Math.floor(num/10)*10;
	end=Math.floor(endd);
	decimales=Math.floor((endd-end)*100);
	switch (end){
		case 1: return "uno"; break;
		case 2: return "dos"; break;
		case 3: return "tres"; break;
		case 4: return "cuatro"; break;
		case 5: return "cinco"; break;
		case 6: return "seis"; break;
		case 7: return "siete"; break;
		case 8: return "ocho"; break;
		case 9: return "nueve"; break;
		case 0: return (num==0)?"cero":""; break;
	}
	return end;
}

//LA FUNCION DECIMOS ES PARA 99 -> 0 Y LLAMA A LA BASE
/** Devuelve cantidad de decimos en forma de cadena de caracteres según argumento numérico real. 
* Ver también: {@link numToStr}, {@link nTSCentavos}, {@link nTSMillones}, {@link nTSCientosDeMiles}, 
* {@link nTSMiles}, {@link nTSCientosX}, {@link nTSCientos} y {@link nTSBase}.
* @param num float Argumento numérico real.
* @return string Cantidad de decimos en forma de cadena de caracteres.*/
function nTSDecimos(num){
	if (num<10)
		return nTSBase(num);	
	ends=num-(Math.floor(num/100)*100);
	end=ends-(num-Math.floor(num/10)*10);
	endd=Math.floor(ends);
	switch (end){
		case 10:
			if(ends<16){
				switch(endd){
					case 10: return "diez"; break;
					case 11: return "once"; break;
					case 12: return "doce"; break;
					case 13: return "trece"; break;
					case 14: return "catorce"; break;
					case 15: return "quince"; break;
				}
			}
			else
				return "diez y "+nTSBase(num);			
			break;
		case 20: return (endd==20)?"veinte":"veinti"+nTSBase(num); break;
		case 30: return (endd==30)?"treinta":"treinta y "+nTSBase(num); break;
		case 40: return (endd==40)?"cuarenta":"cuarenta y "+nTSBase(num); break;
		case 50: return (endd==50)?"cincuenta":"cincuenta y "+nTSBase(num); break;
		case 60: return (endd==60)?"sesenta":"sesenta y "+nTSBase(num); break;
		case 70: return (endd==70)?"setenta":"setenta y "+nTSBase(num); break;
		case 80: return (endd==80)?"ochenta":"ochenta y "+nTSBase(num); break;
		case 90: return (endd==90)?"noventa":"noventa y "+nTSBase(num); break;
		case 0: return nTSBase(num);
	}
}

//LA FUNCION CIENTOS ES PARA 99 -> 0 Y LLAMA A DECIMOS
/** Devuelve cantidad de cientos en forma de cadena de caracteres según argumento numérico real. 
* Ver también: {@link numToStr}, {@link nTSCentavos}, {@link nTSMillones}, {@link nTSCientosDeMiles}, 
* {@link nTSMiles}, {@link nTSCientosX}, {@link nTSDecimos} y {@link nTSBase}.
* @param num float Argumento numérico real.
* @return string Cantidad de cientos en forma de cadena de caracteres.*/
function nTSCientos(num){
	if (num<100) 
		return nTSDecimos(num);
	var ends=num-Math.floor(num/1000)*1000;
	var end=ends-(num-Math.floor(num/100)*100);
	switch (end){
		case 100: return (ends==100)?"cien":"ciento "+nTSDecimos(num); break;
		case 900: return "novecientos "+nTSDecimos(num); break;
		case 700: return "setecientos "+nTSDecimos(num); break;
		case 500: return "quinientos "+nTSDecimos(num); break;
		case 0: return nTSDecimos(num); break;
		default: return nTSBase(end/100)+"cientos "+nTSDecimos(num); break;
	}
}

/** Devuelve cantidad de cientos para los miles que terminan en 1, 
* en forma de cadena de caracteres según argumento numérico real. 
* Ver también: {@link numToStr}, {@link nTSCentavos}, {@link nTSMillones}, {@link nTSCientosDeMiles}, 
* {@link nTSMiles}, {@link nTSCientos}, {@link nTSDecimos} y {@link nTSBase}.
* @param num float Argumento numérico real.
* @return string Cantidad de cientos para los miles que terminan en 1, en forma de cadena de caracteres.*/
function nTSCientosX(num){
	endd=num-Math.floor(num/10)*10;
	ends=endd-Math.floor(endd/10)*10;
	resultado=nTSCientos(num);
	return (ends==1 && endd!=11)?resultado.substring(0,resultado.length-1):resultado;
}

/** Devuelve cantidad de miles en forma de cadena de caracteres según argumento numérico real. 
* Ver también: {@link numToStr}, {@link nTSCentavos}, {@link nTSMillones}, {@link nTSCientosDeMiles}, 
* {@link nTSCientosX}, {@link nTSCientos}, {@link nTSDecimos} y {@link nTSBase}.
* @param num float Argumento numérico real.
* @return string Cantidad de miles en forma de cadena de caracteres.*/
function nTSMiles(num){
	if (num<1000) 
		return nTSCientos(num);
	ends=num-Math.floor(num/10000)*10000;
	end=ends-(num-Math.floor(num/1000)*1000);
	switch (end){
		case 1000: 
			return (ends==1000)?"mil":"mil "+nTSCientos(num);
			break;
		default:
			mil=nTSBase(Math.floor(num/1000));
			return (mil==0)?nTSCientos(num):mil+" mil "+nTSCientos(num);
			break;
	}
}

/** Devuelve cantidad de cientos de miles en forma de cadena de caracteres según argumento numérico real. 
* Ver también: {@link numToStr}, {@link nTSCentavos}, {@link nTSMillones}, {@link nTSMiles}, 
* {@link nTSCientosX}, {@link nTSCientos}, {@link nTSDecimos} y {@link nTSBase}.
* @param num float Argumento numérico real.
* @return string Cantidad de cientos de miles en forma de cadena de caracteres.*/
function nTSCientosDeMiles(num){
	if (num<10000) 
		return nTSMiles(num);
	var ends=Math.floor((num-Math.floor(num/1000000)*1000000)/1000);
	return nTSCientosX(ends)+" mil "+nTSCientos(num);1000005
}

/** Devuelve cantidad de millones en forma de cadena de caracteres según argumento numérico real. 
* Ver también: {@link numToStr}, {@link nTSCentavos}, {@link nTSCientosDeMiles}, {@link nTSMiles}, 
* {@link nTSCientosX}, {@link nTSCientos}, {@link nTSDecimos} y {@link nTSBase}.
* @param num float Argumento numérico real.
* @return string Cantidad de millones en forma de cadena de caracteres.*/
function nTSMillones(num){
	if (num<Math.pow(10,6)) 
		return nTSCientosDeMiles(num);
	var ends=Math.floor(num/Math.pow(10,6));
	var end=ends-Math.floor(ends/10)*10;
	resultado=nTSCientosDeMiles(ends);
	if (end==1) {
		parcial=resultado.substring(0,resultado.length-1);
		return (ends<2)?parcial+" millon "+nTSCientosDeMiles(num):parcial+" millones "+nTSCientosDeMiles(num);
	}
	return resultado+" millones "+nTSCientosDeMiles(num);
}

/** Devuelve cantidad de centavos según argumento numérico real. 
* Ver también: {@link numToStr}, {@link nTSMillones}, {@link nTSCientosDeMiles}, {@link nTSMiles}, 
* {@link nTSCientosX}, {@link nTSCientos}, {@link nTSDecimos} y {@link nTSBase}.
* @param num float Argumento numérico real.
* @return string Cantidad de centavos.*/
function nTSCentavos(num){
	return Math.floor(num*100)-Math.floor(num)*100;
}

/** Devuelve cantidad numérica en forma de cadena de caracteres según argumento numérico real. 
* Ver también: {@link nTSCentavos}, {@link nTSMillones}, {@link nTSCientosDeMiles}, {@link nTSMiles}, 
* {@link nTSCientosX}, {@link nTSCientos}, {@link nTSDecimos} y {@link nTSBase}.
* @param num float Argumento numérico real.
* @return string Cantidad numérica en forma de cadena de caracteres.*/
function numToStr(num){
	return (nTSMillones(num)+" con "+nTSCentavos(num)+" cts.").toUpperCase();
}
