var okey = () => { 
    hide('id_div_background_info', 0);
    hide('id_div_info', 0);
    document.getElementById('id_div_group_info').parentNode.removeChild(document.getElementById('id_div_group_info'));    
}

var alerts = ( aMsjInfo, titleInfo = 'Mensaje(s)') => {
    this.divGroup = () => {
        this.divBackground = () => {
            let obj = document.createElement('div');
            obj.id='id_div_background_info';
            obj.setAttribute('class','fondo_opaco_information');
            obj.setAttribute('style','display: none');
            return obj;
        }
        this.divModalWindow = () => {
            this.tableInfo = () => {
                this.objTh = () => {
                    let obj=document.createElement('th');
                    let nodeTh=document.createTextNode(titleInfo);
                    obj.appendChild(nodeTh);
                    return obj;
                }
                this.objTr = (msj) => {
                    this.objTd = () => {
                        let obj = document.createElement('td');
                        obj.setAttribute('align','center'); 
                        obj.textContent = msj;
                        return obj;
                    }
                    let obj = document.createElement('tr');                        
                    obj.appendChild(objTd());
                    return obj; 
                }
                let obj=document.createElement('table');
                obj.setAttribute('class','tabla_grid');
                obj.setAttribute('align','center');
                obj.appendChild(objTh());
                if (!Array.isArray(aMsjInfo)) {
                    obj.appendChild(objTr(aMsjInfo));
                    obj.rows[0].className='losnone';
                } else {                
                    let leng = aMsjInfo.length;
                    for (let i = 0; i < leng; i++) {
                        obj.appendChild(objTr(aMsjInfo[i]));
                        obj.rows[i].className=((i % 2) == 0)?'losnone':'lospare';
                    }
                }
                return obj;
            }
            this.acceptButton = () => {
                this.objDiv = () => {
                    this.objBut = () => {
                        let obj=document.createElement('button');
                        obj.setAttribute('type','button');
                        obj.setAttribute('onClick','okey()');
                        obj.textContent="OK";
                        return obj;
                    }
                    let obj=document.createElement('div');
                    obj.setAttribute('align','center');
                    obj.setAttribute('class','botonera');
                    obj.appendChild(objBut());
                    return obj;
                }
                return objDiv();
            }
            let obj=document.createElement('div');
            obj.id='id_div_info';
            obj.setAttribute('class','ventana_modal_information');
            obj.setAttribute('style','display: none;');
            obj.appendChild(tableInfo());
            obj.appendChild(acceptButton())
            return obj;
        }
        let obj=document.createElement('div');
        obj.id = 'id_div_group_info'; 
        obj.appendChild(divBackground());
        obj.appendChild(divModalWindow());
        return obj;
    }
    let obj = document.getElementsByTagName('body')[0];
    obj.appendChild(divGroup());
    show('id_div_background_info', 0);
    show('id_div_info', 0);
}


   
