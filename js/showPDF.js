var showPDF = (oPdfHash, timeSleep = 500) =>
{
    "mode_stric";
    this.removeWindows = () =>
    { 
        var func = () => 
        {
            eDivGroup.parentNode.removeChild(eDivGroup);
            clearInterval(numInterval);
        }
        hide(eDivGroup.id, timeSleep);
        var numInterval = setInterval(func, timeSleep);
    }

    var eObject = document.createElement("object");
    eObject.setAttribute("data", 'data:application/pdf;base64,'+oPdfHash);
    eObject.setAttribute("class", "objeto");
    eObject.setAttribute("type", "application/pdf");
    var eDivObject=document.createElement("div");
    eDivObject.setAttribute("class", "ventana_modal_pdf");
    eDivObject.appendChild(eObject); 

    var eImgClose=document.createElement("img");
    eImgClose.setAttribute("class", "imgClose");
    eImgClose.setAttribute("src" , "../../../" + appOrg + "/img/cross_white.png");
    eImgClose.setAttribute("onclick", "removeWindows();");
    var eDivBackground=document.createElement("div");
    eDivBackground.setAttribute("class", "fondo_opaco_pdf");
    eDivBackground.appendChild(eImgClose);

    var eDivGroup=document.createElement("div");
    eDivGroup.style.display = "none";
    eDivGroup.id = "id_div_group";
    eDivGroup.appendChild(eDivBackground);
    eDivGroup.appendChild(eDivObject); 
    document.getElementsByTagName("body")[0].appendChild(eDivGroup);

    show( eDivGroup.id, timeSleep );

}

