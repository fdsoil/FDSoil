/** Barra de Progreso. 
* Función auxiliar que es implementada por {@link custonLabel}. Para esta función se necesita de la librería JQuery.js*/
function barraDeProgreso(){
    $(function() {$( "#progressbar" ).progressbar({ value: false });
    $("button").on("click", function(event) {
        var target=$( event.target), progressbar=$( "#progressbar"), progressbarValue=progressbar.find( ".ui-progressbar-value");
        if ( target.is( "#numButton" ) ) { 
            progressbar.progressbar( "option", { value: Math.floor( Math.random()*100)});
        }
        else if ( target.is( "#colorButton")) {
            progressbarValue.css({"background":'#'+Math.floor(Math.random()*16777215).toString(16)});
        }
        else if ( target.is( "#falseButton")) {
            progressbar.progressbar( "option", "value", false );
        }
    });
}); 
}

/** Personalizar Etiqueta para la Barra de Progreso.
* Ver también: {@link barraDeProgreso}.
* @param valor integer Valor de la barra de progreso.
* @param msj1 string Mensaje que se quiere que aparesca mientras carga la barra de progreso.
* @param msj2 string Mensaje que se quiere que aparesca cuando termina de cargar la barra de progreso.*/
function custonLabel(valor, msj1, msj2 ){
    var valor=valor-1;//Hacer esto cuando no se use la línea de código:  ->    progressbar.progressbar( "value", valor );
    $(function() {
        var progressbar = $( "#progressbar" ), progressLabel = $( ".progress-label" );
        progressbar.progressbar({ 
            value: false, change: function() {
                //progressLabel.text( progressbar.progressbar( "value" ).toString() + "%" );
                progressLabel.text( msj1+progressbar.progressbar( "value" ).toString() + "%" );
            },
            complete: function() {
                //progressLabel.text( "Completado!" );     
                progressLabel.text( msj2 );    
            }
        });
        function progress() {
            var val = progressbar.progressbar( "value" ) || 0;
            progressbar.progressbar( "value", val + 1 );
            //progressbar.progressbar( "value", valor );
            if ( val < valor ) {
                setTimeout( progress, 0 );
            }
        }
        setTimeout( progress, 0 );
        
    });
}
