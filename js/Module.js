var Module = (function(){
    var modules = {};
    return {
        create: function(name, credit) {
            if(modules[name] === undefined) {
                modules[name] = {};
            }

            modules[name].credit = function(){
                return credit;
            }
        },
        append: function(name, module) {
            if(modules[name] === undefined) {
                throw 'Module not exists';
            }

            for(var k in module) {
                modules[name][k] = module[k];
            }
        },
        get: function(name) {
            if(modules[name] === undefined) {
                throw 'Module not exists';
            }

            return modules[name];
        }
    }
})();

/*
Module.create('Table', {
    name: 'Table',
    description: 'Módulo que contiene métodos para trabajar con tablas',
    version: '1.0.0'
});

Module.append('Table', {
    addEventListenerRowsGroup: function (id, namespace) {
        let tbl = document.getElementById(id);
        let rows = tbl.tBodies[0].rows;
        let lenRows = rows.length;
        for (let i=0; i<lenRows;i++){
            let aImg = rows[i].cells[ rows[i].cells.length-1 ].getElementsByTagName('img');
            aImg[0].addEventListener("click", function() { 
                namespace.valEdit(aImg[0]);
                valBtnClose();
            });
            aImg[1].addEventListener("click", function() { 
                namespace.rqsRemove(rows[i].id);
                valBtnClose();
            });
        }
    }
});

Module.append('Table', {
    deleteAllRowsTable: function (idTable,limit) {
       for(var i = document.getElementById(idTable).rows.length; i > limit;i--){
           document.getElementById(idTable).deleteRow(i -1);
        }
    }
});

Module.append('Table', {
    paintTRsClearDark: function (idTable) {
        var objTable= document.getElementById(idTable);
    if (objTable.rows.length == 1)
            return false;
    for (rowIndex = 1; rowIndex < objTable.rows.length; rowIndex++) 
            objTable.rows[rowIndex].className=((rowIndex% 2) == 0)?'losnone':'lospare';              
    }
});
*/
