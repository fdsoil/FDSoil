/** Construye un captcha
*
*/

function drawCaptcha()
{
    $.post('../../../'+FDSoil+'/reqs/control/captcha/index.php', {'token':'94a08da1fecbb6e8b46990538c7b50b2'}, function(dataJson) {
        //alert(dataJson.code);
        $('#txtCaptcha').html("<img width='100%' src='"+dataJson.uri+"'>");
        // Guardar datos en el almacén de la sesión actual
        sessionStorage.setItem("cache", dataJson.code);
        /*optional stuff to do after success */
    },'JSON');
}

// Validate the Entered input aganist the generated security code function   
function validCaptcha()
{
    var resp = false;
    var str1 = sessionStorage.getItem("cache");
    var str2 = removeSpaces(document.getElementById('txtInput').value);
    //alert(str1+'---'+str2);
    if (str1 == str2) 
        resp = true; 
    else
        drawCaptcha();
    return resp;      
}

