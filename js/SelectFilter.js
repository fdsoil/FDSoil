var SelectFilter = ( function () {

    let _field = "";
    let _label = "";

    const _createObjInputChk = function (field) {
        const _removeObj = function () {
            const oTr = document.getElementById("tr_" + field);
            oTr.parentNode.removeChild(oTr);
            const objSelelect=document.getElementById("id_select_campos");
            for (let i = 0; i<(objSelelect.length); i++){
                if (objSelelect[i].value == field)
                    objSelelect[i].removeAttribute("disabled");
            }
        }
        obj = document.createElement('input');
        obj.type="checkbox"
        obj.id = "chk_" + field;
        obj.checked="checked"
        obj.addEventListener('click', () => { _removeObj(); });
        return obj;
    }

    const _creaTr = function () {
        let obj = document.createElement('tr');
        obj.id = "tr_"+_field;
        return obj;
    }

    const _creaTdChk = function () {
        const obj = document.createElement('td');
        obj.setAttribute("valign", "top");
        const oInput = _createObjInputChk(_field);
        obj.appendChild(oInput);  
        return obj;
    }

    const _creaTdLabel = function () {
        const obj = document.createElement('td');
        obj.setAttribute("valign", "top");
        obj.appendChild(document.createTextNode(_label));
        return obj;
    }
   
    const addTypeInput = function (oJson) {
        const creaTd2 = function () {
                const oInput = document.createElement('input');
                oInput.type="text";
                oInput.id = "id_"+ _field;
                oInput.name = _field;
                oInput.value = "";
                const obj = document.createElement('td');
                obj.appendChild(oInput);
                return obj;
            }            
            _field = oJson.field;
            _label = oJson.label;
            const oTr = _creaTr();
            oTr.appendChild(_creaTdChk());
            oTr.appendChild(_creaTdLabel());
            oTr.appendChild(creaTd2());
            document.getElementById('tab_mi_filtro_aux').appendChild(oTr);
        }

    const addTypeInputMultipleSelect = function (oJson) {
            const creaTdTable = function () {
                const creaTdInput = function () {
                    const creaInput = function () {
                        const obj = document.createElement('input');
                        obj.type="text";
                        obj.id = "id_" + _field; 
                        obj.name = _field;
                        obj.value = "";
                        return obj;
                    }
                    const obj = document.createElement('td');
                    obj.appendChild(creaInput());
                    return obj;
                }
                const creaTdButton = function () {
                    const creaButton = function () {                        
                        const obj = document.createElement('button');
                        obj.id = "id_button_" + _field;
                        obj.name = "button_" + _field;
                        obj.type = "button";
                        obj.textContent = "Agregar";                    
                        obj.setAttribute("onclick",
                            "SelectFilter.insertOption('" + _field + "');"
                            + "getValues(document.getElementById('id_" + _field + "_selected'),"
                            + "document.getElementById('strArr_" + _field + "'));"                             
                        );
                        return obj;
                    }
                    const obj = document.createElement('td');
                    obj.setAttribute("align", "center");
                    obj.appendChild(creaButton());
                    return obj;
                }
                const creaTdSelectMultiple = function () {
                    const creaSelect = function () {
                        const obj = document.createElement('select');
                        obj.id = "id_"+ _field + "_selected";
                        obj.name = _field + "_selected";
                        obj.multiple = "multiple";
                        obj.setAttribute("ondblclick",
                            "SelectFilter.deleteOption('" + _field + "');"
                            + "getValues(document.getElementById('id_" + _field + "_selected'),"
                            + "document.getElementById('strArr_" + _field + "'));"                             
                        );
                        return obj;
                    }
                    const creaInput = function () {
                        let obj  = document.createElement('input');
                        obj.id = "strArr_"+_field;
                        obj.name = "strArr_"+_field;
                        obj.type = "hidden";
                        obj.value = "";
                        return obj;
                    }
                    const obj = document.createElement('td');
                    obj.appendChild(creaSelect());
                    obj.appendChild(creaInput());
                    return obj;
                }
                let aTr = [];
                aTr[0] = document.createElement('tr');
                aTr[0].appendChild(creaTdInput());
                aTr[1] = document.createElement('tr');
                aTr[1].appendChild(creaTdButton());
                aTr[2] = document.createElement('tr');
                aTr[2].appendChild(creaTdSelectMultiple());
                const oTable = document.createElement('table');
                oTable.appendChild(aTr[0]);
                oTable.appendChild(aTr[1]);
                oTable.appendChild(aTr[2]);
                const obj = document.createElement('td');              
                obj.appendChild(oTable);
                return obj;
            }   
            _field = oJson.field;
            _label = oJson.label;
            const oTr = _creaTr();
            oTr.appendChild(_creaTdChk());
            oTr.appendChild(_creaTdLabel());
            oTr.appendChild(creaTdTable());
            document.getElementById('tab_mi_filtro_aux').appendChild(oTr);
        }

    const addTypeInputRange = function (oJson) {
        const creaTdTable = function () {
            const creaTdInput = function () {
                const creaInput = function () {
                    const obj = document.createElement('input');
                    obj.type="text";
                    obj.id = "id_" + _field;
                    obj.name = _field;
                    return obj;
                }
                const obj = document.createElement('td');
                obj.appendChild(creaInput());
                return obj;
            }
            const oTr = _creaTr();
            oTr.appendChild(creaTdInput());
            _field = oJson.field + "_hasta";
            _label = oJson.label + " Hasta";
            oTr.appendChild(_creaTdLabel());
            oTr.appendChild(creaTdInput());
            const oTable = document.createElement('table');
            oTable.appendChild(oTr);
            const obj = document.createElement('td');              
            obj.appendChild(oTable);
            return obj;
        }
        _field = oJson.field;
        const oTr = _creaTr();
        oTr.appendChild(_creaTdChk());
        _field = oJson.field + "_desde";
        _label = oJson.label + " Desde";
        oTr.appendChild(_creaTdLabel());
        oTr.appendChild(creaTdTable());           
        document.getElementById('tab_mi_filtro_aux').appendChild(oTr);
    }

    const addTypeDate = function (oJson) {
        const creaTdTable = function () {
            const creaTdInputScript = function () {
                const creaDiv = function () {
                    const creaInput = function () {
                        const obj = document.createElement('input');
                        obj.type="text";
                        obj.id = "id_" + _field;
                        obj.name = _field;
                        obj.setAttribute("readonly","readonly");
                        return obj;
                    }
                    const creaScript = function () {
                        const obj = document.createElement('script');
                        obj.innerHTML = "Calendar.setup("
                            + "{inputField : 'id_"+_field+"'," 
                            + "ifFormat : '%d/%m/%Y', "
                            + "button : 'id_"+_field+"', "
                            + "align : 'Tr'});";
                        return obj;
                    }
                    const obj = document.createElement('div');
                    obj.appendChild(creaInput());
                    obj.appendChild(creaScript());
                    return obj;
                }
                const obj = document.createElement('td');
                obj.appendChild(creaDiv());
                return obj;
            }
            const oTr = document.createElement('tr');
            oTr.appendChild(creaTdInputScript());
            _field = oJson.field + "_hasta";
            _label = oJson.label + " Hasta";
            oTr.appendChild(_creaTdLabel());
            oTr.appendChild(creaTdInputScript());
            const oTable = document.createElement('table');
            oTable.appendChild(oTr);
            const obj = document.createElement('td');              
            obj.appendChild(oTable); 
            return obj;               
        }
        _field = oJson.field;
        const oTr = _creaTr();
        oTr.appendChild(_creaTdChk());
        _field = oJson.field + "_desde";
        _label = oJson.label + " Desde";
        oTr.appendChild(_creaTdLabel());
        oTr.appendChild(creaTdTable());           
        document.getElementById('tab_mi_filtro_aux').appendChild(oTr);
    }

    addTypeInputRadio = function ( oJson ){
        const creaTd2 = function () {
            const creaInputRadio = function (arr) {
                const obj = document.createElement("input");
                obj.type = "radio";	
                obj.value = arr[0];
                obj.name = _field;
                obj.id = _field + arr[1];
                obj.innerHTML = arr[1];
                return obj
            }
            const creaLabel = function (arr) {
                const obj = document.createElement("label");
                //obj.textContent = arr[1].substr(0,1).toUpperCase() + arr[1].substr(1, arr[1].length).toLowerCase();
                obj.textContent = arr[1];
                return obj;
            }
            const obj = document.createElement('td');
            for ( let i = 0; i < (oJson.arr.length); i++) {
                obj.appendChild(creaInputRadio(oJson.arr[i]));
                obj.appendChild(creaLabel(oJson.arr[i]));
            }
            return obj;
        } 
        _field = oJson.field;
        _label = oJson.label;
        const oTr = _creaTr();
        oTr.appendChild(_creaTdChk());
        oTr.appendChild(_creaTdLabel());
        oTr.appendChild(creaTd2());
        document.getElementById('tab_mi_filtro_aux').appendChild(oTr);
    }

    const addTypeMultipleSelect = function ( oJson ) {
        const SELECT_SIZE = oJson.arr.length > 5 ? 5 : oJson.arr.length;
        const creaTd2 = function () {              
            const creaSelect0 = function () {
                const obj = document.createElement('select');
                obj.setAttribute("style", "width:100%");
                obj.id = "id_" + _field +"_list";
                obj.name = _field + "_list";
                obj.size = SELECT_SIZE;
                obj.multiple = "multiple";
                obj.setAttribute("ondblclick",
                    "move('right', document.getElementById('id_" + _field 
                        + "_list'), document.getElementById('id_" + _field 
                        + "_selected')); getValues(document.getElementById('id_" + _field 
                        + "_selected'), document.getElementById('strArr_" + _field + "'));"                             
                );
                let aOptions = [];
                for ( let i = 0; i < (oJson.arr.length); i++) {
                    aOptions[i] = document.createElement("option");	
                    aOptions[i].value = oJson.arr[i][0];
                    aOptions[i].text = oJson.arr[i][1] ? oJson.arr[i][1] : oJson.arr[i][0];
                    obj.appendChild(aOptions[i]);
                }
                return obj;
            }
            const creaButton0 = function () {
                const obj = document.createElement('button');
                obj.type="button"
                obj.textContent ="--->";
                obj.setAttribute( "onclick", 
                    "move('right', document.getElementById('id_" + _field 
                         + "_list'), document.getElementById('id_" + _field 
                         + "_selected')); getValues(document.getElementById('id_" + _field 
                         + "_selected'), document.getElementById('strArr_" + _field + "'));" 
                );
                return obj;
            }
            const creaButton1 = function () {
                const obj = document.createElement('button');
                obj.type="button"
                obj.textContent ="<---";
                obj.setAttribute( "onclick", 
                    "move('left', document.getElementById('id_" + _field 
                        + "_list'), document.getElementById('id_" +_field
                        + "_selected')); getValues(document.getElementById('id_" + _field
                        + "_selected'), document.getElementById('strArr_" + _field + "'));" 
                );
                return obj;
            }
            const creaSelect1 = function () {
                const obj = document.createElement('select');
                obj.setAttribute("style", "width:100%");
                obj.id = "id_"+ _field + "_selected";
                obj.name = _field + "_selected";
                obj.size = SELECT_SIZE;
                obj.multiple = "multiple";
                obj.setAttribute("ondblclick",
                    "move('left', document.getElementById('id_" + _field 
                        + "_list'), document.getElementById('id_" + _field
                        + "_selected')); getValues(document.getElementById('id_" + _field 
                        + "_selected'), document.getElementById('strArr_" + _field + "'));"                             
                );
                return obj;
            }
            const creaInput = function () {
                const obj = document.createElement('input');
                obj.id = "strArr_"+_field;
                obj.name = "strArr_"+_field;
                obj.type = "hidden";
                obj.value = "";
                return obj;
            }
            const creaTable = function () {
                let aTr = [];
                aTr[0] = document.createElement('td');
                aTr[0].appendChild(creaSelect0());
                aTr[1] = document.createElement('td');
                aTr[1].align="center";
                aTr[1].appendChild(creaButton0());
                aTr[1].appendChild(creaButton1());
                aTr[2] = document.createElement('td');
                aTr[2].appendChild(creaSelect1());
                aTr[2].appendChild(creaInput());
                const oTr = document.createElement('tr');
                oTr.appendChild(aTr[0]);
                oTr.appendChild(aTr[1]);
                oTr.appendChild(aTr[2]);
                const oTable = document.createElement('table');
                oTable.setAttribute("style","width: 100%");
                return oTable.appendChild(oTr);
            }
            const obj = document.createElement('td');
            obj.appendChild(creaTable());
            return obj;
        }
        _field = oJson.field;
        _label = oJson.label;
        const oTr = _creaTr();
        oTr.appendChild(_creaTdChk());
        oTr.appendChild(_creaTdLabel());
        oTr.appendChild(creaTd2());       
        document.getElementById('tab_mi_filtro_aux').appendChild(oTr);
    }

    addTypeSelect = function ( oJson ){
        const creaTd2 = function () {
            const creaSelect = function () {
                const obj = document.createElement('select');
                obj.id = "id_"+ _field;
                obj.name = _field;
                obj.setAttribute("style", "width:100%");
                let aOptions = [];
                let i = 0;
                aOptions[i] = document.createElement("option");	
                aOptions[i].value = "0";
                aOptions[i].text = "Seleccione...";
                obj.appendChild(aOptions[i]);
                for ( let i = 0; i < (oJson.arr.length); i++) {
                    aOptions[i+1] = document.createElement("option");	
                    aOptions[i+1].value = oJson.arr[i][0];
                    aOptions[i+1].text = oJson.arr[i][1] ? oJson.arr[i][1] : oJson.arr[i][0];
                    obj.appendChild(aOptions[i+1]);
                }
                return obj
            }              
            const obj = document.createElement('td');
            obj.appendChild(creaSelect());
            return obj;
        } 
        _field = oJson.field;
        _label = oJson.label;
        const oTr = _creaTr();
        oTr.appendChild(_creaTdChk());
        oTr.appendChild(_creaTdLabel());
        oTr.appendChild(creaTd2());
        document.getElementById('tab_mi_filtro_aux').appendChild(oTr);
    }

    addTypeSelectRango = function ( oJson ){
        const creaTdTable = function () {
            const creaSelect = function (desdeHasta) {
                const obj = document.createElement('select');
                obj.id = "id_"+ _field + "_" + desdeHasta;
                obj.name = _field+ "_" + desdeHasta;
                //obj.setAttribute("style", "width:50%");
                let aOptions = [];
                let i = 0;
                aOptions[i] = document.createElement("option");	
                aOptions[i].value = "0";
                aOptions[i].text = "Seleccione...";
                obj.appendChild(aOptions[i]);
                for ( let i = 0; i < (oJson.arr.length); i++) {
                    aOptions[i+1] = document.createElement("option");	
                    aOptions[i+1].value = oJson.arr[i][0];
                    aOptions[i+1].text = oJson.arr[i][1] ? oJson.arr[i][1] : oJson.arr[i][0];
                    obj.appendChild(aOptions[i+1]);
                }
                return obj
            } 
            const creaTable = function () {
                let aTr = [];
                aTr[0] = document.createElement('td');
                aTr[0].appendChild(creaSelect("desde"));
                aTr[1] = document.createElement('td');
                aTr[1].align="center";
                _label = oJson.label + ' Hasta:';
                aTr[1].appendChild(_creaTdLabel());
                aTr[2] = document.createElement('td');
                aTr[2].appendChild(creaSelect("hasta"));
                const oTr = document.createElement('tr');
                oTr.appendChild(aTr[0]);
                oTr.appendChild(aTr[1]);
                oTr.appendChild(aTr[2]);
                const oTable = document.createElement('table');
                oTable.setAttribute("style","width: 100%");
                return oTable.appendChild(oTr);
            }
            const obj = document.createElement('td');
            obj.appendChild(creaTable());
            return obj;
        } 
        _field = oJson.field;
        _label = oJson.label + ' Desde:';
        const oTr = _creaTr();
        oTr.appendChild(_creaTdChk());
        oTr.appendChild(_creaTdLabel());
        oTr.appendChild(creaTdTable());
        document.getElementById('tab_mi_filtro_aux').appendChild(oTr);
    }

    return {

        deleteOption : function (field) {
            const oSelected = document.getElementById("id_"+ field + "_selected");
            oSelected.remove(oSelected.selectedIndex);
        },

        insertOption : function (field) {
            const option = document.createElement("option");
            const valor = document.getElementById("id_" + field).value;
            if (valor) {
                option.value = valor;
                option.text =  valor;
                const oSelected = document.getElementById("id_"+ field +"_selected");
                oSelected.add(option);
                document.getElementById("id_"+ field).value = '';
            }
        },

        addItem : function (obj) {
            obj.options[obj.selectedIndex].setAttribute("disabled","disabled");
            let oJson = JSON.parse(obj.options[obj.selectedIndex].attributes["ojson"].nodeValue);
            switch (oJson.type) {
                case 'input': 
                addTypeInput(oJson);
                break;
            case 'inputDate':
                addTypeDate(oJson);
                break;
            case 'inputRange': 
                addTypeInputRange(oJson);
                break;
            case 'inputRadio': 
                addTypeInputRadio(oJson);
                break;
            case 'inputSelectMulti': 
                 addTypeInputMultipleSelect(oJson);
                break;
            case 'selectMulti':
                addTypeMultipleSelect(oJson);
                break;
            case 'select':
                addTypeSelect(oJson);
                break;
            case 'selectRango':
                addTypeSelectRango(oJson);
                break;
            }
            obj.value="0";
        }    
    }

}
)();

