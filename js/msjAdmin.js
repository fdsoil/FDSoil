/** Muestra un mensaje administrativo del framework FDSoil según.
* @ valMsj string Posible argumento.
*/
function msjAdmin(valMsj){
    var strMsj;
    if (valMsj == 'C')
        strMsj='El registro fue agregado con éxito.';
    else if(valMsj == 'A')
        strMsj='El registro fue actualizado con éxito.';
    else if(valMsj == 'B')
        strMsj='El registro fue eliminado con éxito.';
    else if(valMsj == 'D')
        strMsj='El registro fue reversado con éxito.';
    else if(valMsj == 'E')
        strMsj='El archivo fue cargado con éxito.';
    else if(valMsj == 'G')
        strMsj='El archivo fue eliminado con éxito.'; 
    else if(valMsj == 'H')
        strMsj='El registro fue cerrado con éxito.'; 
    else if(valMsj == 'T')
        strMsj='Disculpe: El registro está repetido.';
    else if(valMsj == 'U')
        strMsj='Disculpe: Hay registro(s) asociado(s).';
    else if(valMsj == 'Z')
        strMsj='Error de ejecución; notifique al especialista del sistema.';
    else if (valMsj!='' && valMsj!=null)
        strMsj=valMsj;
    else
        strMsj='Error de ejecución desconocido.';
    alert(strMsj,'Mensaje(s)');
}
