/** Valida que la tecla presionada sea un número, una coma o un punto.
* Ver también: {@link acceptTime}, {@link acceptNumTelefono}, {@link IsNumeric}, {@link isInt}, {@link isInteger},
* {@link entero}, {@link isFloat} y {@link flotante}.
* @param e object Evento de presionar una tecla.
* @return boolean Devuelve TRUE si la tecla presionada sea un número, una coma o un punto; 
* de lo contrario devuelve FALSE.*/
function acceptNum(e) {

    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if(tecla == 8)
    {
        return true;
    }
    var patron;
    //patron = /[abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUV WXYZ0123456789]/
    //patron = /\d/; //solo acepta numeros, mejor dicho, no acepta digitos (letras)
    patron = /[0-9,.]/; //solo acepta numeros, puntos y comas
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/** Valida que la tecla presionada sea un número, una guión o dos puntos.
* Ver también: {@link acceptNum}, {@link acceptNumTelefono}, {@link IsNumeric}, {@link isInt}, {@link isInteger},
* {@link entero}, {@link isFloat} y {@link flotante}.
* @param e object Evento de presionar una tecla.
* @return boolean Devuelve TRUE si la tecla presionada sea un número, una guión o dos puntos;
* de lo contrario devuelve FALSE.*/
function acceptTime(e) {

    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if(tecla == 8)
    {
        return true;
    }
    var patron;
    //patron = /[abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUV WXYZ0123456789]/
    //patron = /\d/; //solo acepta numeros, mejor dicho, no acepta digitos (letras)
    patron = /[0-9:]/; //solo acepta numeros, guión y dos puntos.
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/** Valida un Porcentaje.
* @param e object Evento de presionar una tecla.
* @param valor float Monto en base al calculo.
* @return boolean Devuelve TRUE, de lo contrario devuelve FALSE.*/
function porcentaje(e,valor) {
    if(flotante(e)){
        tecla = (document.all) ? e.keyCode : e.which;
        if(tecla == 8 || e.which == 0){
            return true;
        }else{
            num = String.fromCharCode(tecla);
            if(valor + num <=100){
                return true;
            }else{
                return false
            }
        }
    }else{
        return false;
    }
}

/** Valida un número real.
* @param e object Evento de presionar una tecla.
* @param valor float Monto en base al calculo.
* @return boolean Devuelve TRUE, de lo contrario devuelve FALSE.*/
function flotante(e,value) {
    tecla = (document.all) ? e.keyCode : e.which;
      
    if(tecla == 46 || e.which == 0)
    {
        var patron;
        patron = /[.]/;
        
        if(patron.test(value)){
            return false;
        }else{
            return true;
        }      
    }else{
        return entero(e);
    }
}

/** Valida un número entero.
* @param e object Evento de presionar una tecla.
* @return boolean Devuelve TRUE si la tecla presionada representa un número entero, de lo contrario devuelve FALSE.*/
function entero(e) {   
    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if(tecla == 8 || e.which == 0)
    {
        return true;
    }
    var patron;
    //patron = /[abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUV WXYZ0123456789]/
    //patron = /\d/; //solo acepta numeros, mejor dicho, no acepta digitos (letras)
    patron = /[0-9]/; //solo acepta numeros
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/** Valida que la tecla presionada sea un número, una guión o paréntesis.
* Ver también: {@link acceptNum}, {@link acceptTime}, {@link IsNumeric}, {@link isInt}, {@link isInteger},
* {@link entero}, {@link isFloat} y {@link flotante}.
* @param e object Evento de presionar una tecla.
* @return boolean Devuelve TRUE si la tecla presionada sea un número, una guión o paréntesis;
* de lo contrario devuelve FALSE.*/
function acceptNumTelefono(e) {
        
    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if(tecla == 8 || e.which == 0)
    {
        return true;
    }
    var patron;
    //patron = /[abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUV WXYZ0123456789]/
    //patron = /\d/; //solo acepta numeros, mejor dicho, no acepta digitos (letras)
    patron = /[0-9()]/; //solo acepta numeros, puntos y comas
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/** Coloca formato de número real (separadores de miles y decimales de tres (3) dígitos) 
* a un elemento INPUT automáticamente, 
* mientras el usuario presiona las teclas. Nota: El teclado solo aceptará números y el punto decimal.
* Ver también: {@link formato_float} y {@link formato_numeric}.
* @param fld object Elemento INPUT que tendrá el valor tecleado.
* @param milSep string Separador de miles, puede ser punto ó coma.
* @param decSep string Separador de decimales, puede ser punto ó coma.
* @param e object Evento de presionar una tecla.
* @return Retorna FALSE si no se puede colocar el formáto; de lo contrario, coloca el formato.*/
function formato_float_3d(fld, milSep, decSep, e){
      	var tipo=e.keyCode;

      	if (tipo == 8){  
        	return true; // 3 8,37,39,46
        }
        var sep = 0;
        var key = '';
        var i = j = 0;
        var len = len2 = 0;
        var strCheck = '0123456789';
        var aux = aux2 = '';
        var whichCode = (window.Event) ? e.which : e.keyCode;
        //if (whichCode == 13) return true; // Enter
        key = String.fromCharCode(whichCode); // Get key value from key code
        if (strCheck.indexOf(key) == -1) 
		return false; // Not a valid key
        len = fld.value.length;
        for(i = 0; i < len; i++)
        	if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) 
			break;
        aux = '';
        for(; i < len; i++)
        	if (strCheck.indexOf(fld.value.charAt(i))!=-1) 
			aux += fld.value.charAt(i);
        aux += key;
        len = aux.length;
        if (len == 0) 
		fld.value = '';
        if (len == 1) 
		fld.value = '0'+ decSep + '00' + aux;
        if (len == 2) 
		fld.value = '0'+ decSep + '0' + aux;
	if (len == 3) 
		fld.value = '0'+ decSep + aux;
	if (len > 3) {
        aux2 = '';
        for (j = 0, i = len - 4; i >= 0; i--) {
        	if (j == 3) {
        		aux2 += milSep;
        		j = 0;
        	}
        	aux2 += aux.charAt(i);
        	j++;
        }
        fld.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        	fld.value += aux2.charAt(i);

        fld.value += decSep + aux.substr(len - 3, len);
        }
        return false;
}

/** Coloca formato de número real (separadores de miles y decimales) a un elemento INPUT automáticamente, 
* mientras el usuario presiona las teclas. Nota: El teclado solo aceptará números y el punto decimal.
* Ver también: {@link formato_float_3d} y {@link formato_numeric}.
* @param fld object Elemento INPUT que tendrá el valor tecleado.
* @param milSep string Separador de miles, puede ser punto ó coma.
* @param decSep string Separador de decimales, puede ser punto ó coma.
* @param e object Evento de presionar una tecla.
* @return Retorna FALSE si no se puede colocar el formáto; de lo contrario, coloca el formato.*/
function formato_float(fld, milSep, decSep, e){

      	var tipo=e.keyCode;
      	if (tipo == 8){  
        	return true; // 3 8,37,39,46
        }
        var sep = 0;
        var key = '';
        var i = j = 0;
        var len = len2 = 0;
        var strCheck = '0123456789';
        var aux = aux2 = '';
        var whichCode = (window.Event) ? e.which : e.keyCode;
        //if (whichCode == 13) return true; // Enter
        key = String.fromCharCode(whichCode); // Get key value from key code
        if (strCheck.indexOf(key) == -1) return false; // Not a valid key
        len = fld.value.length;
        for(i = 0; i < len; i++)
        if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
        aux = '';
        for(; i < len; i++)
        if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
        aux += key;
        len = aux.length;
        if (len == 0) fld.value = '';
        if (len == 1) fld.value = '0'+ decSep + '0' + aux;
        if (len == 2) fld.value = '0'+ decSep + aux;
        if (len > 2) {
        aux2 = '';
        for (j = 0, i = len - 3; i >= 0; i--) {
        if (j == 3) {
        aux2 += milSep;
        j = 0;
        }
        aux2 += aux.charAt(i);
        j++;
        }
        fld.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
        fld.value += aux2.charAt(i);
        fld.value += decSep + aux.substr(len - 2, len);
        }
        return false;
}

/** Coloca formato de número entero (separadores de miles) a un elemento INPUT automáticamente, 
* mientras el usuario presiona las teclas. Nota: El teclado solo aceptará números y el punto decimal.
* Ver también: {@link formato_float} y {@link formato_float_3d}.
* @param fld object Elemento INPUT que tendrá el valor tecleado.
* @param milSep string Separador de miles, puede ser punto ó coma.
* @param decSep string Separador de decimales, puede ser punto ó coma.
* @param e object Evento de presionar una tecla.
* @return Retorna FALSE si no se puede colocar el formáto; de lo contrario, coloca el formato.*/
function formato_numeric(fld, milSep, decSep, e){
      	var tipo=e.keyCode;
      	if (tipo == 8)  
            return true; // 3 8,37,39,46
        var key = '';
        var i = j = 0;
        var len = len2 = 0;
        var strCheck = '0123456789';
        var aux = aux2 = '';
        var whichCode = (window.Event) ? e.which : e.keyCode;
        //if (whichCode == 13) return true; // Enter
        key = String.fromCharCode(whichCode); // Get key value from key code
        if (strCheck.indexOf(key) == -1) 
            return false; // Not a valid key
        len = fld.value.length;
        for(i = 0; i < len; i++)
            if ((fld.value.charAt(i) != '0')) 
                break;
        aux = '';
        for(; i < len; i++)
            if (strCheck.indexOf(fld.value.charAt(i))!=-1) 
                aux += fld.value.charAt(i);
        aux += key;
        len = aux.length;
        aux2 = '';
        for (j = 0, i = len - 1; i >= 0; i--) {
            if (j == 3) {
                aux2 += milSep;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
        }
        fld.value = '';
        len2 = aux2.length;
        for (i = len2 - 1; i >= 0; i--)
            fld.value += aux2.charAt(i);
        fld.value += aux.substr(len - 0, len);
        return false;
}

/** Valida si un valor dado representa un número (entero o real).
* Ver también: {@link isInteger}, {@link isInt} y {@link isFloat}.
* @param n string Valor dado.
* @return boolean Devuelve TRUE si el valor dado representa un número (entero o real); de lo contrario devuelve FALSE.*/
function IsNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/** Valida si un valor dado representa un número entero.
* Ver también: {@link isFloat}, {@link isInteger} y {@link IsNumeric}.
* @param n string Valor dado.
* @return boolean Devuelve TRUE si el valor dado representa un número entero; de lo contrario devuelve FALSE.*/
function isInt(x){
        var y = parseInt(x);
        if (isNaN(y))
                return false;
        return x == y && x.toString() == y.toString();
}

/** Valida si un valor dado representa un número real.
* Ver también: {@link isInteger}, {@link isInt} y {@link IsNumeric}.
* @param n string Valor dado.
* @return boolean Devuelve TRUE si el valor dado representa un número real; de lo contrario devuelve FALSE.*/
function isFloat(n) {
    return n === +n && n !== (n|0);
}

/** Valida si un valor dado representa un número entero.
* Ver también: {@link isFloat}, {@link isInt} y {@link IsNumeric}.
* @param n string Valor dado.
* @return boolean Devuelve TRUE si el valor dado representa un número entero; de lo contrario devuelve FALSE.*/
function isInteger(n) {
    return n === +n && n === (n|0);
}

/** Formatea un número real con separadores de miles, separadores de decimales y cantidad de decimales.
* Nota: El separador de miles será punto y el separador de decimales será coma.
* Ver también: {@link putFloatFormat}.
* @param nStr float Número decimal sin formato.
* @return string Cadena que representa el número real formateado con separadores de miles, 
* separadores de decimales y cantidad de decimales.*/
function putFormat(nStr)
{
	nStr += '';
	x = nStr.split(',');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}

/** Elimina el formato de una cadena en forma de número formateado tipo entero o real.
* Nota: Elimina las comas y los puntos; solo deja un punto representando los decimales en el caso que sea necesario.
* @param val1 string Cadena en forma de número formateado tipo entero o real.
* @return float Número sin formato.*/
function delFormat(val1){
	var val2='';
	for (i=0; i<(val1.length); i++){
		if (val1.charAt(i)!='.'){
			val2=val2+val1.charAt(i);
		}
	}
	return val2.replace(',', '.');
}

/** Redondea un número dado.
* @param num Número.
* @param dec Decimal.
* @return result float Número redondeado.*/
function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}

/** Formatea un número real con separadores de miles, separadores de decimales y cantidad de decimales.
* Nota: Si el separador de miles es punto, el separador de decimales debeser coma y viceversa.
* Ver también: {@link putFormat}.
* @param numVal float Número decimal sin formato.
* @param sepMil string Separador de miles, puede ser punto ó coma.
* @param sepDec string Separador de decimales, puede ser punto ó coma.
* @param cantDec integer Indica la cantidad de decimales que aceptará el formato.
* @return string Cadena que representa el número real formateado con separadores de miles, 
* separadores de decimales y cantidad de decimales.*/
function putFloatFormat(numVal, sepMil,sepDec,cantDec){
        var numStr=numVal.toFixed(cantDec).toString()
        var regExp=/\./;
        var array=new Array();        
        if (regExp.test(numStr))
            array=numStr.split('.');
        else{
            array[0]=numStr;
            array[1]='0';
        }
        regExp=/(\d+)(\d{3})/;
	while (regExp.test(array[0]))           
		array[0] = array[0].replace(regExp, '$1' + sepMil + '$2');
	return array[0]+((cantDec!=0)?sepDec+array[1]:'');
}

/** Porcentaje. Calcula el porcentaje entre dos (2) cantidades.
* @param lessNum float Monto porcentaje a calcular.
* @param greatNum float Monto en base al calculo.
* @return float Porcentaje*/
function percentage(lessNum,greatNum){
    var div=lessNum/greatNum;
    var x100to=(div*100).toString();
    if ((x100to.search(/[.]/))==-1)
	return x100to;	
    else
        return parseFloat(x100to).toFixed(2);    
}
