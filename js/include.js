function includeGroup(obj)
{
    var oJSON = JSON.parse(obj);
    var path=oJSON.path===undefined?"../../../":oJSON.path;
    var dir;
    if (oJSON.dir==="FDSoil")
        dir=FDSoil;
    else if (oJSON.dir==="appOrg")
        dir=appOrg;
    else if (oJSON.dir==="myApp")
        dir=myApp;
    var modulo = oJSON.modulo;
    var arr = oJSON.arr;
    var arrLen=arr.length;
    for (var i = 0; i < arrLen; i++) { 
        if (arr[i]["dir"]==="FDSoil")
            include(arr[i]["typ"] , path+FDSoil+"/"+arr[i]["typ"]+"/"+arr[i]["arc"]+"."+ arr[i]["typ"], arr[i]["top"]);
        if (arr[i]["dir"]==="packs")
            include(arr[i]["typ"] , path+FDSoil+"/"+arr[i]["dir"]+"/"+arr[i]["arc"]+"."+ arr[i]["typ"], arr[i]["top"]);
        else if (arr[i]["dir"]==="appOrg")
            include(arr[i]["typ"] , path+appOrg+"/"+arr[i]["typ"]+"/"+arr[i]["arc"]+"."+ arr[i]["typ"], arr[i]["top"]);
        else if (arr[i]["dir"]==="myApp")
            include(arr[i]["typ"] , path+myApp+"/"+arr[i]["typ"]+"/"+arr[i]["arc"]+"."+ arr[i]["typ"], arr[i]["top"]);
        else if (arr[i]["dir"]==="modulo")
            include(arr[i]["typ"] , path+dir+"/modulos/"+modulo+"/"+arr[i]["typ"]+"/"+arr[i]["arc"]+"."+ arr[i]["typ"], arr[i]["top"]);
        else if (arr[i]["dir"]==="pickList")
            include(arr[i]["typ"] , path+dir+"/pickList/"+modulo+"/"+arr[i]["typ"]+"/"+arr[i]["arc"]+"."+ arr[i]["typ"], arr[i]["top"]);
    }
}

function include(fileType, fileName, top = true){
    var fileRef=createJsCssFile(fileName, fileType);
    var tagName = (top) ? "head" : "body";
    if (typeof fileRef!="undefined")
        document.getElementsByTagName(tagName)[0].appendChild(fileRef);    
}

function createJsCssFile(fileName, fileType){
    if (fileType=="js"){ 
        var fileRef=document.createElement('script');
        fileRef.setAttribute("type","text/javascript");
        fileRef.setAttribute("src", fileName);
    } else if (fileType=="css") {
        var fileRef=document.createElement("link");
        fileRef.setAttribute("rel", "stylesheet");
        fileRef.setAttribute("type", "text/css");
        fileRef.setAttribute("href", fileName);
    }
    return fileRef
}

/*var filesadded="" //list of files already added
function checkLoadJsCssFile(fileName, fileType){
    if (filesadded.indexOf("["+fileName+"]")==-1){
        loadJsCssFile(fileName, fileType);
        filesadded+="["+fileName+"]"; //List of files added in the form "[fileName1],[fileName2],etc"
    }
    else
        alert("'"+fileName+"'..., file already added!");
}

function removejscssfile(fileName, fileType){
    var targetelement=(fileType=="js")? "script" : (fileType=="css")? "link" : "none" //determine element type to create nodelist from
    var targetattr=(fileType=="js")? "src" : (fileType=="css")? "href" : "none" //determine corresponding attribute to test for
    var allsuspects=document.getElementsByTagName(targetelement)
    for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
        if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(fileName)!=-1)
            allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
    }
}

//removejscssfile("somescript.js", "js") //remove all occurences of "somescript.js" on page
//removejscssfile("somestyle.css", "css") //remove all occurences "somestyle.css" on page


function replacejscssfile(oldfileName, newfileName, fileType){
 var targetelement=(fileType=="js")? "script" : (fileType=="css")? "link" : "none" //determine element type to create nodelist using
 var targetattr=(fileType=="js")? "src" : (fileType=="css")? "href" : "none" //determine corresponding attribute to test for
 var allsuspects=document.getElementsByTagName(targetelement)
 for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
  if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(oldfileName)!=-1){
   var newelement=createjscssfile(newfileName, fileType)
   allsuspects[i].parentNode.replaceChild(newelement, allsuspects[i])
  }
 }
}

replacejscssfile("oldscript.js", "newscript.js", "js") //Replace all occurences of "oldscript.js" with "newscript.js"
replacejscssfile("oldstyle.css", "newstyle", "css") //Replace all occurences "oldstyle.css" with "newstyle.css"
*/

