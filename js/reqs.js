function sendCedula( nac, cedula)
{
    function responseAjax(response)
    {   
        /*if ( response.length === 1 ){    
            document.getElementById("id_nombres").value = response[0]['nombre1'].toUpperCase();
            document.getElementById("id_apellidos").value = response[0]['apellido1'].toUpperCase();
            if (response[0]['nombre2'] != '')
                document.getElementById("id_nombres").value += " " + response[0]['nombre2'].toUpperCase();
            if (response[0]['apellido2'] != '')
                document.getElementById("id_apellidos").value += " " + response[0]['apellido2'].toUpperCase();
        } else {
            displayErrorMsg( document.getElementById("id_cedula"), "Cédula no existe..." );
        }*/

        if ( response ){    
            document.getElementById("id_nombres").value = response['NOMBRE1'].toUpperCase();
            document.getElementById("id_apellidos").value = response['APELLIDO1'].toUpperCase();
            if (response['nombre2'] != '')
                document.getElementById("id_nombres").value += " " + response['NOMBRE2'].toUpperCase();
            if (response['apellido2'] != '')
                document.getElementById("id_apellidos").value += " " + response['APELLIDO2'].toUpperCase();
        } else {
            displayErrorMsg( document.getElementById("id_cedula"), "Cédula no existe..." );
        }

    }
    removeErrorMsg( document.getElementById("id_cedula"))
    document.getElementById("id_nombres").value = "";
    document.getElementById("id_apellidos").value = "";
    var ajax = new sendAjax();
    ajax.url = pathApiWSCed(nac, cedula);
    ajax.funResponse = responseAjax;
    ajax.dataType='json';
    ajax.send();
}

function sendUsuario(usuario)
{
    function responseAjax(response)
    {   
        if(response=='t') {
            document.getElementById("div_usuario").innerHTML ="";
        } else if(response=='f') {
            document.getElementById("div_usuario").innerHTML ="Este nombre ya esta siendo usado por otra persona...";   
            document.getElementById("id_usuario").focus();
        }
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + FDSoil+ "/reqs/control/usuario/index.php/comprobarDisponibilidad";
    ajax.data = "usuario="+usuario;
    ajax.funResponse = responseAjax;
    ajax.send();
}

