/** Convierte una cadena de carácteres en una matriz (arreglo bidimensional).
* La cadena debe contener separadores que definan las columnas (campos) y las filas (registros).
* @param strArray (string) Cadena de carácteres con separadores que definan las columnas (campos) y las filas (registros).
* @param separate1 (string) Separador de columnas (campos).
* @param separate2 (string) Separador de filas (registros).
* @return matriz (array) Arreglo bidimensional (matriz).*/
function strToMatriz(strArray,separate1,separate2){
    var arrayRow=strArray.split(separate2);
    var matriz=new Array();
    for (a=0; a < arrayRow.length; a++){
        var arrayCol=arrayRow[a].split(separate1);
        matriz[a]=arrayCol;
    }
    return matriz;
}

/** Método de la burbula. Ordena una matriz (arreglo bidimensional) según la columna indicada por parámetro.
* Debe tambien indicar por parámetro si el orden es creciente ('<') o decreciente ('>').
* @param matrix (array) Matriz (arreglo bidimensional) para ser ordenado.
* @param sortNumCol (integer) Representa el índice de la columna por la cual será ordenada la matriz.
* @param ofWhatToWhat (string) Indica el orden en que se ordenará la matriz: creciente ('<') o decreciente ('>').
* @return matrix (array) Arreglo bidimensional ordenado según los parámetros enviados.*/
function bubbleMatrix(matrix,sortNumCol,ofWhatToWhat){
    var arrayAux=new Array();
    var array1=new Array();
    var array2=new Array();
    for(i=0;i<matrix.length;i++){
        for(j=i+1;j<matrix.length;j++){
             if (ofWhatToWhat=='>'){
                 array1=matrix[i][sortNumCol];
                 array2=matrix[j][sortNumCol];
             }
             else if (ofWhatToWhat=='<'){
                 array1=matrix[j][sortNumCol];
                 array2=matrix[i][sortNumCol];
             }
             if (array1<array2){
                arrayAux=matrix[i];
                matrix[i]=matrix[j];
                matrix[j]=arrayAux;
            }
       }
    }
    return matrix;
}

/** Selecciona y extrae de una matriz (arreglo bidimensional) las columnas especificadas.
* @param oldMatrix (array) Matriz (arreglo bidimensional) de donde se estraerán las columnas seleccionadas.
* @param fromCol (integer) Representa el índice desde donde se estraerán las columnas seleccionadas.
* @param toCol (integer) Representa el índice hasta donde se estraerán las columnas seleccionadas.
* @return newMatrix (array) Matriz (arreglo bidimensional) solo con las columnas seleccionadas.*/
function selectMatrixColumns(oldMatrix,fromCol,toCol){
var newMatrix=new Array();
var newRow=0;
var newCol=0;
for(var oldRow in oldMatrix) {
    newMatrix[newRow]=new Array();
    for(var oldCol in oldMatrix[oldRow]){
        if (oldCol>=fromCol && oldCol<=toCol) {
            newMatrix[newRow][newCol]=oldMatrix[oldRow][oldCol];
            newCol++;
        }
    }
    newCol=0;
    newRow++;
    }
    return newMatrix;
}

/** Remueve de una matriz (arreglo bidimensional) las columnas especificadas.
* @param oldMatrix (array) Matriz (arreglo bidimensional) donde se eliminarán las columnas especificadas.
* @param fromCol (integer) Representa el índice desde donde se eliminarán las columnas especificadas.
* @param toCol (integer) Representa el índice hasta donde se eliminarán las columnas especificadas.
* @return newMatrix (array) Matriz (arreglo bidimensional) con las columnas especificadas eliminadas.*/
function removeMatrixColumns(oldMatrix,fromCol,toCol){
var newMatrix=new Array();
var newRow=0;
var newCol=0;
for(var oldRow in oldMatrix) {
    newMatrix[newRow]=new Array();
    for(var oldCol in oldMatrix[oldRow]){
        if (!(oldCol>=fromCol && oldCol<=toCol)) {
            newMatrix[newRow][newCol]=oldMatrix[oldRow][oldCol];
            newCol++;
        }
    }
    newCol=0;
    newRow++;
    }
    return newMatrix;
}

/** Pivotea una matriz (arreglo bidimensional).
* Es decir, lo que antes eran las columnas, ahora serán las filas, y viceversa.
* @param oldMatrix (array) Matriz (arreglo bidimensional) para ser pivoteado.
* @return newMatrix (array) Matriz (arreglo bidimensional) pivoteado.*/
function pivotMatrix(oldMatrix){
    var newMatrix = new Array();
    var l=0;
    for (var x=0;x<(oldMatrix[0].length); x++){
        newMatrix[x] = new Array();
        for(var i in oldMatrix){
            newMatrix[x][l]=oldMatrix[i][x];
            l++;
        }
        l=0;
    }
    return newMatrix;
}

function inArray(needle, haystack, argStrict)
{
    var key = '';
    var strict = !!argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    }else{
        for (key in haystack) {
            if (haystack[key] == needle) { 
                return true;
            }
        }
    }

    return false;

}


/*function arrayCompare(a1, a2) {
    if (a1.length != a2.length) return false;
    var length = a2.length;
    for (var i = 0; i < length; i++) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(typeof haystack[i] == 'object') {
            if(arrayCompare(haystack[i], needle)) return true;
        } else {
            if(haystack[i] == needle) return true;
        }
    }
    return false;
}
*/

/**
 * Check if an array key or object property exists
 * @key - what value to check for
 * @search - an array or object to check in
 */
/*function keyExists(key, search) {
    if (!search || (search.constructor !== Array && search.constructor !== Object)) {
        return false;
    }
    for (var i = 0; i < search.length; i++) {
        if (search[i] === key) {
            return true;
        }
    }
    return key in search;
}*/

/*
module.exports = function in_array (needle, haystack, argStrict) { // eslint-disable-line camelcase
  //  discuss at: http://locutus.io/php/in_array/
  // original by: Kevin van Zonneveld (http://kvz.io)
  // improved by: vlado houba
  // improved by: Jonas Sciangula Street (Joni2Back)
  //    input by: Billy
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //   example 1: in_array('van', ['Kevin', 'van', 'Zonneveld'])
  //   returns 1: true
  //   example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'})
  //   returns 2: false
  //   example 3: in_array(1, ['1', '2', '3'])
  //   example 3: in_array(1, ['1', '2', '3'], false)
  //   returns 3: true
  //   returns 3: true
  //   example 4: in_array(1, ['1', '2', '3'], true)
  //   returns 4: false

  var key = ''
  var strict = !!argStrict

  // we prevent the double check (strict && arr[key] === ndl) || (!strict && arr[key] === ndl)
  // in just one for, in order to improve the performance
  // deciding wich type of comparation will do before walk array
  if (strict) {
    for (key in haystack) {
      if (haystack[key] === needle) {
        return true
      }
    }
  } else {
    for (key in haystack) {
      if (haystack[key] == needle) { // eslint-disable-line eqeqeq
        return true
      }
    }
  }

  return false
}
*/
