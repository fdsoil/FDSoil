const Select = (function () {
    return {
        optionAdd : function (oSelect, valor, texto) {
            const oOption = document.createElement("option");
            oOption.value = valor;
            oOption.text = texto;
            oSelect.options.add(oOption);
        },
        getValues : function (objForm, objStrArray) {
	    if (objForm.length!=0)
		for (i=0;i<objForm.length;i++)
		    objStrArray.value=(i==0)?objForm.options[i].value:objStrArray.value+", "+objForm.options[i].value;   
            else
                objStrArray.value='';
   	    return objStrArray.value;
        }
    }
})();




