function jQryConstraintsData(obj){
    $(obj).ready(function(){
        /* Accepts Numbers and Letters. */
        $("[data-constraints=code]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9a-zA-Z]/);});
	/* Accepts Letters, Blanks, Points and Commas. */
        $("[data-constraints=fullname]").keypress(function(event){return acceptOnlyRegExp(event, /[a-zñA-ZÑ\s.,]/);});
        /* Accepts Numbers. */
        $("[data-constraints=number]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9]/);});
        /* Accepts Letters, Blanks, Points, Commas and Parentheses. */
	//$("[data-constraints=textarea]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9a-zñA-ZÑ\s.,()-°#\/\"]/);});
	$("[data-constraints=textarea]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9a-zñA-ZÑ\s.,;:°!#$%\/()=¿?¡*-ÁÉÍÓÚáéíóú]/);});//"&+
        /* Accepts Letters, Blanks, Points, Commas, Parentheses and Ampersand. */
	$("[data-constraints=firm_name]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9a-zñA-ZÑ\s.,()&]/);});
        /* Accepts Numbers, Letters, Points And Underscore. */
        $("[data-constraints=username]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9a-zA-Z._]/);});
        /* Accepts Numbers, Letters, Points, Underscore, Guión And Arroba. */
        $("[data-constraints=email]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9a-zA-Z.@_-]/);});
        /* Accepts Numbers, Letters, Points, Guión, Slash And Parentheses. */
	//$("[data-constraints=doc_num]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9a-zA-Z.()-\/]/);});
	$("[data-constraints=doc_num]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9a-zA-Z.\/()°#-]/);});
	//$("[data-constraints=doc_num]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9a-zA-Z.()-\/]/);});
	$("[data-constraints=telephone]").keypress(function(event){return acceptOnlyRegExp(event, /[0-9\s.()-\/]/);});

    });	
}

function acceptOnlyRegExp(e,strRegExp){    
    var tecla=(document.all)?e.keyCode:e.which;
    if(tecla == 8)
        return true;    
    var patron = strRegExp;
    var te;
    te=String.fromCharCode(tecla);
    return patron.test(te);
}
