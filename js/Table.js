var Table = (function () {

    return {
        addEventListenerRowsGroup: function (id, namespace) {
            let tbl = document.getElementById(id);
            let rows = tbl.tBodies[0].rows;
            let lenRows = rows.length;
            for (let i=0; i<lenRows;i++){
                let aImg = rows[i].cells[ rows[i].cells.length-1 ].getElementsByTagName('img');
                aImg[0].addEventListener("click", function() { 
                    namespace.valEdit(aImg[0]);
                    valBtnClose();
                });
                aImg[1].addEventListener("click", function() { 
                    namespace.rqsRemove(rows[i].id);
                    valBtnClose();
                });
            }
        },

        /** Elimina 'todas' las filas de una tabla.
        * Generalmente la tabla debe quedar mínimo con una o dos fila, 
        * la(s) cual(es) representa(n) el encabezado (th).
        * Para indicar este mínimo se implementa el parametro 'limit'.
        * @param idTable (string) Identificador de la tabla a la cual se eliminarán las filas.
        * @param limit (integer) Número de filas mínimas (encabezado) con que quedará la tabla.*/
        deleteAllRowsTable: function (idTable,limit) {
            for(var i = document.getElementById(idTable).rows.length; i > limit;i--){
                document.getElementById(idTable).deleteRow(i -1);
            }
        },

        /** Pinta (maquilla) las filas (elemento TR) de un elemento TABLE,
        * unas claras y otras oscuras de forma intermitente.
        * Esta función necesita definada las clases css: losnone y lospare.
        * @param idTable (string) Identificador del elemento TABLE que será maquillado.*/
        paintTRsClearDark: function (idTable) {
            var objTable= document.getElementById(idTable);
            if (objTable.rows.length == 1)
                return false;
            for (rowIndex = 1; rowIndex < objTable.rows.length; rowIndex++) 
                objTable.rows[rowIndex].className=((rowIndex% 2) == 0)?'losnone':'lospare';              
        }

    };
})();








