/* Variable global que cuenta el tiempo que dura el usuario sin tocar el teclado ni el mouse.*/
var t;

/*Control de Tiempo de Sesión; 
* controla el tiempo desde que el usuario activa la sesión y no toca el teclado ni el mouse.
* Si el usuario no toca el teclado ni el mouse durante un tiempo especificado en la función resetTimer(),
* entonces el sistema cierra la sesión automáticamente. Implementa la variable global 'timer'*/
function timeControlSession(){
	document.body.setAttribute('onkeypress',"resetTimer();");
        document.body.setAttribute('onmousemove',"resetTimer();");
        resetTimer();
}

/* Resetea el tiempo de la variable global 'timer', cada vez que el usuario activa el mouse o toca el teclado;
* de lo contrario ejecuta la función logOut(); esta última función redirecciona la página al cierre forzado de sesión.*/
function resetTimer(){
	clearTimeout(t);
	t=setTimeout(logOut,1380000); //3000
}

/* Redirecciona la página al cierre forzado de sesión.*/
function logOut(){
	location.href="../../../"+FDSoil+"/"+modulos+"/admin_session_closed/";
}
