/** Convierte un archivo en metadata.
* Si cumple con la validación esta función asignará la metadata en el 'value' del elemento
* identificado con 'strIdFieldMeta'.
* @param objField (elemento) Elemento input que cargará el archivo que se requiere subir.
* @param strIdFieldMeta (string) Id del elemento que guardará el resultado de esta función.
* @param numSizeMax (integer) Para validar el tamaño máximo del archivo a subir.
* @param aExtType (array) Arreglo que contiene la lista de extenciones permitidas para subir.
* @return boolean Devuelve falso si no cumple o de lo contrario verdadero*/
function metaDataConvertFile(objField, strIdFieldMeta, numSizeMax, aExtType) {
    if (objField.files && objField.files[0]) {
        var FR = new FileReader();
        var typeArch = (objField.files[0].type).split('/');
        var control = false;
        for (var i = 0; i < aExtType.length; i++) {
            if (aExtType[i] === typeArch[1]) {
                control = true;
                break;
            }
        }
        if (control) {
            var sizeByte = objField.files[0].size;
            var sizekiloByte = parseInt(sizeByte / 1024);
            if (sizekiloByte > numSizeMax) {
                alert('Disculpe: El tamaño del archivo supera el limite permitido de: '+numSizeMax+' kB ('+roundNumber((numSizeMax / 1024),0)+' MB)');
                return false;
            }
            else {
                FR.onload = function (e) {
                    $('#' + strIdFieldMeta).val(e.target.result);
                };
                FR.readAsDataURL(objField.files[0]);
                return true;
            }
        }
        else {
            alert('Disculpe: El tipo de archivo adjunto es inválido.');
            return false;
        }
    }
}


