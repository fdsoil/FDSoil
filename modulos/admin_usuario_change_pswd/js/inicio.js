function inicio(oJSON)
{
    initView();
    menu(oJSON);
}

function valForm()
{
    var clave1=document.getElementById('id_clave1').value;
    var clave2=document.getElementById('id_clave2').value;
    var clave3=document.getElementById('id_clave3').value;
    var divErr1=document.getElementById('div_clave1');
    var divErr2=document.getElementById('div_clave2');
    var divErr3=document.getElementById('div_clave3');    
    divErr1.innerHTML=(clave1=='')?'Debe Indicar la Clave Anterior...':'';
    divErr2.innerHTML=(clave2=='')?'Debe Indicar la Nueva Clave...':'';
    divErr3.innerHTML=(clave3=='')?'Debe Confirmar la Nuena Clave...':'';            
    if (divErr1.innerHTML=='' && divErr2.innerHTML=='' && divErr3.innerHTML=='')
        document.forms[0].submit();    
}

function valNewClave()
{
var obj=new appPasswrd();
var objClave1=document.getElementById('id_clave1');
var objClave2=document.getElementById('id_clave2');
var divClave2=document.getElementById('div_clave2');
    if (validatePassWord(objClave2.value)==false) {
        divClave2.innerHTML='Debe estar conformada por letras mayúsculas y minúsculas, números, caracteres especiales y el largo debe ser de '+obj.rangeMin+' a '+obj.rangeMax+' dígitos';   
    } else {
        divClave2.innerHTML='';  
    	divClave2.innerHTML=(objClave1.value==objClave2.value)?'La nueva debe ser diferente a la anterior...':'';  
    }
}

function valNewClaveConfirme()
{
    var clave2=document.getElementById('id_clave2').value;
    var clave3=document.getElementById('id_clave3').value;
    var divErr3=document.getElementById('div_clave3');  
    divErr3.innerHTML=(clave2!=clave3)?'Las claves no coinciden...':'';        
}

