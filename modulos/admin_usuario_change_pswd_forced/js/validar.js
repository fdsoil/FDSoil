function divIniData()
{
    document.getElementById('id_div_val_data').style.display = 'block';
    document.getElementById('id_div_val_update_key').style.display = 'none';
}

function onOffDiv()
{
    toggle('id_div_val_data', 'slide', 500);
    toggle('id_div_val_update_key', 'slide', 500);
}

function bTrDisplay(idTr)
{
    return (document.getElementById(idTr).style.display==='none')?false:true;
}

function valDivValData()
{
    var ci=document.getElementById('id_cedula').value;
    var email=document.getElementById('id_correo').value;
    var cel=document.getElementById('id_celular').value;
    var resp=document.getElementById('id_respuesta').value;
    var bTr1=bTrDisplay('tr_pregunta_seguridad_1');
    if (ci!=''    && div_cedula_id.innerHTML=='' && 
        email!='' && div_correo.innerHTML==''    && 
        cel!=''  && div_celular.innerHTML==''    &&
        ((bTr1 && resp!='' && div_respuesta.innerHTML=='') || !bTr1))
            onOffDiv();
    else
        alert('Valide los Datos antes de Continuar...','Mensaje');
}

function valForm()
{    
    var clave1=document.getElementById('id_clave1').value;
    var clave2=document.getElementById('id_clave2').value;
    var clave3=document.getElementById('id_clave3').value;

    var divErr1=document.getElementById('div_clave1');
    var divErr2=document.getElementById('div_clave2');
    var divErr3=document.getElementById('div_clave3');    
    
    divErr1.innerHTML=(clave1=='')?'Debe Indicar la Clave Anterior...':'';
    divErr2.innerHTML=(clave2=='')?'Debe Indicar la Nueva Clave...':'';
    divErr3.innerHTML=(clave3=='')?'Debe Confirmar la Nuena Clave...':'';

    var bTr2=bTrDisplay('tr_pregunta_seguridad_2');

    if ( bTr2 ){
        var pregunta=document.getElementById('id_pregunta2').value;
        var respuesta=document.getElementById('id_respuesta2').value;
        var divErr4=document.getElementById('div_pregunta2');    
        var divErr5=document.getElementById('div_respuesta2');
        divErr4.innerHTML=(pregunta==0)?'Debe Seleccionar una Pregunta...':'';            
        divErr5.innerHTML=(respuesta=='')?'Debe Responder la Pregunta...':'';
    }

    if ( ( divErr1.innerHTML == '' && divErr2.innerHTML == '' && divErr3.innerHTML == '' ) &&
       ( ( bTr2 && divErr4.innerHTML == '' && divErr5.innerHTML == '' ) || !bTr2 ) ) 
           javascript:document.forms[0].submit();        
}

function valNewClaveConfirme()
{
    var clave2=document.getElementById('id_clave2').value;
    var clave3=document.getElementById('id_clave3').value;
    var divErr3=document.getElementById('div_clave3');  
    divErr3.innerHTML=(clave2!=clave3)?'Las claves no coinciden...':'';        
}

function valNewClave()
{
    var objClave1=document.getElementById('id_clave1');
    var objClave2=document.getElementById('id_clave2');
    var divClave2=document.getElementById('div_clave2');
    if (validatePassWord(objClave2.value)==false)
        divClave2.innerHTML='Debe estar conformada por letras, números, caracteres especiales y el largo debe ser de 6 a 10 dígitos';   
    else{
        divClave2.innerHTML='';  
        divClave2.innerHTML=(objClave1.value==objClave2.value)?'La nueva debe ser diferente a la anterior...':'';  
    }
}

function validarPreguntaSecreta()
{
    var ci=document.getElementById('id_cedula').value;
    var email=document.getElementById('id_correo').value;
    var cel=document.getElementById('id_celular').value;
    var bTr1=bTrDisplay('tr_pregunta_seguridad_1');
    if (bTr1 && 
        ci!='' && 
        email!='' && 
        cel!='' && 
        div_correo.innerHTML=='' && 
        div_cedula_id.innerHTML=='' && 
        div_celular.innerHTML=='')
            sendPreguntaSeguridad();
    else
            document.getElementById('id_pregunta').value='';
}
