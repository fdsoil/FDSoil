function sendValidarCedulaId( ci)
{
    var responseAjax = (response) =>
    {
        document.getElementById('div_cedula_id').innerHTML=(response==0)?'La Cédula que Introdujo es Incorrecta':'';
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + FDSoil + "/reqs/control/usuario/index.php/validarCedulaId";
    ajax.data = "ci="+ci;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'html';
    ajax.send();
}

function sendValidarEmail( email)
{
    var responseAjax = (response) =>
    {
	document.getElementById('div_correo').innerHTML=(response==0)?'El Correo que Introdujo es Incorrecto':'';
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + FDSoil + "/reqs/control/usuario/index.php/validarEmail";
    ajax.data = "email="+email;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'html';
    ajax.send();
}

function sendValidarCelular( cel)
{
    function responseAjax( response )
    {
	document.getElementById('div_celular').innerHTML=(response==0)?'El Télefono Celular que Introdujo es Incorrecto':'';
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + FDSoil + "/reqs/control/usuario/index.php/validarCelular";
    ajax.data = "cel="+cel;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'html';
    ajax.send();
}

function sendValPswdOld(clave)
{
    var responseAjax = (response) =>
    {
        if(response==0) {
            document.getElementById("div_clave1").innerHTML ="La clave está errada..";   
            document.getElementById("id_clave1").focus();
        } else if(response==1) {        
            document.getElementById("div_clave1").innerHTML ="";    
            document.getElementById("id_clave2").focus();
        }
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + FDSoil + "/reqs/control/usuario/index.php/valPswdOld";
    ajax.data = "clave="+clave;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'html';
    ajax.send();
}


function sendValidarRespSeguridad(resp)
{
    var responseAjax = (response) =>
    {
        document.getElementById('div_respuesta').innerHTML=(response==0)?'La Respuesta de Seguridad es Incorrecta':'';
    }
    if (document.getElementById('id_pregunta').value!='') {
        var ajax = new sendAjax();
        ajax.method = 'POST';
        ajax.url = "../../../" + FDSoil + "/reqs/control/usuario/index.php/validarRespSeguridad";
        ajax.data = "resp="+resp;
        ajax.funResponse = responseAjax;
        ajax.dataType = 'html';
        ajax.send();
    }
}

function sendPreguntaSeguridad()
{
    var responseAjax = (response) =>
    {
        document.getElementById('id_pregunta').value=response;
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + FDSoil + "/reqs/control/usuario/index.php/preguntaSecreta";
    ajax.funResponse = responseAjax;
    ajax.dataType = 'html';
    ajax.send();
}


