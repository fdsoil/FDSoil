<?php
use \FDSoil\Func as Func;
use \FDSoil\Usuario as Usuario;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        /*Functions::btnRecordAdd( $xtpl ,["btnRecordName"=>"Menu (Nivel 0)"]);
        $result = $obj->menuNivel_0_list();
        $msj = $obj->listar_MsjDB();
        while ($row = $obj->extraer_registro($msj)) {
            $xtpl->assign('ID_ROW', $row[0]);
            $xtpl->assign('DESCRIPCION', $row[1]);
            Functions::btnRecordEdit( $xtpl ,["btnId"=>$row[0]]);
            Functions::btnRecordDelete( $xtpl ,["btnId"=>$row[0]]);
            $xtpl->parse('main.ciclo_msj');
        }
        Functions::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
*/
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }     
}

