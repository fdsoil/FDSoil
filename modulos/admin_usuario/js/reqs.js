function send_cedula()
{
    var responseAjax = (response) =>
    {
        if ( response[0] != "undefined" ) {
            var sNomApe = response[0].nombre + ', '+response[0].apellido;
            document.getElementById("id_nombre_apellido").value = sNomApe;
            document.getElementById('add_saime').setAttribute('src','../../../'+appOrg+'/img/addnew.gif');
            document.getElementById('add_saime').setAttribute('style',"cursor: pointer");
            document.getElementById('add_saime').setAttribute('onclick',"valInsertOption('"+sNomApe+"')");
        }
    }
    var nac = document.getElementById('id_nacionalidad').value;
    var num = document.getElementById('id_cedula').value;
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + FDSoil + "/reqs/control/usuario/index.php/buscarXCed";
    ajax.funResponse = responseAjax;
    ajax.data = "ced="+nac+num;
    ajax.dataType = 'json';
    ajax.send();
}

function sendUsuariosListGet()
{
    var responseAjax = (response) =>
    {
        deleteAllRowsTable('tablaRows',1);
        if ( response.length == 0 )
            alert('No hay registros con los parámetros suministrados.','Disculpe');
        else if ( response.length > 0 && response.length < 501 ) {
                fillTab(response);
                hide('id_div_panel' ,500);
                hide('id_consultar' ,500);
                show('id_filtrar',500);
                show('id_div_resultado',500);
                show('tablaRows',500);
                jQryTableRefresh('tablaRows');
        } else
            alert('Su consulta devuelve demasiados registros, \n intente realizando un filtro más específico.', 'Disculpe');
    }
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + FDSoil + "/reqs/control/usuario/index.php/usuarioGet";
    ajax.funResponse = responseAjax;
    ajax.data = request(document.getElementById("two"));
    ajax.dataType = 'json';
    ajax.send();
}

