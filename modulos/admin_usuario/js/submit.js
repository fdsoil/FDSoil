function submitInsert()
{
    relocate('../admin_usuario_aux/', {});
}

function submitEdit(valor)
{
    relocate('../admin_usuario_aux/', {'id':valor});
}

function submitDelete(valor)
{
    if (confirm('¿Desea Eliminar Este Registro?'))
        relocate('../admin_usuario/delete/', {'id':valor});
}

function submitKeyReset(val1, val2)
{
    confirm('¿Desea Resetear la Clave de Este Registro?', 'Confirmar', function (ok) {
        if (ok)
            relocate('../admin_usuario/reset_key/', {'id':val1, 'ced':val2});
        });
}
