function fillTab(aObjs)
{

    var tbl = document.getElementById('tablaRows');
    deleteAllRowsTable('tablaRows',1);

    if (!tbl.tBodies[0]) {
        var tbody = document.createElement('tbody');
        tbody.setAttribute('style', 'display: block');
        tbody.setAttribute('style', 'width: 100%');
        tbl.appendChild(tbody);
    }

    if (aObjs.length !== 0) {
        for (var i = 0; i < aObjs.length; i++)
            tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));
        //setAttributeTD('id_tab_minuta_acuerdo','align','left', 1, 3, 1, aObjs.length+1);
        paintTRsClearDark('tablaRows');
    }

    function fillTabAux(row)
    {

        var oTr = document.createElement('tr');
        oTr.id = row.id;
        var aTd = [];

        aTd[0] = document.createElement('td');
        aTd[0].appendChild(document.createTextNode(row.cedula));
        oTr.appendChild(aTd[0]);

        aTd[1] = document.createElement('td');
        aTd[1].appendChild(document.createTextNode(row.nombre));
        oTr.appendChild(aTd[1]);

        aTd[2] = document.createElement('td');
        aTd[2].appendChild(document.createTextNode(row.apellido));
        oTr.appendChild(aTd[2]);

        aTd[3] = document.createElement('td');
        aTd[3].appendChild(document.createTextNode(row.usuario));
        oTr.appendChild(aTd[3]);

        aTd[4] = document.createElement('td');
        aTd[4].appendChild(document.createTextNode(row.correo));
        oTr.appendChild(aTd[4]);

        aTd[5] = document.createElement('td');
        aTd[5].appendChild(document.createTextNode(row.perfil));
        oTr.appendChild(aTd[5]);

        aTd[6] = document.createElement('td');
        aTd[6].appendChild(document.createTextNode(row.estatus));
        oTr.appendChild(aTd[6]);

        var oImgEdit = document.createElement('img');
        oImgEdit.setAttribute('class', 'accion');
        oImgEdit.setAttribute('src', '../../../../../'+appOrg+'/img/edit.png');
        oImgEdit.setAttribute('title', 'Editar datos...');
        oImgEdit.setAttribute('onclick',"submitEdit("+row.id+");");
        aTd[7] = document.createElement('td');
        aTd[7].align = 'center';
        aTd[7].appendChild(oImgEdit);

        var oImgReset = document.createElement('img');
        oImgReset.setAttribute('class', 'accion');
        oImgReset.setAttribute('src', '../../../../../'+appOrg+'/img/llave.png');
        oImgReset.setAttribute('title', 'Reset Clave...');
        oImgReset.setAttribute('onclick',"submitKeyReset("+row.id+", '"+row.cedula+"');");
        aTd[7].appendChild(oImgReset);
        oTr.appendChild(aTd[7]);

        // var oImgDelete = document.createElement('img');
        // oImgDelete.setAttribute('class', 'accion');
        // oImgDelete.setAttribute('src', '../../../../../'+appOrg+'/img/unlock.png');
        // oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
        // oImgDelete.setAttribute('onclick',"submitUnlock(("+row.id+");");
        // aTd[7].appendChild(oImgDelete);
        // oTr.appendChild(aTd[7]);

        // var oImgDelete = document.createElement('img');
        // oImgDelete.setAttribute('class', 'accion');
        // oImgDelete.setAttribute('src', '../../../../../'+appOrg+'/img/cross.png');
        // oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
        // oImgDelete.setAttribute('onclick',"DDDDsubmitDelete(("+row.id+");");
        // aTd[7].appendChild(oImgDelete);
        // oTr.appendChild(aTd[7]);

        return oTr;

    }

}
