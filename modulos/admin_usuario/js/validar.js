function initPanelRuta()
{
    document.getElementById('id_nombre_apellido').value = '';
    document.getElementById('id_cedula').value = '';
    document.getElementById("resp_saime").innerHTML = '';
    document.getElementById("id_nacionalidad").selectedIndex = "0";
    document.getElementById('add_saime').setAttribute('src','../../../'+appOrg+'/img/addnew_off.png');
    document.getElementById('add_saime').setAttribute('style',"cursor: normal");
    document.getElementById('add_saime').setAttribute('onclick',"");
}

function insertOption(elemento){
    var arr = elemento.split(" - ");
    var x = document.getElementById("id_caja_nombre_apellido");
    var option = document.createElement("option");
    option.value = arr[0];
    option.text = elemento;
    x.add(option);
    initPanelRuta();
}

function deleteOption(){
    var x = document.getElementById("id_caja_nombre_apellido");
    x.remove(x.selectedIndex);
}

function valInsertOption(response)
{
    var nac = document.getElementById('id_nacionalidad').value;
    var num = document.getElementById('id_cedula').value;

    if (valQuePasajeroNoExiste(nac + num +" - "+response))
        insertOption(nac + num +" - "+response);
    else{
        alert('Disculpe: Usuario repetido');
        initPanelRuta();
    }
     miGetValues(document.getElementById("id_caja_nombre_apellido"), document.getElementById("id_str_arr_caja"));
}

function valQuePasajeroNoExiste(pasajero)
{
    var resp = false;
    var oCaja = document.getElementById("id_caja_nombre_apellido");
    var oCajaLen=oCaja.length;
    if (oCajaLen==0) {
        resp = true;
    }
    else {
        for (var i = 0; i < oCajaLen; i++) {
            if (oCaja.options[i].value == pasajero){
                resp = false;
                break;
            }else
                resp = true;
        }
    }

    return resp;
}

function miGetValues(objForm, objStrArray)
{
    if (objForm.length!=0) {
        var arr = [];
        for ( var i = 0; i < objForm.length; i++ )
            arr[i] = objForm.options[i].value;//arr[i]='\"'+objForm.options[i].value+'\"';
        objStrArray.value = JSON.stringify( arr );
    } else {
        objStrArray.value="";
    }
    return objStrArray.value;
}

function regresar(){
    hide('id_div_resultado',500);
    hide('tablaRows',500);
    hide('id_filtrar',500);
    show('id_div_panel' ,500);
    show('id_consultar' ,500);
}
