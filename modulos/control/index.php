<?php 
session_start();
require_once("../../../".$_SESSION['FDSoil']."/class/Controll/ControllNav.php");
abstract class Index
{
    public function control()
    {
        $arr['path']=__DIR__."/../";
        $arr['filesName']=['save', 'delete', 'login', 'logout', 'reset_key', 'recover', 'captcha','generar'];
        $obj = new FDSoil\Controll\ControllNav($arr);
        $obj->execute();
    }
}
Index::control();
