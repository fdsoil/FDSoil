function submitInsert()
{
    relocate('../admin_roles_aux/', {});    
}

function submitEdit(valor)
{
    relocate('../admin_roles_aux/', {'id':valor});    
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok)
    {
        if (ok)
            relocate('delete/', {'id':val});
    });
}
