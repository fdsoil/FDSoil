function submitInsert(valor)
{
    relocate('../admin_menu_1n_aux/', {'id_n0':valor});    
}

function submitEdit(valor1,valor2)
{
    relocate('../admin_menu_1n_aux/', {'id':valor1,'id_n0':valor2});    
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok)
    {
        if (ok)
            relocate('delete/', {'id':val});
    });
}
