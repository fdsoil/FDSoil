FDSoil Versión 3.0
==================

Descripción
---------------

"Fast Development Structure, Ordered In Layers" 
(Estructura de Desarrollo Rápido, Ordenado en Capas) 

La solución tecnológica que está revolucionando la forma de desarrollar sistemas web, especialmente en el Sector Público, en Venezuela.

Porque es sencillo, fácil y de calidad. Mientras más sencillo es más fácil (de aprender e implementar) y porque la calidad la certifica el usuario. 

Programamos en base a lenguajes nativos:
----------------------------------------

- **Del lado del cliente: HTML define el contenido de la página, CSS el estilo y JavaScript su comportamiento.**

- **Del lado del Servidor: PHP en tecnologías web, es el lenguaje más sencillo, flexible, rendidor, económico y popular. PostgreSQL es el gestor de base de datos de código abierto más avanzado y robusto del mundo.**

- **Además: FDSoil es compatible con paquetes como JQuery, HighCharts, Bootstrap, Xtemplate, TcPdf, etc.**

Programación Orientada a Objeto
-------------------------------
Es un paradigma (modelo ejemplar) de desarrollo de alta complejidad. Gracias a la flexibilidad que permite PHP, en nuestros inicios del framework, combinamos la programación procedimental con la orientada a objeto.

Sin embargo y como la mejora siempre es posible, fuimos refactorizando el código para su optimización. Ya FDSoil es 97% OOP. 

MVC
---
Patrón arquitectónico que separa los datos y la lógica del negocio (el modelo), de la interfaz del usuario (la vista) y del módulo encargado de gestionar los eventos (el controlador)... FDSoil separa además, c/u de ellas de la siguiente forma:

- **Modelo: Abstracción de datos - Acceso a datos - Archivos script.sql - Procedimientos (transacciones) almacenados en base de datos.**

- **Vista: Layout único - Plantillas de contenido - Plantillas reutilizables - Librerías JavaScript - Hojas de estilo CSS.**

- **Controlador: Controlador Frontal - SubControladores.**

*Ver tutorial en la página oficial:*
 
http://fdsoil.com.ve

Actualización de la Estructura de la Base de Datos
--------------------------------------------------

A partir de el commit de fecha [13/02/2019](https://gitlab.com/fdsoil/FDSoil/tree/46437f085466f2cea75f94f05aded516dbbc1a89) 

(Se le agregó al framework la posibilidad auditar a los DBA que administran la tabla 'usuario'. 
Por lo tanto, hubo cambios en el esquema de seguridad de la base de datos. Estos cambios fueron ejecutados en la base de datos a travez 
de un script.sql el cual está documentado en el README.md)

```
ALTER TABLE seguridad.usuario ADD COLUMN fecha_insert date DEFAULT ('now'::text)::date;
UPDATE seguridad.usuario SET fecha_insert = (SELECT ('now'::text)::date);
ALTER TABLE seguridad.usuario ALTER COLUMN fecha_insert SET NOT NULL;

ALTER TABLE seguridad.usuario ADD COLUMN hora_insert time without time zone DEFAULT ('now'::text)::time without time zone;
UPDATE seguridad.usuario SET hora_insert = (SELECT ('now'::text)::time);
ALTER TABLE seguridad.usuario ALTER COLUMN hora_insert SET NOT NULL;

ALTER TABLE seguridad.usuario ADD COLUMN fecha_update date DEFAULT ('now'::text)::date;
UPDATE seguridad.usuario SET fecha_update = (SELECT ('now'::text)::date);
ALTER TABLE seguridad.usuario ALTER COLUMN fecha_update SET NOT NULL;

ALTER TABLE seguridad.usuario ADD COLUMN hora_update time without time zone DEFAULT ('now'::text)::time without time zone;
UPDATE seguridad.usuario SET hora_update = (SELECT ('now'::text)::time);
ALTER TABLE seguridad.usuario ALTER COLUMN hora_update SET NOT NULL;

ALTER TABLE seguridad.usuario ADD COLUMN id_user_insert integer;
UPDATE seguridad.usuario SET id_user_insert = id;
ALTER TABLE seguridad.usuario ALTER COLUMN id_user_insert SET NOT NULL;

ALTER TABLE seguridad.usuario ADD COLUMN id_user_update integer;
UPDATE seguridad.usuario SET id_user_update = id;
ALTER TABLE seguridad.usuario ALTER COLUMN id_user_update SET NOT NULL;

DROP FUNCTION seguridad.usuario_registrar(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying);

-- Function: seguridad.usuario_registrar(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, integer)

-- DROP FUNCTION seguridad.usuario_registrar(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, integer);

CREATE OR REPLACE FUNCTION seguridad.usuario_registrar(
    i_id_usuario integer,
    i_usuario character varying,
    i_correo character varying,
    i_cedula character varying,
    i_clave character varying,
    i_nombre character varying,
    i_apellido character varying,
    i_celular character varying,
    i_telefono1 character varying,
    i_telefono2 character varying,
    i_id_rol integer,
    i_id_status integer,
    i_pregunta_seguridad character varying,
    i_respuesta_seguridad character varying,
    i_id_user_action integer)
  RETURNS character AS
$BODY$
DECLARE 
        o_return character;
        v_existe boolean;
BEGIN
        o_return:='';
        IF  i_id_usuario=0 THEN 
		SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM seguridad.usuario WHERE cedula = i_cedula;
                IF  v_existe='f' THEN
			INSERT INTO seguridad.usuario(
                            usuario, 
                            correo, 
                            cedula, 
                            clave, 
                            nombre, 
                            apellido, 
                            celular,
			    telefono1,
                            telefono2,
                            id_rol,
                            id_status,
                            pregunta_seguridad,
			    respuesta_seguridad,
                            fecha_registro,
                            id_user_insert,
                            id_user_update
                        ) VALUES (
                            i_usuario,
                            i_correo,
                            i_cedula,
                            i_clave,
                            i_nombre,
                            i_apellido,
                            i_celular,
			    i_telefono1,
                            i_telefono2,
                            i_id_rol,
                            i_id_status,
                            i_pregunta_seguridad,
			    i_respuesta_seguridad,
                            current_date,
                            i_id_user_action,
                            i_id_user_action
                        );
			o_return:= 'C';
		ELSE
                        o_return:='T';        
		END IF;
	ELSE
		UPDATE seguridad.usuario
			SET usuario = i_usuario, 
                            correo = i_correo,
                            cedula = i_cedula,
                            nombre = i_nombre,
                            apellido = i_apellido, 
			    celular = i_celular,
                            telefono1 = i_telefono1,
                            telefono2 = i_telefono2,
                            id_rol = i_id_rol,
                            id_status = i_id_status, 
			    pregunta_seguridad = i_pregunta_seguridad, 
                            respuesta_seguridad = i_respuesta_seguridad,
                            fecha_registro=current_date,
                            fecha_update=(SELECT ('now'::text)::date),
                            hora_update=(SELECT ('now'::text)::time),
                            id_user_update = i_id_user_action
		WHERE id=i_id_usuario;		
		o_return:='A';	
	END IF;
        RETURN o_return;  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION seguridad.usuario_registrar(integer, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying, integer)
  OWNER TO postgres;


-- Function: seguridad.usuario_registrar_web(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying)

-- DROP FUNCTION seguridad.usuario_registrar_web(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying);

CREATE OR REPLACE FUNCTION seguridad.usuario_registrar_web(
    i_usuario character varying,
    i_correo character varying,
    i_cedula character varying,
    i_clave character varying,
    i_nombre character varying,
    i_apellido character varying,
    i_celular character varying,
    i_telefono1 character varying,
    i_telefono2 character varying,
    i_id_rol integer,
    i_id_status integer,
    i_pregunta_seguridad character varying,
    i_respuesta_seguridad character varying)
  RETURNS character AS
$BODY$
DECLARE 
        o_return character;
        v_existe boolean;
        oCORREO  integer;
BEGIN
        o_return:='';
        SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM seguridad.usuario WHERE cedula = i_cedula;        
        IF  v_existe='f' THEN
		SELECT count(correo) INTO oCORREO FROM seguridad.usuario WHERE correo = i_correo;  
		IF (oCORREO) > 0 THEN 
			o_return:= 'S';
		ELSE
			INSERT INTO seguridad.usuario(usuario, correo, cedula, clave, nombre, apellido, celular,
			telefono1, telefono2, id_rol, id_status, pregunta_seguridad,
			respuesta_seguridad, fecha_registro, id_user_insert, id_user_update)
			VALUES (i_usuario, i_correo, i_cedula, i_clave, i_nombre, i_apellido, i_celular,
			i_telefono1, i_telefono2, i_id_rol, i_id_status, i_pregunta_seguridad,
			i_respuesta_seguridad, current_date, 0, 0);
			o_return:= 'C';
                END IF;
                
        ELSE
		o_return:='T';        
        END IF;
        RETURN o_return;  
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION seguridad.usuario_registrar_web(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying, integer, integer, character varying, character varying)
  OWNER TO postgres;

-- Function: migrante.auditoria_general()

-- DROP FUNCTION migrante.auditoria_general();

CREATE OR REPLACE FUNCTION seguridad.auditoria_general()
  RETURNS trigger AS
$BODY$ 
 DECLARE 
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
 --  @propiedad:MPPComercio 
 --  @Autor: Gregorio Bolívar 
 --  @email: elalconxvii@gmail.com 
 --  @Fecha de Creacion: 20/05/2015 
 --  @Auditado por: Gregorio J Bolívar B 
 --  @Fecha de Modificacion: 22/05/2015
 --  @Descripción: Funcion de auditoria para todos los sistemas, con el fin de contemplar todos los procesos efectuados por los usuarios dentro del sistema. 
 --  @version: 0.6 
 --  @Blog: http://gbbolivar.wordpress.com/ 
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
   -- Variables de configuracion para la conexion con el dblink
   db_audi VARCHAR :='auditoria_myapp'; 
   ip_audi VARCHAR :='127.0.0.1'; 
   es_audi VARCHAR :='postgres'; 
   ps_audi VARCHAR :='postgres';
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
   -- Usuario de base de datos, es requerido que este usuario sea diferente a u_producto
   user_access VARCHAR := user;
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
   -- Definicion de los campos relacionados al identificador del usuario, es recomendable que toda tabla tiene que tener identificador del usuario, cuando sea insert o update
   user_id_insert VARCHAR ;
   user_id_otros  VARCHAR ;
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
   -- Esta variable es para concatenar la conexion de los valores
   conf TEXT :='';
   sqla TEXT :='';
 BEGIN
   conf='hostaddr='''||ip_audi||''' dbname='''||db_audi||''' user='''||es_audi||''' password='''||ps_audi ||'''';
 IF(TG_OP = 'INSERT')THEN
     user_id_insert = NEW.id_user_insert;
     IF(CURRENT_USER=user_access)THEN
                 sqla='INSERT INTO seg_eventos(usuario_id, host, usuario_db, name_db, esquema, entidad, proceso, new_values, entidad_id, fecha) values ('''||user_id_insert||''', '''|| inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||NEW||''' , '''||NEW.id||''' , '''|| now() ||''')';                                      
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
         ELSE                                                                                                                                                                                                                                                                                                                                                                                                                                            
                 sqla='INSERT INTO seg_eventos(host, usuario_db, name_db, esquema, entidad, proceso, new_values, entidad_id, fecha) values ('''|| inet_client_addr() ||''', '''|| USER ||''', '''|| current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||NEW||''' , '''||NEW.id||''' , '''|| now() ||''')';                                                                             
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
         END IF;                                                                                                                                                                                                                                                                                                                                                                                                                                         
 ELSE 
     user_id_otros   = OLD.id_user_update; 
     IF(TG_OP = 'UPDATE')THEN 
        IF(CURRENT_USER=user_access)THEN                                                                                                                                                                                                                                                                                                                                                                                                                
                 sqla='INSERT INTO seg_eventos(usuario_id, host, usuario_db, name_db, esquema, entidad, proceso, new_values, old_values, entidad_id, fecha) values ('''||user_id_otros||''', '''|| inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||NEW||''' , '''||OLD||''' , '''||OLD.id||''' , '''|| now() ||''')';           
        ELSE                                                                                                                                                                                                                                                                                                                                                                                                                                            
                 sqla='INSERT INTO seg_eventos(host, usuario_db, name_db, esquema, entidad, proceso, new_values, old_values, entidad_id, fecha) values ('''|| inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||NEW||''' , '''||OLD||''' , '''||OLD.id||''' , '''|| now() ||''')';                                                
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
        END IF;                                                                                                                                                                                                                                                                                                                                                                                                                                         
      END IF; 
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
      IF(TG_OP = 'DELETE')THEN 
         IF(CURRENT_USER=user_access)THEN                                                                                                                                                                                                                                                                                                                                                                                                                
                 sqla='INSERT INTO seg_eventos(usuario_id, host, usuario_db, name_db, esquema, entidad, proceso, old_values, entidad_id, fecha) values ('''||user_id_otros||''', '''|| inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||OLD||''' , '''||OLD.id||''' , '||' now() '||')';                                         
         ELSE                                                                                                                                                                                                                                                                                                                                                                                                                                            
                 sqla='INSERT INTO seg_eventos(host, usuario_db, name_db, esquema, entidad, proceso, old_values, entidad_id, fecha) values ('''||inet_client_addr() ||''', '''|| USER ||''', '''||  current_database() ||''' , '''|| current_schema() ||''' ,  '''||TG_TABLE_NAME||'''  , '''||TG_OP||''', '''||OLD||''' , '''||OLD.id||''' , '||' now() '||')';                                                                               
                                                                                                                                                                                                                                                                                                                                                                                                                                                         
         END IF;                                                                                                                                                                                                                                                                                                                                                                                                                                         
      END IF; 
 END IF; 
      PERFORM dblink_exec(''||conf||'',''||sqla||''); 
      return new; 
 END; 
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION seguridad.auditoria_general()
  OWNER TO postgres;


-- Trigger: auditoria_origen on migrante.migrante

-- DROP TRIGGER auditoria_origen ON migrante.migrante;

CREATE TRIGGER auditoria_origen
  AFTER INSERT OR UPDATE OR DELETE
  ON seguridad.usuario
  FOR EACH ROW
  EXECUTE PROCEDURE seguridad.auditoria_general();
```

Para que la auditoria funcione, hay que crear una base de datos (auditoria_myapp) donde se guardará la información y hacer respaldo con el siguiente script:

```
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 9.6.10

-- Started on 2019-02-13 11:25:44 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12391)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2133 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 187 (class 1255 OID 333493)
-- Name: resg_auditoria(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.resg_auditoria() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
 --  @propiedad: MPPComercio
 --  @Autor: Gregorio Bolívar
 --  @email: elalconxvii@gmail.com
 --  @Fecha de Creacion: 22/05/2015
 --  @Auditado por: Gregorio J Bolívar B
 --  @Fecha de Modificacion: 22/05/2015
 --  @Descripción: Funcion encargada de bloquear los registros.
 --  @version: 1.6
 --  @Blog: http://gbbolivar.wordpress.com/
BEGIN
     IF(TG_OP = 'DELETE') THEN
RAISE EXCEPTION 'No se pueden eliminar registros de auditoria';
     END IF;
     IF (TG_OP = 'UPDATE') THEN 
RAISE EXCEPTION 'No se pueden modificar registros de auditoria';
     END IF;
END;
$$;


ALTER FUNCTION public.resg_auditoria() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 185 (class 1259 OID 333494)
-- Name: seg_eventos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seg_eventos (
    id bigint NOT NULL,
    usuario_id integer,
    host character varying(60) NOT NULL,
    usuario_db character varying(60) NOT NULL,
    name_db character varying(60) NOT NULL,
    esquema character varying(60) NOT NULL,
    entidad character varying(60) NOT NULL,
    proceso character varying(20) NOT NULL,
    new_values text,
    old_values text,
    entidad_id integer NOT NULL,
    fecha timestamp without time zone NOT NULL
);


ALTER TABLE public.seg_eventos OWNER TO postgres;

--
-- TOC entry 2134 (class 0 OID 0)
-- Dependencies: 185
-- Name: TABLE seg_eventos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.seg_eventos IS 'Registrar todos los eventos relacionados a las modificaciones de registros de esta base de datos.';


--
-- TOC entry 2135 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.usuario_id IS 'Identifica el usuario del sistema que efectuo el proceso.';


--
-- TOC entry 2136 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.usuario_db; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.usuario_db IS 'Nombre del usuario del base de datos el cual se conecto y efectuo la operacion.';


--
-- TOC entry 2137 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.name_db; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.name_db IS 'Corresponde la base de datos al cual fue afectada.';


--
-- TOC entry 2138 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.esquema; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.esquema IS 'Esquema donde se efectuo la operacion.';


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.entidad; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.entidad IS 'Identifica la entidad afectada.';


--
-- TOC entry 2140 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.proceso; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.proceso IS 'Identifica la accin [UPDATE, INSERT, DELETE] efectuada al registro de la tabla actividades_especiales.';


--
-- TOC entry 2141 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.new_values; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.new_values IS 'los datos relacionadas al nuevo registro.';


--
-- TOC entry 2142 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.old_values; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.old_values IS 'Campos afectados en proceso de los registro anterior.';


--
-- TOC entry 2143 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.entidad_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.entidad_id IS 'Corresponde al identificador del registro que fue afectado.';


--
-- TOC entry 2144 (class 0 OID 0)
-- Dependencies: 185
-- Name: COLUMN seg_eventos.fecha; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.seg_eventos.fecha IS 'Corresponde la fecha de accion del evento.';


--
-- TOC entry 186 (class 1259 OID 333500)
-- Name: seg_eventos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seg_eventos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seg_eventos_id_seq OWNER TO postgres;

--
-- TOC entry 2145 (class 0 OID 0)
-- Dependencies: 186
-- Name: seg_eventos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seg_eventos_id_seq OWNED BY public.seg_eventos.id;


--
-- TOC entry 2005 (class 2604 OID 333502)
-- Name: seg_eventos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_eventos ALTER COLUMN id SET DEFAULT nextval('public.seg_eventos_id_seq'::regclass);


--
-- TOC entry 2007 (class 2606 OID 333504)
-- Name: seg_eventos seg_evento_id_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seg_eventos
    ADD CONSTRAINT seg_evento_id_pk PRIMARY KEY (id);


--
-- TOC entry 2008 (class 2620 OID 333505)
-- Name: seg_eventos resguardo_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER resguardo_auditoria BEFORE DELETE OR UPDATE ON public.seg_eventos FOR EACH ROW EXECUTE PROCEDURE public.resg_auditoria();


-- Completed on 2019-02-13 11:25:44 -04

--
-- PostgreSQL database dump complete
--
```
Esta funcionalidad no es extrictamente necesaria para el buen funcionamiento del framework. Si no se desea utilizar, 
basta con desvincular el correspondiente trigger 'auditoria_origen' de la respectiva tabla 'usuario'. 
--------------------------------------------------------------------------------------------------------------------

A partir de el commit de fecha [26/02/2018](https://gitlab.com/fdsoil/FDSoil/commit/3dc3a6be1d34059fb518fd61c649c472f5365e65) 

(Se le agregó al framework la posibilidad de asignar una página de inicio respectiva por rol (perfil) de usuario. 
Por lo tanto, hubo cambios en el esquema de seguridad de la base de datos. Estos cambios fueron ejecutados en la base de datos a travez 
de un script.sql el cual está documentado en el README.md)

```
ALTER TABLE seguridad.roles
  ADD COLUMN pag_ini_default character varying(150) NOT NULL DEFAULT 'FDSoil/admin_inicio/';

DROP FUNCTION seguridad.rol_register(integer, character varying, integer, character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION seguridad.rol_register(
    i_rol integer,
    i_descripcion character varying,
    i_pag_ini_default character varying,
    i_cadena_0 character varying,
    i_cadena_1 character varying,
    i_cadena_2 character varying,
    i_cadena_3 character varying,
    i_status integer)
  RETURNS character AS
$BODY$
DECLARE 
	o_return character;
	v_id  integer;
	v_existe boolean;
BEGIN
	o_return:='';
	IF  i_rol=0 THEN 
		SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM seguridad.roles WHERE descripcion=i_descripcion;
		IF v_existe='f' THEN
			INSERT INTO seguridad.roles( descripcion, id_status, pag_ini_default) VALUES ( i_descripcion, i_status, i_pag_ini_default);		
			SELECT id INTO v_id FROM  seguridad.roles WHERE descripcion=i_descripcion;			
			o_return:='C';	
		ELSE 
			o_return:='T';		
		END IF;
	ELSE	
		v_id:=i_rol;
		UPDATE seguridad.roles SET descripcion=i_descripcion, id_status=i_status, pag_ini_default=i_pag_ini_default WHERE id=v_id;
		DELETE FROM seguridad.rol_3 WHERE id_rol=v_id;
		DELETE FROM seguridad.rol_2 WHERE id_rol=v_id;
		DELETE FROM seguridad.rol_1 WHERE id_rol=v_id;
		DELETE FROM seguridad.rol_0 WHERE id_rol=v_id;		
		o_return:='A';			
	END IF;
	IF i_cadena_0 !='' AND (o_return = 'C' OR o_return = 'A') THEN
		SELECT seguridad.rol_0_register(v_id, i_cadena_0) INTO o_return; 
		IF i_cadena_1 !='' AND o_return = 'A' THEN
			SELECT seguridad.rol_1_register(v_id, i_cadena_1) INTO o_return; 
			IF i_cadena_2 !='' AND o_return = 'A' THEN
				SELECT seguridad.rol_2_register(v_id, i_cadena_2) INTO o_return; 
				IF i_cadena_3 !='' AND o_return = 'A' THEN
					SELECT seguridad.rol_3_register(v_id, i_cadena_3) INTO o_return; 
				END IF;							
			END IF;					
		END IF;				
	END IF;
RETURN o_return; 
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
```
