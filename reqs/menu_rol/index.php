<?php
use \FDSoil\Usuario\Rol\Menu as Menu;
use \FDSoil\DbFunc as DbFunc;

class SubIndex
{

    public function __construct($method) { self::$method(); }

    private function menuNivel1Tabla() { echo DbFunc::resultToString(Menu::menuNivel_1_list_tabla($_POST),'|',"¬"); }

    private function menuNivel1Combo() { echo DbFunc::resultToString(Menu::menuNivel_1_list_combo($_POST),'|',"¬"); }

    private function menuNivel2Tabla() { echo DbFunc::resultToString(Menu::menuNivel_2_list_tabla($_POST),'|',"¬"); }

    private function menuNivel2Combo() { echo DbFunc::resultToString(Menu::menuNivel_2_list_combo($_POST),'|',"¬"); }

    private function menuNivel3Tabla() { echo DbFunc::resultToString(Menu::menuNivel_3_list_tabla($_POST),'|',"¬"); }

}

