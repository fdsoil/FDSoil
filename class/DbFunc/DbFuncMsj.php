<?php
trait DbFuncMsj
{
    private function path() { return "../../../".$_SESSION['FDSoil']."/class/DbFunc/sql/Msj/"; }

    public function getMsj($aId) { return self::_obj()->exeQryFile( self::path()."get_msj_select.sql", $aId ); }  
    
    public function listar_MsjDB() { return self::_obj()->exeQryFile( self::path()."select_msj_db.sql", null ); }
    
    public function select_MsjDB($Post) { return self::_obj()->exeQryFile( self::path()."select_msj.sql", $Post ); }
}

