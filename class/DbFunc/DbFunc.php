<?php
namespace FDSoil;
/** FUNCIONES DE BASES DE DATOS
* @author Ernesto Jiménez <fdsoil123@gmail.com>*/
require_once("DbFuncIniRegist.php");
require_once("DbFuncMsj.php");
require_once("DbFuncResult.php");

class DbFunc
{

    use \DbFuncIniRegist, \DbFuncMsj, \DbFuncResult;

    private function _obj($db=null, $persistence=null) { return new \FDSoil\ExeQryFile($db, $persistence); }

    public function exeQryFile($fileSql, $array=[], $doAudit=false, $coment='', $db=null, $persistence=null)
    {
        return self::_obj($db, $persistence)->exeQryFile($fileSql, $array, $doAudit, $coment);
    }

    public function seeQryFile($fileSql, $array=[], $doAudit=false, $coment='', $db=null)
    {
        return self::_obj($db)->seeQryFile($fileSql, $array, $doAudit, $coment);
    }

}

