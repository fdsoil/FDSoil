<?php
namespace FDSoil\DbFunc;

interface Interfaz
{
    //public function fetchResult($result);

    public function numFields($result);
    public function numRows($result);
    public function affectedRows($result);
    public function fetchArray($result);
    public function fetchAssoc($result);
    public function fetchRow($result);
    public function fetchAll($result);
}

