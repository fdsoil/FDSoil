<?php
//session_start();
include_once('../../../'.$_SESSION['FDSoil'].'/packs/email/phpmailer.class.php');

class emailSend 
{

    function sendMail($arrEmail) 
    { 

        $mail = new phpmailer();
        $mail->PluginDir = "";
        $mail->Helo = $arrEmail['web']; //direccion web de la aplicacion
        $mail->Mailer = $arrEmail['mailer']; //le indicamos que vamos a usar un servidor smtp
        $mail->Host = $arrEmail['host']; //Asignamos a Host el nombre de nuestro servidor smtp
        $mail->SMTPAuth = $arrEmail['smtpAuth']; //Le indicamos que el servidor smtp requiere autenticacion
        $mail->Username = $arrEmail['email_origen']; //Le decimos cual es nuestro nombre de usuario y password
        $mail->Password = $arrEmail['clave_envio']; //clave del correo origen
        $mail->AddAddress($arrEmail['addAddress']);  //Indicamos cual es nuestra dirección de correo
        $mail->From = $arrEmail['email_origen']; //queremos que vea el usuario que lee nuestro correo
        $mail->FromName = $arrEmail['fromName'];//Nombre del contacto de correo desde donde se envia el mensaje
        $mail->Timeout=$arrEmail['timeout']; //el valor por defecto 10 de Timeout es un poco escaso dado que voy a usar una cuenta gratuita, por tanto lo pongo a 30 
        $mail->Subject = $arrEmail['fromName']." - ".$arrEmail['subject']; //Asignamos asunto y cuerpo del mensaje
        $mail->Body = $arrEmail['body'].$arrEmail['msj']."</br>";
        $mail->AltBody = $arrEmail['altBody']; //Definimos AltBody por si el destinatario del correo no admite email con formato html
        $exito = $mail->Send(); //se envia el mensaje, si no ha habido problemas la variable $exito tendra el valor true
        $intentos = 1;
        while ((!$exito) && ($intentos < $arrEmail['intentosTot'])) { //Si el mensaje no ha podido ser enviado se realizaran 4 intentos mas como mucho para intentar enviar el mensaje,
            sleep($arrEmail['segundos']); //cada intento se hara 5 segundos despues del anterior
                $exito = $mail->Send();
                $intentos = $intentos + 1;
        }

    }

}

?>
