<?php
namespace FDSoil;

require_once("AuditValidate.php");

class Audit
{

    use \AuditValidate;

    private function _path() { return "../../../".$_SESSION['FDSoil']."/class/Audit/sql/"; }

    /** Hace autitoría.
    * Descripción: Hacer auditoría de la transacción.
    * @param string $qry Query o sentencia(s) de SQL.
    * @param string $coment Comentario de la auditoría.*/
    function doAudit($qry, $coment)
    {
        $array['remote_addr']=$_SERVER['REMOTE_ADDR'];
        $array['remote_port']=$_SERVER['REMOTE_PORT'];
        $array['id_usuario']=(Func::isThereThisKeyInTheArray($_SESSION,'id_usuario'))?$_SESSION['id_usuario']:0;
        $array['script_filename']=$_SERVER['SCRIPT_FILENAME'];
        $array['request_method']=$_SERVER['REQUEST_METHOD'];
        $array['query']=str_replace("'",'"' , $qry);
        $array['fecha']= date("Y-m-d");
        $array['hora'] = date("H:i:s");
        $array['coment']=$coment;
        \FDSoil\DbFunc::exeQryFile(self::_path()."insert.sql", $array);
    }

    /** Audita hackeos.
    * Descripción: Hace autitoría en base de dato de intento de hackeo detectado,
    * manda correo de notificación y cierra la sesión.
    * @param array $aRequest Arreglo $_GET, $_POST o $_REQUEST que con información maliciosa.
    * @param string $msj Comentario de la auditoría.*/
    function auditarHackeo($aRequest, $msj)
    {
        if ($_SESSION['audit']){
            $str='';
            foreach ($aRequest as $key=>$value)
                $str.=$key.': '.$value.'; ';
                self::doAudit($str,$msj);
                //include_once("../../../".$_SESSION['FDSoil']."/class/email_send.class.php");
                $strFileName = "../../../".$_SESSION['myApp']."/config/app.json";
                file_exists($strFileName)?$oJSON=json_decode(file_get_contents($strFileName)):die("File nor Found " . $strFileName);
                $arrEmail=Func::pasPropOfObjToArr( $oJSON, array('email_audit'));
                $arrEmail['msj']=$msj;
                //$obj=new \emailSend();
                //$obj->sendMail($arrEmail);
            }
            header("Location: ../../../".strtolower($_SESSION['FDSoil'])."/admin_session_closed/");
            die();
    }

    public function validaReferenc()
    {
        if (!isset($_SERVER['HTTP_REFERER'])) {
            self::auditarHackeo($_SERVER,'Alerta, Están Intentando Hackear el Sistema Violando la URL...');
        }
    }

}

