<?php
use \FDSoil\Func as Func;

trait AuditValidate
{

    /** Valida entradas.
    * Descripción: Validar entradas de datos realizadas por usuario, para prevenir ataques de hacker.
    *
    * Nota: Las entradas de datos realizadas por usuarios, generalmente son $_GET, $_POST y $_REQUEST.
    * Si los parametros contienen ataques, automáticamente hace auditoría, cierra la sesión y aborta el sistema.
    * include_once 'constraintsInput.php'; (Valores no permitidos)
    *
    * @param array $aParameters Arreglo de parametros (entradas de datos), generalmente son ($_GET, $_POST y $_REQUEST).
    * @return array Arreglo de parametros debidamente validados y sin caracteres especiales.*/
    public function validateInput($aParameters)
    {
        include_once("../../../".$_SESSION['FDSoil']."/class/Audit/constraintsInput.php");
        //self::validateInputAux($aParameters,$aConstraintsSQLInjection);
//      if ($_POST['val_html'] != 'false')        
        if (!$_SESSION['val_html'] || ($_SESSION['val_html'] && !isset($_POST['val_html'])))
            self::validateInputAux($aParameters,$aConstraintsXssHTML);
        self::validateInputAux($aParameters,$aConstraintsXssJS);
        self::validateInputAux($aParameters,$aConstraintsURLCode);
        self::validateInputAux($aParameters,$aConstraintsHTMLCode);
        self::validateInputAux($aParameters,$aConstraintsHTMLCode);
        //self::validateInputAux($aParameters,$aConstraintsSQLInjection);
        return Func::removeSpecialCharacters($aParameters);
    }

    /** Auxiliar de validateInput().
    * Descripción: Método auxiliar de la función validateInput().
    * @param array $aParameters Arreglo de parametros (entradas de datos), generalmente son ($_GET, $_POST y $_REQUEST).
    * @return array Arreglo de parametros debidamente validados.*/
    private function validateInputAux($aParameters,$aConstraints)
    {
        $return=true;
        foreach ($aParameters as $key=>$value) {
            for ($i=0;$i<count($aConstraints);$i++){
                for ($j=0;$j<count(@$aConstraints[$i]);$j++)
                    $return=(stristr(strtoupper($value),$aConstraints[$i][$j]))?false:true;
                if (!$return)
                    break;
            }
            if (!$return)
                break;
        }
        if (!$return){
            $msj='ALERTA, Están Intentando Hackear el Sistema '.$_SESSION['myApp'].', por $_GET, $_POST ó $_REQUEST...';
            self::auditarHackeo($aParameters, $msj);
        }
    }

    public function validateThereIsNotGET()
    {
        if (!count($_GET)==0){
            $msj='ALERTA, Están Intentando Hackear el Sistema '.$_SESSION['myApp'].', por $_GET...';
            self::auditarHackeo($_GET, $msj);
        }
    }

    public function validateAll()                                                                                           
    {                                                                                                                        
        self::validateThereIsNotGET();                                                                                       
        self::validateInput($_POST);                                                                                         
    }

}


