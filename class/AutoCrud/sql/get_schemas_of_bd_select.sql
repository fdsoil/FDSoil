SELECT distinct table_schema, table_schema
FROM information_schema.tables 
WHERE table_schema NOT IN ('pg_catalog', 'information_schema') 
AND table_type = 'BASE TABLE' ORDER BY 1;
