SELECT table_schema, table_schema, table_name, table_name 
FROM information_schema.tables 
WHERE table_schema NOT IN ('pg_catalog', 'information_schema') 
AND table_type = 'BASE TABLE' ORDER BY 1,3
