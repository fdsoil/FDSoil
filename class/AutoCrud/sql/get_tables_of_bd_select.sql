SELECT table_name, table_name 
FROM information_schema.tables 
WHERE table_schema NOT IN ('pg_catalog', 'information_schema') 
AND table_type = 'BASE TABLE' 
AND table_schema = '{fld:bd}' 
ORDER BY 1;
