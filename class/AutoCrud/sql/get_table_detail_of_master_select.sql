SELECT A.table_name, B.column_name AS id_foreign_key, C.column_name AS id_master
FROM information_schema.table_constraints A
JOIN information_schema.key_column_usage AS B
ON A.constraint_name = B.constraint_name
JOIN information_schema.constraint_column_usage C
ON A.constraint_catalog = C.constraint_catalog
AND A.constraint_schema = C.constraint_schema
AND A.constraint_name = C.constraint_name
AND C.table_schema IN ('{fld:schema}')
AND C.table_name IN ('{fld:table}')
WHERE lower(A.constraint_type) IN ('foreign key');
