SELECT B.column_name
FROM information_schema.table_constraints AS A
JOIN information_schema.key_column_usage AS B
ON A.constraint_name = B.constraint_name
WHERE A.table_schema='{fld:schema}'
AND A.table_name='{fld:table}'
AND constraint_type = 'PRIMARY KEY'
AND B.constraint_schema='{fld:schema}';
