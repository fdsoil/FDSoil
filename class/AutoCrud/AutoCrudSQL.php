<?php

trait AutoCrudSQL
{

    private function _bldSQL()
    {
        //echo '<pre>';var_dump($this->_arr);echo '</pre>';die();
        //self::_dirBld("/class/".$this->_tableModel."/sql/");
        self::_dirBld("/class/".$this->_tableModel."/sql/".$this->_arr['table']);
        self::_bldFileGetMasterSelectSql();
        self::_bldFileRegisterMasterPlSql();
        self::_bldFileDeletePlSql();
        self::_bldFilesForeignKeyListSql();
        self::_bldFilesForeignKeyListSqlAux();
        self::_bldFileRegisterDetailPlSql();
        self::_bldFileGetDetailSelectSql();
        self::_bldFileDeleteDetailPlSql();

    }

    private function _bldFileGetMasterSelectSql()
    {

        $alias=(count($this->_arr['aTableForeignKeysAssoc'])>0)?"A":"";
        $punto=(count($this->_arr['aTableForeignKeysAssoc'])>0)?".":"";

        self::_fileBldStart("/class/".$this->_tableModel."/sql/".$this->_arr['table']."/",$this->_arr['table']."_get_select.sql");

        fputs($this->_file, "SELECT \n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            fputs($this->_file, "    ".$alias.$punto.$this->_arr['aTableStructure'][$i]['column_name']);
            if ($i<(count($this->_arr['aTableStructure'])-1))
                fputs($this->_file, ",\n");
            else
                fputs($this->_file, "\n");
        }

        for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){
            $a['schema']=$this->_arr['aTableForeignKeysAssoc'][$i]['table_schema'];
            $a['table']=$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name'];
            $aForeinKey=self::_getTableStructure($a);
            for ($j=0;$j<count($aForeinKey);$j++){
                if ($this->_arr['aTableForeignKeysAssoc'][$i]['foreign_column_name']!=$aForeinKey[$j]['column_name']){
                    fputs($this->_file, "    ,".self::_numXCha($i+1).".".$aForeinKey[$j]['column_name']);
                    fputs($this->_file, " AS des_".$a['table']."\n");
                }
            }
        }

        fputs($this->_file, "FROM ".$this->_arr['schema'].".".$this->_arr['table']." ".$alias."\n");

        if (count($this->_arr['aTableForeignKeysAssoc'])>0){
            for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){
                fputs($this->_file, "INNER JOIN ".$this->_arr['schema'].".".$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']
                    ." ".self::_numXCha($i+1));
                fputs($this->_file, " ON A.".$this->_arr['aTableForeignKeysAssoc'][$i]['column_name']."=".self::_numXCha($i+1)
                    .".".$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_column_name']."\n");
            }
        }

        fputs($this->_file, "{fld:where}\n");
        fputs($this->_file, "ORDER BY 2;\n");

        self::_fileBldEnd("/class/".$this->_tableModel."/sql/".$this->_arr['table']."/",$this->_arr['table']."_get_select.sql");

    }

    private function _bldFileRegisterMasterPlSql()
    {

        self::_fileBldStart("/class/".$this->_tableModel."/sql/".$this->_arr['table']."/",$this->_arr['table']."_register_pl.sql");

        fputs($this->_file, "SELECT ".$this->_arr['schema'].".".$this->_arr['table']."_register(\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            if (in_array($this->_arr['aTableStructure'][$i]['data_type'],
                ['serial', 'integer', 'smallint', 'double precision']))
                    fputs($this->_file, "    {fld:".$this->_arr['aTableStructure'][$i]['column_name']."}");
            else if(in_array($this->_arr['aTableStructure'][$i]['data_type'],
                ['character', 'character varying', 'text', 'date', 'time without time zone', 'boolean']))
                    fputs($this->_file, "    '{fld:".$this->_arr['aTableStructure'][$i]['column_name']."}'");
            if ($i<(count($this->_arr['aTableStructure'])-1))
                fputs($this->_file, ",\n");
            else
                fputs($this->_file, "\n);");
        }

        self::_fileBldEnd("/class/".$this->_tableModel."/sql/".$this->_arr['table']."/",$this->_arr['table']."_register_pl.sql");

    }

    private function _bldFileDeletePlSql()
    {

        self::_fileBldStart("/class/".$this->_tableModel."/sql/".$this->_arr['table']."/",$this->_arr['table']."_delete_pl.sql");

        fputs($this->_file, "SELECT ".$this->_arr['schema'].".".$this->_arr['table']."_delete({fld:".$this->_arr['aTableTablePrimaryKey'][0]."});");

        self::_fileBldEnd("/class/".$this->_tableModel."/sql/".$this->_arr['table']."/",$this->_arr['table']."_delete_pl.sql");

    }

    private function _bldFilesForeignKeyListSql()
    {

        for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){

            $a['schema']=$this->_arr['aTableForeignKeysAssoc'][$i]['table_schema'];
            $a['table']=$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name'];
            $aForeinKey=self::_getTableStructure($a);

            self::_fileBldStart("/class/".$this->_tableModel."/sql/".$this->_arr['table']."/",$a['table']."_list_select.sql");

            fputs($this->_file, "SELECT ");

            for ($j=0;$j<count($aForeinKey);$j++){
                fputs($this->_file, $aForeinKey[$j]['column_name']);
                if ($j<(count($aForeinKey)-1))
                    fputs($this->_file, ", ");
                else
                    fputs($this->_file, " ");
            }

            fputs($this->_file, "FROM ".$a['schema'].".".$a['table']." ORDER BY 2;");

            self::_fileBldEnd("/class/".$this->_tableModel."/sql/".$this->_arr['table']."/",$a['table']."_list_select.sql");

        }

    }

    private function _bldFilesForeignKeyListSqlAux()
    {

        $arr = [];

        for ($k=0;$k<count($this->_arr['aTableDetailOfMaster']);$k++){

            $arr=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$k]['table_name']]);

            $a = [];
            //echo '<pre>';var_dump($arr);echo '</pre>';die();
            for ($i=0;$i<count($arr['aTableForeignKeysAssoc']);$i++){
                self::_dirBld("/class/".$this->_tableModel."/sql/".$arr['table_detail']);

                $a['schema']=$arr['aTableForeignKeysAssoc'][$i]['table_schema'];
                $a['table']=$arr['aTableForeignKeysAssoc'][$i]['foreign_table_name'];

                $aForeinKey=self::_getTableStructure($a);

                self::_fileBldStart( "/class/".$this->_tableModel."/sql/".$arr['table_detail']."/",$a['table']."_list_select.sql" );

                fputs($this->_file, "SELECT ");

                for ($j=0;$j<count($aForeinKey);$j++){
                    fputs($this->_file, $aForeinKey[$j]['column_name']);
                    if ($j<(count($aForeinKey)-1))
                        fputs($this->_file, ", ");
                    else
                        fputs($this->_file, " ");
                }

                fputs($this->_file, "FROM ".$a['schema'].".".$a['table']." ORDER BY 2;");

                self::_fileBldEnd( "/class/".$this->_tableModel."/sql/".$arr['table_detail']."/",$a['table']."_list_select.sql" );

            }

        }

    }

    private function _bldFileRegisterDetailPlSql()
    {

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){

            $arrDet=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$i]['table_name']]);
            //echo '<pre>';var_dump($arrDet);echo '</pre>';die();//table_name
            self::_dirBld("/class/".$this->_tableModel."/sql/".$arrDet['table_detail']);
            self::_fileBldStart( "/class/".$this->_tableModel."/sql/".$arrDet['table_detail']."/",$arrDet['table_detail']."_register_pl.sql" );

            fputs($this->_file, "SELECT ".$arrDet['schema'].".".$arrDet['table_detail']."_register(\n");

            for ( $j = 0; $j < count( $arrDet['aTableStructure'] ) ; $j++ ){

                if ($arrDet['aTableStructure'][$j]['column_name']===$arrDet['aTableTablePrimaryKey'][0])
                    $arrDet['aTableStructure'][$j]['column_name']="id_".$arrDet['table_detail'];

                if (in_array($arrDet['aTableStructure'][$j]['data_type'], ['serial', 'integer', 'smallint', 'double precision']))
                        fputs($this->_file, "    {fld:".$arrDet['aTableStructure'][$j]['column_name']."}");
                else if(in_array($arrDet['aTableStructure'][$j]['data_type'],
                ['character', 'character varying', 'text', 'date', 'time without time zone', 'boolean']))
                    fputs($this->_file, "    '{fld:".$arrDet['aTableStructure'][$j]['column_name']."}'");

                if ( $j < ( count($arrDet['aTableStructure'] )-1 ) )
                    fputs($this->_file, ",\n");
                else
                    fputs($this->_file, "\n);");

            }

            self::_fileBldEnd("/class/".$this->_tableModel."/sql/".$arrDet['table_detail']."/",$arrDet['table_detail']."_register_pl.sql");

        }

    }

    private function _bldFileGetDetailSelectSql()
    {
        //$this->seeArray($this->_arr);

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){

            $arrDet=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$i]['table_name']]);

            $alias=(count($arrDet['aTableForeignKeysAssoc'])>0)?"A":"";
            $punto=(count($arrDet['aTableForeignKeysAssoc'])>0)?".":"";

            self::_fileBldStart( "/class/".$this->_tableModel."/sql/".$arrDet['table_detail']."/",$arrDet['table_detail']."_get_select.sql" );

            fputs($this->_file, "SELECT \n");

            for ( $j = 0; $j < count( $arrDet['aTableStructure'] ) ; $j++ ){
                fputs($this->_file, "    ".$alias.$punto.$arrDet['aTableStructure'][$j]['column_name']);
                if ( $j < ( count($arrDet['aTableStructure'] ) -1 ) )
                    fputs($this->_file, ",\n");
                else
                    fputs($this->_file, "\n");
            }

            for ( $j = 0; $j < count($arrDet['aTableForeignKeysAssoc'] ); $j++){
                $a['schema'] = $arrDet['aTableForeignKeysAssoc'][$j]['table_schema'];
                $a['table'] = $arrDet['aTableForeignKeysAssoc'][$j]['foreign_table_name'];
                $aForeinKey = self::_getTableStructure($a);
                for ( $k = 0; $k < count( $aForeinKey ); $k++ ){
                    if ($arrDet['aTableForeignKeysAssoc'][$j]['foreign_column_name']!=$aForeinKey[$k]['column_name']){
                        fputs($this->_file, "    ,".self::_numXCha($j+1).".".$aForeinKey[$k]['column_name']);
                        fputs($this->_file, " AS des_".$a['table']."\n");
                    }
                }
            }

            fputs($this->_file, "FROM ".$arrDet['schema'].".".$arrDet['table_detail']." ".$alias."\n");

             if ( count( $arrDet['aTableForeignKeysAssoc'] )> 0 ){
                 for ( $j = 0; $j < count( $arrDet['aTableForeignKeysAssoc']) ; $j++){
                     fputs($this->_file, "INNER JOIN ".$arrDet['schema'].".".$arrDet['aTableForeignKeysAssoc'][$j]['foreign_table_name']." ".self::_numXCha($j+1));
                     fputs($this->_file, " ON A.".$arrDet['aTableForeignKeysAssoc'][$j]['column_name']."=".self::_numXCha($j+1)
                             .".".$arrDet['aTableForeignKeysAssoc'][$j]['foreign_column_name']."\n");
                 }
             }

             fputs($this->_file, "WHERE $alias$punto".$arrDet['aTableForeignKeysAssocMasterDetail'][0]['column_name']." = {fld:".$arrDet['aTableTablePrimaryKey'][0]."}\n");
             fputs($this->_file, "ORDER BY 3;\n");

               self::_fileBldEnd( "/class/".$this->_tableModel."/sql/".$arrDet['table_detail']."/",$arrDet['table_detail']."_get_select.sql" );
           }
       }

       private function _bldFileDeleteDetailPlSql()
       {

           for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){

               $arrDet=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$i]['table_name']]);

               self::_fileBldStart( "/class/".$this->_tableModel."/sql/".$arrDet['table_detail']."/",$arrDet['table_detail']."_delete_pl.sql" );

               fputs($this->_file, "SELECT ".$arrDet['schema'].".".$arrDet['table_detail']."_delete({fld:".$arrDet['aTableTablePrimaryKey'][0]."});");

               self::_fileBldEnd( "/class/".$this->_tableModel."/sql/".$arrDet['table_detail']."/",$arrDet['table_detail']."_delete_pl.sql" );

           }

       }

}
