<?php
//https://developer.mozilla.org/es/docs/Web/API/WindowBase64/Base64_codificando_y_decodificando
trait AutoCrudModuleAuxAjax
{

    private function _bldModuleAuxAjax()
    {
        self::_dirBld("modulos/".$this->_arr['table']."_aux/");
        self::_bldFileIndexPhpAuxAjax();
        self::_bldFileSolapa0PhpAuxAjax();
        self::_bldFileSolapaNPhpAuxAjax();
        self::_bldFileViewHtmlAuxAjax();
        self::_bldFileSolapa0HtmlAuxAjax();
        self::_bldFileSolapaNHtmlAuxAjax();
        self::_dirBld("modulos/".$this->_arr['table']."_aux/js");
        self::_bldFileJsIncludeJsonAuxAjax();
        self::_bldFileJsInicioJsAuxAjax();
        self::_bldFileJsValidarJsAuxAjax();
        self::_bldFileJsRequestJsAjax();
        self::_bldFileJsValTabJsAuxAjax();
        self::_bldFileJsReqTabJsAuxAjax();
        self::_bldFileJsTblTabJsAuxAjax();
        self::_bldFileJsIndexPhpAuxAjax();
    }

    private function _bldFileIndexPhpAuxAjax()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/", "index.php" );

        fputs($this->_file,"<?php\n");
        //fputs($this->_file,"use \\".$_SESSION['FDSoil']."\XTemplate as XTemplate;\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\DbFunc as DbFunc;\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\Func as Func;\n\n");

        for ($i=0;$i<=count($this->_arr['aTableDetailOfMaster']);$i++)
            fputs($this->_file, "require_once(__DIR__.\"/Solapa".$i.".php\");\n");

        fputs($this->_file,"\n");
        fputs($this->_file,"class SubIndex\n");
        fputs($this->_file,"{\n    use");

        for ($i=0;$i<=count($this->_arr['aTableDetailOfMaster']);$i++)
            fputs($this->_file, " Solapa".$i.($i<count($this->_arr['aTableDetailOfMaster'])?",":";\n\n"));

        fputs($this->_file,"    public function execute()\n");
        fputs($this->_file,"    {\n");
        fputs($this->_file,"        \$aView['include'] = Func::getFileJSON(__DIR__.\"/js/include.json\");\n");
        fputs($this->_file,"        \$aView['userData'] = Func::usuarioData();\n");
        fputs($this->_file,"        \$aView['load'] = array_key_exists('id', \$_POST)?\$_POST['id']:0;\n");
        fputs($this->_file,"        \$xtpl = new \\".$_SESSION['FDSoil']."\XTemplate(__DIR__.\"/view.html\");\n");
        fputs($this->_file,"        Func::appShowId(\$xtpl);\n");

        fputs($this->_file,"        if (!array_key_exists('id', \$_POST)) {\n");

        for ($i=1;$i<=count($this->_arr['aTableDetailOfMaster']);$i++)
            fputs($this->_file,"            \$xtpl->assign('TAB_NONE_BLOCK$i', 'none');\n");

        fputs($this->_file,"            \$xtpl->assign('BOTONES_NONE_BLOCK', 'none');\n");
        fputs($this->_file,"        }\n");

        for ($i=0;$i<=count($this->_arr['aTableDetailOfMaster']);$i++)
            fputs($this->_file, "        \$aSolapa[".$i."] = self::_solapa".$i."();\n");

        fputs($this->_file,"        Func::bldSolapas(\$xtpl, \$aSolapa);\n");
        fputs($this->_file,"        Func::btnsPutPanel( \$xtpl, [");
        fputs($this->_file,"[\"btnName\" => \"Return\", \"btnBack\" => \"".$this->_arr['table']."\"],\n");
        fputs($this->_file, str_repeat(" ",44));
        fputs($this->_file,"[\"btnName\" => \"Save\", \"btnClick\"=> \"valEnvio(1);\"],\n");
        fputs($this->_file, str_repeat(" ",44));
        fputs($this->_file,"[\"btnName\" => \"Close\", \"btnClick\"=> \"valEnvio(2);\", \"btnLabel\" => \"".$this->_tableModel."\", \"btnDisplay\" => \"none\"]]);\n");

        fputs($this->_file,"        \$xtpl->parse('main');\n");
        fputs($this->_file,"        \$aView['content'] = \$xtpl->out_var('main');\n");
        fputs($this->_file,"        return \$aView;\n");
        fputs($this->_file,"    }\n");
        fputs($this->_file,"}\n");
        fputs($this->_file,"\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/", "index.php" );

    }

    private function _bldFileSolapa0PhpAuxAjax()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/", "Solapa0.php" );

        fputs($this->_file,"<?php\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\XTemplate as XTemplate;\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\DbFunc as DbFunc;\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\Func as Func;\n");
        fputs($this->_file,"use \\".$this->_dirLog."\\".$this->_tableModel." as ".$this->_tableModel.";\n\n");

            fputs($this->_file,"trait Solapa0\n{\n");
            fputs($this->_file,"    private function _solapa0()\n    {\n");
            fputs($this->_file,"        \$aRegist = array_key_exists('id', \$_POST) ?\n");
            fputs($this->_file,"            DbFunc::fetchAssoc($this->_tableModel::".$this->_tableMetodo."Get('REGIST')) :\n");
            fputs($this->_file,"                \\".$_SESSION['FDSoil']."\DbFunc::iniRegist('".$this->_arr['table']."','".$this->_arr['schema']."');\n");
            fputs($this->_file,"        \$xtpl = new \\".$_SESSION['FDSoil']."\XTemplate(__DIR__.\"/solapa0.html\");\n");
            fputs($this->_file,"        Func::appShowId(\$xtpl);\n");
            fputs($this->_file,"        \$xtpl->assign('ID', \$aRegist['id']);\n");

            for ($i=0;$i<count($this->_arr['aTableStructure']);$i++) {

                if ($this->_arr['aTableStructure'][$i]['column_name']!=$this->_arr['aTableTablePrimaryKey'][0]
                && !self::_inMatrix($this->_arr['aTableStructure'][$i]['column_name'], $this->_arr['aTableForeignKeysAssoc'] )) {

                    fputs($this->_file, "        \$xtpl->assign('".strtoupper($this->_arr['aTableStructure'][$i]['column_name']));

                    if ($this->_arr['aTableStructure'][$i]['data_type']=='boolean') {
                        fputs($this->_file,"_CHK', \$this->_aRegist['".$this->_arr['aTableStructure'][$i]['column_name']
                            ."'] === 't' ? 'checked' : '');\n");
                    } else if ($this->_arr['aTableStructure'][$i]['data_type']=='date') {
                        fputs($this->_file,"', Func::change_date_format(\$aRegist['".$this->_arr['aTableStructure'][$i]['column_name']
                            ."']));\n");
                    } else if ($this->_arr['aTableStructure'][$i]['data_type']=='time without time zone') {
                        fputs($this->_file,"', Func::timeMilitarToNormal(\$aRegist['".$this->_arr['aTableStructure'][$i]['column_name']
                            ."']));\n");
                    } else {

                        fputs($this->_file,"', \$aRegist['".$this->_arr['aTableStructure'][$i]['column_name']."']);\n");

                        for ($j=0;$j<count($this->_arr['aTableUniqueConstraint']);$j++)

                            if ($this->_arr['aTableStructure'][$i]['column_name']==$this->_arr['aTableUniqueConstraint'][$j]['column_name']) {

                                fputs($this->_file, "        \$xtpl->assign('"
                                    .strtoupper($this->_arr['aTableStructure'][$i]['column_name'])."_READONLY', ");
                                fputs($this->_file, "array_key_exists('".$this->_arr['aTableTablePrimaryKey'][0]."', \$_POST) ? 'readonly' : '');\n");
                            }
                    }
                }
            }

            for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){
                $tableMetodo = self::_strCamel($this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name'], 1);
                $FOREIGNNAME = strtoupper($this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']);
                fputs($this->_file, "        \$result = $this->_tableModel::".$tableMetodo."List();\n");
                fputs($this->_file, "        while (\$row = DbFunc::fetchRow(\$result)) {\n");
                fputs($this->_file, "            \$xtpl->assign('ID_".$FOREIGNNAME."', \$row[0]);\n");
                fputs($this->_file, "            \$xtpl->assign('DES_".$FOREIGNNAME."', \$row[1]);\n");
                fputs($this->_file, "            \$xtpl->assign('SELECTED_".$FOREIGNNAME."', (\$aRegist['");
                fputs($this->_file, $this->_arr['aTableForeignKeysAssoc'][$i]['column_name']."'] == ");
                fputs($this->_file, "\$row[0]) ? 'selected' : '');\n            \$xtpl->parse('main.");
                fputs($this->_file, $this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."');\n");
                fputs($this->_file, "        }\n");
            }

            fputs($this->_file,"        \$xtpl->parse('main');\n");
            fputs($this->_file,"        return \$xtpl->out_var('main');\n");
            fputs($this->_file,"    }\n");
            fputs($this->_file,"}\n");
            fputs($this->_file,"\n");

            self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/", "Solapa0.php" );

    }

    private function _bldFileSolapaNPhpAuxAjax()
    {

        $arrDet = [];
        $n=0;

        for ($i=1;$i<=count($this->_arr['aTableDetailOfMaster']);$i++){

            $n=$i-1;
            self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/", "Solapa".$i.".php" );

            fputs($this->_file,"<?php\n");
            fputs($this->_file,"use \\".$_SESSION['FDSoil']."\DbFunc as DbFunc;\n");
            fputs($this->_file,"use \\".$_SESSION['FDSoil']."\Func as Func;\n");
            fputs($this->_file,"use \\".$this->_dirLog."\\".$this->_tableModel."\\"
                .self::_strCamel($this->_arr['aTableDetailOfMaster'][$i-1]['table_name'])." as "
                        .self::_strCamel($this->_arr['aTableDetailOfMaster'][$i-1]['table_name']).";\n\n");

            fputs($this->_file,"trait Solapa".$i."\n{\n");
            fputs($this->_file,"    private function _solapa".$i."()\n    {\n");
            fputs($this->_file,"        \$xtpl = new \\".$_SESSION['FDSoil']."\XTemplate(__DIR__.\"/solapa".$i.".html\");\n");
            fputs($this->_file,"        Func::appShowId(\$xtpl);\n");
            //fputs($this->_file,"        \$obj = new ".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i-1]['table_name'])."();\n");

            $arrDet=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$n]['table_name']]);

            for ($z=0;$z<count($arrDet['aTableForeignKeysAssoc']);$z++){
                $tableMetodo = self::_strCamel($arrDet['aTableForeignKeysAssoc'][$z]['foreign_table_name'], 1);
                $FOREIGNNAME = strtoupper($arrDet['aTableForeignKeysAssoc'][$z]['foreign_table_name']);
                fputs($this->_file, "        \$result = ".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i-1]['table_name'])."::".$tableMetodo."List();\n");
                fputs($this->_file, "        while (\$row = DbFunc::fetchRow(\$result)) {\n");
                fputs($this->_file, "            \$xtpl->assign('ID_".$FOREIGNNAME."', \$row[0]);\n");
                fputs($this->_file, "            \$xtpl->assign('DES_".$FOREIGNNAME."', \$row[1]);\n");
                fputs($this->_file, "            \$xtpl->parse('main.");
                fputs($this->_file, $arrDet['aTableForeignKeysAssoc'][$z]['foreign_table_name']."');\n        }\n");
            }

            $tableMetodo = self::_strCamel($arrDet['table_detail'], 1);
            fputs($this->_file,"        if (array_key_exists('".$arrDet['aTableTablePrimaryKey'][0]."', \$_POST)) {\n");
            fputs($this->_file,"            \$matrix = ".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i-1]['table_name'])."::".$tableMetodo."Get();\n");
            fputs($this->_file,"            \$classTR = 'lospare';\n");
            fputs($this->_file,"            foreach (\$matrix as \$arr) {\n");
            fputs($this->_file,"                \$xtpl->assign('CLASS_TR', \$classTR);\n");

            for ( $j = 0; $j < count( $arrDet['aTableStructure'] ) ; $j++ ) {
                if ( $arrDet['aTableStructure'][$j]['column_name'] !== $arrDet['aTableForeignKeysAssocMasterDetail'][0]['column_name']
                && !self::_inMatrix($arrDet['aTableStructure'][$j]['column_name'], $arrDet['aTableForeignKeysAssoc'] ) ) {
                    fputs($this->_file,"                \$xtpl->assign('".strtoupper($arrDet['aTableStructure'][$j]['column_name'])
                        ."', \$arr['".$arrDet['aTableStructure'][$j]['column_name']."']);\n");
                }
            }

            for ( $j = 0; $j < count( $arrDet['aTableForeignKeysAssoc']); $j++ ) {
                fputs($this->_file,"                \$xtpl->assign('".strtoupper($arrDet['aTableForeignKeysAssoc'][$j]['column_name'])
                    ."', \$arr['".$arrDet['aTableForeignKeysAssoc'][$j]['column_name']."']);\n");
                 fputs($this->_file,"                \$xtpl->assign('DES_"
                     .strtoupper($arrDet['aTableForeignKeysAssoc'][$j]['foreign_table_name'])."', \$arr['des_"
                         .$arrDet['aTableForeignKeysAssoc'][$j]['foreign_table_name']."']);\n");
            }

            fputs($this->_file,"                \$xtpl->parse('main.tab_".$arrDet['table_detail']."');\n");
            fputs($this->_file,"                \$classTR = (\$classTR == 'losnone') ? 'lospare' : 'losnone';\n");
            fputs($this->_file,"            }\n");
            fputs($this->_file,"        }\n");
            fputs($this->_file,"        \$xtpl->parse('main');\n");
            fputs($this->_file,"        return \$xtpl->out_var('main');\n    }\n}\n\n");

            self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/", "Solapa".$i.".php" );

        }

    }

    private function _bldFileViewHtmlAuxAjax()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/", "view.html" );

        fputs($this->_file,"<!-- BEGIN: main -->\n");
        fputs($this->_file, "<script src=\"../../../../../{FDSOIL}/js/calendar.js\"></script>\n");
        fputs($this->_file, "<div id='sub_titulo'>Registro de ".self::_strLabel($this->_arr['table'])."</div>\n");
        fputs($this->_file, "<div id=\"panel\" style=\"display: block; clear:both;\">\n");
        fputs($this->_file, "    <div class=\"halfmoon\" id =\"div_navegacion\">\n");
        fputs($this->_file, "        <ul>\n");
        fputs($this->_file, "            <li style=\"display:\">\n");
        fputs($this->_file, "                <a id=\"Tab0\" href=\"javascript:Tab(0,".(count($this->_arr['aTableDetailOfMaster'])+1));
        fputs($this->_file, ");botonGuardarOnOff(0);\" style=\"display:\">Datos Básicos\n");
        fputs($this->_file, "                    <img src=\"../../../{APPORG}/img/s_okay.png\" width=\"10\" height=\"10\" id=\"chk0\"");
        fputs($this->_file, " style=\"style:hidden\" border=0>\n");
        fputs($this->_file, "                </a>\n            </li>\n");

        $n=0;
        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){
            $n=$i+1;
            fputs($this->_file, "            <li style=\"display:\">\n");
            fputs($this->_file, "                <a id=\"Tab".$n."\" href=\"javascript:Tab(".$n.",".(count($this->_arr['aTableDetailOfMaster'])+1));
            fputs($this->_file, ");botonGuardarOnOff(".$n.");\" style=\"display:{TAB_NONE_BLOCK".$n."}\">");
            fputs($this->_file, self::_strLabel($this->_arr['aTableDetailOfMaster'][$i]['table_name'])."\n");
            fputs($this->_file, "                    <img src=\"../../../{APPORG}/img/s_okay.png\" width=\"10\" height=\"10\" id=\"chk".$n."\"");
            fputs($this->_file, " style=\"style:hidden\" border=0>\n");
            fputs($this->_file, "                </a>\n            </li>\n");
        }

        fputs($this->_file, "        </ul>\n    </div>\n");
        fputs($this->_file, "    <div id=\"botones_solapa\">\n");
        fputs($this->_file, "        <table id=\"botones\" align=\"center\" style=\"display:{BOTONES_NONE_BLOCK}\">\n");
        fputs($this->_file, "            <tr>\n                <td>\n");
        fputs($this->_file, "                    <input id=\"id_regresar\"\n");
        fputs($this->_file, "                           type=\"button\"\n");
        fputs($this->_file, "                           value=\"Atras\"\n");
        fputs($this->_file, "                           onClick=\"javascript:regresarTab(".(count($this->_arr['aTableDetailOfMaster'])+1));
        fputs($this->_file, ");regresarTabBotonGuardarOnOff(".(count($this->_arr['aTableDetailOfMaster'])+1).");\">\n");
        fputs($this->_file, "                </td>\n                <td>\n");
        fputs($this->_file, "                    <input id=\"id_seguir\"\n");
        fputs($this->_file, "                           type=button\n");
        fputs($this->_file, "                           value=\"Adelante\"\n");
        fputs($this->_file, "                           onClick=\"javascript:segirTab(".(count($this->_arr['aTableDetailOfMaster'])+1));
        fputs($this->_file, ");segirBotonGuardarOnOff(".(count($this->_arr['aTableDetailOfMaster'])+1).");\">\n");
        fputs($this->_file, "                </td>\n            <tr>\n        </table>\n    </div>\n");
        fputs($this->_file, "    <div id=\"div0\" class=\"divSolapa\" align=\"center\" title=\"...\"");
        fputs($this->_file, " label=\"t\" style=\"display:block\">{SOLAPA_0}</div>\n");

        for ($i=1;$i<=count($this->_arr['aTableDetailOfMaster']);$i++){
           fputs($this->_file, "    <div id=\"div".$i."\" class=\"divSolapa\" align=\"center\" title=\"...\"");
           fputs($this->_file, " label=\"t\" style=\"display:none\">{SOLAPA_".$i."}</div>\n");
        }

        fputs($this->_file, "</div>\n{BUTTONS_PANEL}\n<!-- END: main -->");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/", "view.html" );

    }

    private function _bldFileSolapa0HtmlAuxAjax()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/", "solapa0.html" );

        fputs($this->_file,"<!-- BEGIN: main -->\n");
        fputs($this->_file, "    <div align=\"center\">\n");
        fputs($this->_file, "<input type=\"hidden\" name=\"".$this->_arr['aTableTablePrimaryKey'][0]."\"");
        fputs($this->_file," id=\"".$this->_arr['aTableTablePrimaryKey'][0]."\" value='{"
            .strtoupper($this->_arr['aTableTablePrimaryKey'][0])."}'>\n");
        fputs($this->_file, "        <table class=\"tabla_grid\" style=\"width: 50%\">\n");
        fputs($this->_file, "            <tr>\n");
        fputs($this->_file, "                <th colspan=\"2\">Datos Básicos</th>\n");
        fputs($this->_file, "            </tr>\n");

        $this->_bldSctructureHtml($this->_file, $this->_arr);

        fputs($this->_file, "        </table>\n");
        fputs($this->_file, "    </div>\n");
        fputs($this->_file, "<!-- END: main -->");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/", "solapa0.html" );

    }

    private function _bldFileSolapaNHtmlAuxAjax()
    {

        $n=0;

        for ($k=0;$k<count($this->_arr['aTableDetailOfMaster']);$k++){

            $n=$k+1;
            $tableModel=$this->_strCamel($this->_arr['aTableDetailOfMaster'][$k]['table_name']);
            $label=self::_strLabel($this->_arr['aTableDetailOfMaster'][$k]['table_name']);
            $this->_arrDet=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$k]['table_name']]);

            self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/", "solapa".$n.".html" );

            fputs($this->_file,"<!-- BEGIN: main -->\n");
            fputs($this->_file,"<div  align=\"center\">\n");
            fputs($this->_file,"    <div id=\"sub_titulo\">$label</div>\n");
            fputs($this->_file,"    <div>\n");
            fputs($this->_file,"        <table style=\"cursor:pointer;\" onclick=\"onOffPanel".$tableModel."();\">\n");
            fputs($this->_file,"            <tr>\n");
            fputs($this->_file,"                <td>\n");
            fputs($this->_file,"                    <div class=\"subtitulo_datos\">Agregar Información</div>\n");
            fputs($this->_file,"                </td>\n");
            fputs($this->_file,"                <td>\n");
            fputs($this->_file,"                    <div id=\"id_img_".$this->_arr['aTableDetailOfMaster'][$k]['table_name']."\" title=\"Mostrar Panel\" class=\"img_show\"></div>\n");
            fputs($this->_file,"                </td>\n");
            fputs($this->_file,"            </tr>\n");
            fputs($this->_file,"        </table>\n");
            fputs($this->_file,"    </div>\n");
            fputs($this->_file,"    <div id=\"id_panel_".$this->_arr['aTableDetailOfMaster'][$k]['table_name']."\" style=\"display: none;\">\n");
            fputs($this->_file,"        <input id=\"id_".$this->_arr['aTableDetailOfMaster'][$k]['table_name']."\" name=\"id_".$this->_arr['aTableDetailOfMaster'][$k]['table_name']."\" value=\"0\" type=\"hidden\">\n");
            fputs($this->_file,"        <table class=\"tabla_grid\" style=\"width: 50%\">\n");
            fputs($this->_file,"            <tr>\n");
            fputs($this->_file,"                <th colspan=\"2\">Datos de Detalles</th>\n");
            fputs($this->_file,"            </tr>\n");

            $this->_bldSctructureHtml($this->_file, $this->_arrDet);

            fputs($this->_file,"        </table>\n");
            fputs($this->_file,"        <br/>\n");

            fputs($this->_file,"        <button id=\"addReg\" type=\"button\" onClick=\"valEnvio$tableModel();\">\n");
            fputs($this->_file,"            <img class=\"accion\" src=\"../../../{APPORG}/img/add.png\" align=\"absmiddle\" border=\"0\" > Agregar\n");
            fputs($this->_file,"        </button>\n");
         
            fputs($this->_file,"        <br/><br/>\n");
            fputs($this->_file,"    </div>\n");
            fputs($this->_file,"    <table id=\"id_tab_".$this->_arr['aTableDetailOfMaster'][$k]['table_name']."\" class=\"tabla_grid\" width=\"100%\">\n");
            fputs($this->_file,"        <thead>\n");
            fputs($this->_file,"            <tr>\n");

            for ($m=0;$m<count($this->_arrDet['aTableStructure']);$m++){
                if ($this->_arrDet['aTableStructure'][$m]['column_name'] !== $this->_arrDet['aTableTablePrimaryKey'][0]
                && $this->_arrDet['aTableStructure'][$m]['column_name'] != $this->_arrDet['aTableForeignKeysAssocMasterDetail'][0]['column_name']
                && !self::_inMatrix($this->_arrDet['aTableStructure'][$m]['column_name'], $this->_arrDet['aTableForeignKeysAssoc'])
                && $this->_arrDet['aTableStructure'][$m]['data_type'] !== 'text')
                    fputs($this->_file,"                <th>".$this->_strCamel($this->_arrDet['aTableStructure'][$m]['column_name'])."</th>\n");
            }

            for ($i=0;$i<count($this->_arrDet['aTableForeignKeysAssoc']);$i++)
                fputs($this->_file,"                <th>".$this->_strCamel($this->_arrDet['aTableForeignKeysAssoc'][$i]['foreign_table_name'])."</th>\n");

            for ($m=0;$m<count($this->_arrDet['aTableStructure']);$m++){
                if ($this->_arrDet['aTableStructure'][$m]['column_name'] !== $this->_arrDet['aTableTablePrimaryKey'][0]
                && $this->_arrDet['aTableStructure'][$m]['column_name'] != $this->_arrDet['aTableForeignKeysAssocMasterDetail'][0]['column_name']
                && !self::_inMatrix($this->_arrDet['aTableStructure'][$m]['column_name'], $this->_arrDet['aTableForeignKeysAssoc'])
                && $this->_arrDet['aTableStructure'][$m]['data_type'] === 'text')
                    fputs($this->_file,"                <th>".$this->_strCamel($this->_arrDet['aTableStructure'][$m]['column_name'])."</th>\n");
            }

            fputs($this->_file,"                <th>Acción(es)</th>\n");
            fputs($this->_file,"            </tr>\n");
            fputs($this->_file,"        </thead>\n");
            fputs($this->_file,"        <tbody>\n");
            fputs($this->_file,"            <!-- BEGIN: tab_".$this->_arrDet['table_detail']." -->\n");
            fputs($this->_file,"            <tr id={".strtoupper($this->_arrDet['aTableTablePrimaryKey'][0])."} class={CLASS_TR}>\n");

            for ( $j = 0; $j < count( $this->_arrDet['aTableStructure'] ) ; $j++ ) {
                if ( !in_array( $this->_arrDet['aTableStructure'][$j]['column_name'],
                [ $this->_arrDet['aTableForeignKeysAssocMasterDetail'][0]['column_name'], $this->_arrDet['aTableTablePrimaryKey'][0] ] )
                && !self::_inMatrix($this->_arrDet['aTableStructure'][$j]['column_name'], $this->_arrDet['aTableForeignKeysAssoc'] )
                && $this->_arrDet['aTableStructure'][$j]['data_type'] !== 'text') {
                    fputs($this->_file,"                <td>{".strtoupper($this->_arrDet['aTableStructure'][$j]['column_name'])."}</td>\n");
                }
            }

            for ( $j = 0; $j < count( $this->_arrDet['aTableForeignKeysAssoc']); $j++ ) {
                 fputs($this->_file,"                <td id=\"{".strtoupper($this->_arrDet['aTableForeignKeysAssoc'][$j]['column_name'])."}\">{DES_".strtoupper($this->_arrDet['aTableForeignKeysAssoc'][$j]['foreign_table_name'])."}</td>\n");
            }

            for ( $j = 0; $j < count( $this->_arrDet['aTableStructure'] ) ; $j++ ) {
                if ( !in_array( $this->_arrDet['aTableStructure'][$j]['column_name'],
                [ $this->_arrDet['aTableForeignKeysAssocMasterDetail'][0]['column_name'], $this->_arrDet['aTableTablePrimaryKey'][0] ] )
                && !self::_inMatrix($this->_arrDet['aTableStructure'][$j]['column_name'], $this->_arrDet['aTableForeignKeysAssoc'] )
                && $this->_arrDet['aTableStructure'][$j]['data_type'] === 'text') {
                    fputs($this->_file,"                <td>{".strtoupper($this->_arrDet['aTableStructure'][$j]['column_name'])."}</td>\n");
                }
            }

            fputs($this->_file,"                <td>\n");
            fputs($this->_file,"                    <img class=\"accion\"\n");
            fputs($this->_file,"                        src=\"../../../../../{APPORG}/img/edit.png\"\n");
            fputs($this->_file,"                        title=\"Editar datos...\"\n");
            fputs($this->_file,"                        onclick=\"edit$tableModel(this);valBtnClose();\">\n");
            fputs($this->_file,"                    <img class=\"accion\"\n");
            fputs($this->_file,"                        src=\"../../../../../{APPORG}/img/cross.png\"\n");
            fputs($this->_file,"                        title=\"Eliminar/Borrar datos...\"\n");
            fputs($this->_file,"                        onclick=\"send".$tableModel."Delete({".strtoupper($this->_arrDet['aTableTablePrimaryKey'][0])."});valBtnClose();\">\n");
            fputs($this->_file,"                </td>\n");
            fputs($this->_file,"            </tr>\n");
            fputs($this->_file,"            <!-- END: tab_".$this->_arrDet['table_detail']." -->\n");
            fputs($this->_file,"        </tbody>\n");
            fputs($this->_file,"    </table>\n");
            fputs($this->_file,"</div>\n");
            fputs($this->_file, "<!-- END: main -->");

            self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/", "solapa".$n.".html" );

        }

    }

    private function _bldSctructureHtml($file, $arr)
    {

        $foreignKeyMaster = array_key_exists('aTableForeignKeysAssocMasterDetail', $arr) ? $arr['aTableForeignKeysAssocMasterDetail'][0]['column_name'] : '';

        for ($i=0;$i<count($arr['aTableStructure']);$i++){

            if ($foreignKeyMaster !== '')
                $tableModel = $this->_strCamel($arr['table_detail']);

            $bDo = (( $foreignKeyMaster === '' ) || ( $arr['aTableStructure'][$i]['column_name'] != $foreignKeyMaster )) ? true : false ;

            if ($arr['aTableStructure'][$i]['column_name']!=$arr['aTableTablePrimaryKey'][0] &&
            !self::_inMatrix($arr['aTableStructure'][$i]['column_name'], $arr['aTableForeignKeysAssoc'] )
            && $bDo){
                $label=self::_strLabel($arr['aTableStructure'][$i]['column_name']);
                if ($arr['aTableStructure'][$i]['data_type']==='boolean'){
                    fputs($file, "            <tr>\n");
                    fputs($file, "                <td>".$label."</td>\n");
                    fputs($file, "                <td>\n");
                    fputs($file, "                    <input type=\"checkbox\"\n");
                    fputs($file, "                           id=\"".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                           name=\"".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                           {".strtoupper($arr['aTableStructure'][$i]['column_name'])."_CHK}");
                    fputs($file, ">\n");
                    fputs($file, "                </td>\n");
                    fputs($file, "            </tr>\n");
                }else if ($arr['aTableStructure'][$i]['data_type']==='date'){
                    fputs($file, "            <tr>\n");
                    fputs($file, "                <td>".$label."</td>\n");
                    fputs($file, "                <td>\n");
                    fputs($file, "                    <input id=\"id".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                           name=\"".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                           autocomplete=\"off\"\n");
                    fputs($file, "                           style=\"width: 98%;\"\n");
                    fputs($file, "                           data-validation=\"date\"\n");
                    fputs($file, "                           value=\"{".strtoupper($arr['aTableStructure'][$i]['column_name'])."}\"");
                    fputs($file, " readonly>\n");
                    fputs($file, "                    <script>\n");
                    fputs($file, "                        Calendar.setup({\n");
                    fputs($file, "                            inputField    : \"id".$arr['aTableStructure'][$i]['column_name']."\",\n");
                    fputs($file, "                            ifFormat      : \"%Y-%m-%d\",\n");
                    fputs($file, "                            button        : \"id_".$arr['aTableStructure'][$i]['column_name']."\",\n");
                    fputs($file, "                            align         : \"Tr\"\n");
                    fputs($file, "                        });\n");
                    fputs($file, "                    </script>\n");
                    fputs($file, "                </td>\n");
                    fputs($file, "            </tr>\n");
                }else if ($arr['aTableStructure'][$i]['data_type']==='time without time zone'){     
                    fputs($file, "            <tr>\n");
                    fputs($file, "                <td>".$label."</td>\n");
                    fputs($file, "                <td>\n");
                    fputs($file, "                    <input id=\"id".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                           name=\"".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                           autocomplete=\"off\"\n");
                    fputs($file, "                           style=\"width: 98%;\"\n");
                    fputs($file, "                           class=\"time_element\"\n");
                    fputs($file, "                           data-validation=\"time\"\n");
                    fputs($file, "                           value=\"{".strtoupper($arr['aTableStructure'][$i]['column_name'])."}\"");
                    fputs($file, "                           maxlength=\"8\"\n");
                    fputs($file, "                           onkeypress=\"return acceptTime(event);\"\n");
                    fputs($file, "                           readonly>\n");                    
                    fputs($file, "                </td>\n");
                    fputs($file, "            </tr>\n");
                }else if ( !in_array($arr['aTableStructure'][$i]['data_type'], [ 'boolean', 'date', 'text', 'time without time zone'])){
                    fputs($file, "            <tr>\n");
                    fputs($file, "                <td>".$label."</td>\n");
                    fputs($file, "                <td>\n");
                    fputs($file, "                    <input id=\"".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                           name=\"".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                           autocomplete=\"off\"\n");
                    fputs($file, "                           style=\"width: 98%;\"\n");
                    fputs($file, "                           data-format=\"uppercase\"\n");
                    fputs($file, "                           data-constraints=\"\"\n");
                    fputs($file, "                           data-validation=\"required\"\n");
                    fputs($file, "                           value=\"{".strtoupper($arr['aTableStructure'][$i]['column_name'])."}\"");

                    //if ($foreignKeyMaster !== '')
                    //    fputs($file, "                           onKeyUp=\"valPanel$tableModel();\"");

                    for ($j=0;$j<count($arr['aTableUniqueConstraint']);$j++)
                        if ($arr['aTableStructure'][$i]['column_name']==$arr['aTableUniqueConstraint'][$j]['column_name'])
                            fputs($file, "\n                           {".strtoupper($arr['aTableStructure'][$i]['column_name'])."_READONLY}");
                    fputs($file, ">\n");
                    fputs($file, "                </td>\n");
                    fputs($file, "            </tr>\n");
                }

            }
        }

        for ($i=0;$i<count($arr['aTableForeignKeysAssoc']);$i++){
            $FOREIGNNAME = strtoupper($arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']);
            $label=self::_strLabel($arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']);
            fputs($file, "            <tr>\n");
            fputs($file, "                <td>".$label."</td>\n");
            fputs($file, "                <td>\n");
            fputs($file, "                    <select id=\"id_".$arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."\" name=\"id_".$arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."\" style=\"width: 100%;\" data-validation=\"select\">\n");
            fputs($file, "                        <option value=null selected>Seleccione...</option>\n");
            fputs($file, "                        <!-- BEGIN: ".$arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']." -->\n");
            fputs($file, "                        <option value={ID_".$FOREIGNNAME."} {SELECTED_".$FOREIGNNAME."}>{DES_".$FOREIGNNAME."}</option>\n");
            fputs($file, "                        <!-- END: ".$arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']." -->\n");
            fputs($file, "                    </select>\n");
            fputs($file, "                </td>\n");
            fputs($file, "            </tr>\n");
        }

        for ($i=0;$i<count($arr['aTableStructure']);$i++){

            if ($foreignKeyMaster !== '')
                $tableModel = $this->_strCamel($arr['table_detail']);

            $bDo = (( $foreignKeyMaster === '' ) || ( $arr['aTableStructure'][$i]['column_name'] != $foreignKeyMaster )) ? true : false ;

            if ($arr['aTableStructure'][$i]['column_name']!=$arr['aTableTablePrimaryKey'][0] &&
            !self::_inMatrix($arr['aTableStructure'][$i]['column_name'], $arr['aTableForeignKeysAssoc'] )
            && $bDo){
                $label=self::_strLabel($arr['aTableStructure'][$i]['column_name']);
                if ($arr['aTableStructure'][$i]['data_type']==='text'){
                fputs($file, "            <tr>\n");
                fputs($file, "                <td>".$label."</td>\n");
                fputs($file, "                <td>\n");
                    fputs($file, "                    <textarea id=\"".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                              name=\"".$arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($file, "                              style=\"width: 98%;\"\n");
                    fputs($file, "                              rows=\"\"\n");
                    fputs($file, "                              cols=\"\"\n");
                    fputs($file, "                              data-format=\"uppercase\"\n");
                    fputs($file, "                              data-constraints=\"textarea\"\n");
                    fputs($file, "                              data-validation=\"textarea\"");
                    //if ($foreignKeyMaster !== '')
                    //    fputs($file, "                              onKeyUp=\"valPanel$tableModel();\"");
                    fputs($file, ">{".strtoupper($arr['aTableStructure'][$i]['column_name'])."}</textarea>\n");
                    fputs($file, "                </td>\n");
                    fputs($file, "            </tr>\n");
                }
            }
        }

    }

    private function _bldFileJsIncludeJsonAuxAjax()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "include.json" );

        fputs($this->_file,"{\"dir\":\"$this->_dirLog\",\n\"modulo\":\"".$this->_arr['table']."_aux\",\n\"arr\":[\n");
        fputs($this->_file,"    {\"dir\":\"packs\", \"typ\":\"js\" , \"arc\":\"jquery/jquery-ui-custom\"        },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"relocate\"        },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"combo\"           },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"ajax\"            },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"array\"           },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"time\"           },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"numero\"          },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"effect\"          },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"solapador\"       },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"timepicki\"       },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"alerts\"          },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"msjAdmin\"        },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"tabla\"           },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"validar\"         },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"validateData\"    },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"formatData\"      },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"constraintsData\" },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"js\" , \"arc\":\"app\"             },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"js\" , \"arc\":\"validar\"         },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"tpl\"             },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"form\"            },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"calendar\"        },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"timepicki\"        },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"validateData\"    },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"deniedWritingImg\"},\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"solapador\"       },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"alerts\"          },\n");
        fputs($this->_file,"    {\"dir\":\"modulo\", \"typ\":\"js\", \"arc\":\"reqs$this->_tableModel\"},\n");

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){
            fputs($this->_file,"    {\"dir\":\"modulo\", \"typ\":\"js\", \"arc\":\"req".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'])."\"},\n");
            fputs($this->_file,"    {\"dir\":\"modulo\", \"typ\":\"js\", \"arc\":\"tab".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'])."\"},\n");
            fputs($this->_file,"    {\"dir\":\"modulo\", \"typ\":\"js\", \"arc\":\"val".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'])."\"},\n");
        }

        fputs($this->_file,"    {\"dir\":\"modulo\", \"typ\":\"js\" , \"arc\":\"validar\"         }\n");
        fputs($this->_file,"]}");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "include.json" );

    }

    private function _bldFileJsInicioJsAuxAjax()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "inicio.js" );

        fputs($this->_file,"function inicio(oJSON)\n");
        fputs($this->_file,"{\n");
        fputs($this->_file,"    initView();\n");
        fputs($this->_file,"    jQryFormatData(document);\n");
        fputs($this->_file,"    jQryConstraintsData(document);\n");
        fputs($this->_file,"    jQryValidateData(document.getElementById('div0'));\n");
        fputs($this->_file,"    $(document).ready(function(){\$(\".time_element\").timepicki();});\n");

        fputs($this->_file,"    Tab(0,".(count($this->_arr['aTableDetailOfMaster'])+1).");\n");
        fputs($this->_file,"    if (oJSON != 0) {\n");

        for ( $i=1; $i <= count($this->_arr['aTableDetailOfMaster']) ; $i++){
            fputs($this->_file,"        document.getElementById('Tab$i').style.display='';\n");
        }

        fputs($this->_file,"        document.getElementById('botones').style.display='';\n");
        fputs($this->_file,"        eventListenerActivar();\n");
        fputs($this->_file,"        valBtnClose();\n");
        fputs($this->_file,"    }\n");
        fputs($this->_file,"}\n");
        fputs($this->_file,"\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "inicio.js" );

    }

    private function _bldFileJsValidarJsAuxAjax()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "validar.js" );

        fputs($this->_file,"function valEnvio(sStatus)\n");
        fputs($this->_file,"{\n");
        fputs($this->_file,"    if (validateObjs(document.getElementById('div0')))\n");
        fputs($this->_file,"        send".$this->_tableModel."Register(sStatus);\n");
        fputs($this->_file,"\n");
        fputs($this->_file,"    /*if(validateObjs(document.getElementById('div0'))) {\n");
        fputs($this->_file,"        * var dataDorm = request(document.getElementById('div_input_hidden'));\n");

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster'])+1;$i++)
           fputs($this->_file,"        * dataForm += '&'+request(document.getElementById('div$i'));\n");

        fputs($this->_file,"        * sendDataForm(dataForm);\n    }*/\n");
        fputs($this->_file,"}\n");
        fputs($this->_file,"\n");
        fputs($this->_file,"function botonGuardarOnOff(nTab)\n{");
        fputs($this->_file,"\n");
        fputs($this->_file,"    if (nTab==0)\n");
        fputs($this->_file,"        document.getElementById(\"id_guardar\").style.display='';\n");
        fputs($this->_file,"    else\n");
        fputs($this->_file,"        document.getElementById(\"id_guardar\").style.display='none';\n}\n");
        fputs($this->_file,"\n");
        fputs($this->_file,"function regresarTabBotonGuardarOnOff(tTab)\n");
        fputs($this->_file,"{\n");
        fputs($this->_file,"    for (var i=tTab-1; i >= 0; i--) {\n");
        fputs($this->_file,"        if (document.all('div'+i).style.display!=\"none\") {\n");
        fputs($this->_file,"            if (i==0)\n");
        fputs($this->_file,"                document.getElementById(\"id_guardar\").style.display='';\n");
        fputs($this->_file,"            else\n");
        fputs($this->_file,"                document.getElementById(\"id_guardar\").style.display='none';\n");
        fputs($this->_file,"        }\n");
        fputs($this->_file,"    }\n");
        fputs($this->_file,"}\n");
        fputs($this->_file,"\n");
        fputs($this->_file,"function segirBotonGuardarOnOff(tTab)\n");
        fputs($this->_file,"{\n");
        fputs($this->_file,"    for (var i=1;i<=tTab-1;i++) {\n");
        fputs($this->_file,"        if (document.all('div'+i).style.display!=\"none\") {\n");
        fputs($this->_file,"            if (i==0)\n");
        fputs($this->_file,"                document.getElementById(\"id_guardar\").style.display='';\n");
        fputs($this->_file,"            else\n");
        fputs($this->_file,"                document.getElementById(\"id_guardar\").style.display='none';\n");
        fputs($this->_file,"        }\n");
        fputs($this->_file,"    }\n");
        fputs($this->_file,"}\n");
        fputs($this->_file,"\n");
        fputs($this->_file,"function valBtnClose()\n");
        fputs($this->_file,"{\n");
        fputs($this->_file,"    document.getElementById('id_cerrar').style.display=(\n");
        fputs($this->_file,"        validateObjs(document.getElementById('div0'))\n");

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++)
           fputs($this->_file,"            && valTable(document.getElementById('id_tab_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'),1)\n");

        fputs($this->_file,"        )?'':'none';\n");
        fputs($this->_file,"}\n");
        fputs($this->_file,"\n");
        fputs($this->_file,"function eventListenerActivar()\n");
        fputs($this->_file,"{\n");
        fputs($this->_file,"    document.getElementById(\"div0\").addEventListener(\"change\", valBtnClose);\n");
        fputs($this->_file,"    document.getElementById(\"div0\").addEventListener(\"keyup\", valBtnClose);\n");
        fputs($this->_file,"}\n");
        fputs($this->_file,"\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "validar.js" );

    }

    private function  _bldFileJsRequestJsAjax()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "reqs$this->_tableModel.js" );

        fputs($this->_file,"function send".$this->_tableModel."Register(sStatus)\n");
        fputs($this->_file,"{\n");
        fputs($this->_file,"    var responseAjax = (response) =>\n");
        fputs($this->_file,"    {\n");
        fputs($this->_file,"        msjAdmin(response[0]);\n");
        fputs($this->_file,"        if (inArray(response[0], ['C', 'A', 'H'])) {\n");
        fputs($this->_file,"            if (response[0] == 'C') {\n");
        fputs($this->_file,"                document.getElementById('".$this->_arr['aTableTablePrimaryKey'][0]."').value = response[1];\n");

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++)
            fputs($this->_file,"                show('Tab".($i+1)."', 'explode', 1500);\n");

        fputs($this->_file,"                //sendIdCodDateGet();\n");
        fputs($this->_file,"                eventListenerActivar();\n");
        fputs($this->_file,"            } else if (response[0] == 'H') {\n");
        fputs($this->_file,"                document.getElementById('popup_ok').setAttribute('onClick', 'window.location.assign(\"../".$this->_arr['table']."/\")');\n");
        fputs($this->_file,"            }\n");
        fputs($this->_file,"        }\n");
        fputs($this->_file,"    }\n");
        fputs($this->_file,"    var sIdVal = document.getElementById('id').value;\n");
        fputs($this->_file,"    var oPanel = document.getElementById('div0');\n");
        fputs($this->_file,"    var reqs = b64EncodeUnicode('id') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);// + '&id_status=' + sStatus;\n");
        fputs($this->_file,"    var ajax = new sendAjax();\n");
        fputs($this->_file,"    ajax.method = 'POST';\n");
        fputs($this->_file,"    ajax.url = \"../../../\" + ".$this->_dirLog." + \"/reqs/control/".$this->_arr['table']."/index.php/\";\n");
        fputs($this->_file,"    ajax.data = reqs;\n");
        fputs($this->_file,"    ajax.funResponse = responseAjax;\n");
        fputs($this->_file,"    ajax.dataType = 'json';\n");
        fputs($this->_file,"    ajax.b64 = true;\n");
        fputs($this->_file,"    ajax.send();\n");
        fputs($this->_file,"}\n");
        fputs($this->_file,"\n");
        //fputs($this->_file,"function send".$this->_tableModel."Get(id)\n");
        //fputs($this->_file,"{\n");
        //fputs($this->_file,"    var responseAjax = (response) =>\n");
        //fputs($this->_file,"    {\n");

        //$eVal='';
        //for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
        //    $eVal="document.getElementById('".$this->_arr['aTableStructure'][$i]['column_name']."').value";
        //    fputs($this->_file,"        $eVal = response.".$this->_arr['aTableStructure'][$i]['column_name'].";\n");
        //}

        //fputs($this->_file,"        if (response.id != 0) {\n");

        //for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){
        //    $tableModel=$this->_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']);
        //    fputs($this->_file,"            send".$tableModel."Get(false);\n");
        //}

        //fputs($this->_file,"        }\n");
        //fputs($this->_file,"    }\n");
        //fputs($this->_file,"    var ajax = new sendAjax();\n");
        //fputs($this->_file,"    ajax.method = 'POST';\n");
        //fputs($this->_file,"    ajax.url = \"../../../\" + ".$this->_dirLog." + \"/reqs/".$this->_arr['table']."/".$this->_arr['table']."_get.php\";\n");
        //fputs($this->_file,"    ajax.data = \"id=\" + id;\n");
        //fputs($this->_file,"    ajax.funResponse = responseAjax;\n");
        //fputs($this->_file,"    ajax.dataType = 'json';\n");
        //fputs($this->_file,"    ajax.send();\n");
        //fputs($this->_file,"}\n");
        //fputs($this->_file,"\n");
        //fputs($this->_file,"/*\n");
        //fputs($this->_file,"function sendIdCodDateGet()\n");
        //fputs($this->_file,"{\n");
        //fputs($this->_file,"    var responseAjax = (response) =>\n");
        //fputs($this->_file,"    {\n");
        //fputs($this->_file,"        document.getElementById('id').value=response[0].id;\n");
        //fputs($this->_file,"        document.getElementById('id_cod').innerHTML=response[0].codigo;\n");
        //fputs($this->_file,"        document.getElementById('id_date').innerHTML=response[0].fecha_registro;\n");
        //fputs($this->_file,"    }\n");
        //fputs($this->_file,"    var ajax = new sendAjax();\n");
        //fputs($this->_file,"    ajax.method = 'POST';\n");
        //fputs($this->_file,"    ajax.url = \"../../../\"+myApp+\"/reqs/".$this->_arr['table']."/id_cod_date_get.php\";\n");
        //fputs($this->_file,"    ajax.funResponse = responseAjax;\n");
        //fputs($this->_file,"    ajax.dataType = 'json';\n");
        //fputs($this->_file,"    ajax.send();\n");
        //fputs($this->_file,"}\n");
        //fputs($this->_file,"*/\n");
        //fputs($this->_file,"\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "reqs$this->_tableModel.js" );

    }

    private function _bldFileJsValTabJsAuxAjax()
    {
        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){

            $tableModel=$this->_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']);

            self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "val$tableModel.js" );

            fputs($this->_file,"function onOffPanel$tableModel() \n{\n");
            fputs($this->_file,"    toggle('id_panel_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."', 'blind',500);\n");
            fputs($this->_file,"    changeTheObDeniedWritingImg('id_img_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."');\n");
            fputs($this->_file,"    initObjs(document.getElementById('id_panel_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'));\n");
            fputs($this->_file,"    document.getElementById('id_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."').value=0;\n");
            fputs($this->_file,"}\n");
            fputs($this->_file,"\n");

            fputs($this->_file,"function valEnvio$tableModel()\n");
            fputs($this->_file,"{\n");
            fputs($this->_file,"    var oDiv=document.getElementById('id_panel_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."');\n");
            fputs($this->_file,"    if (validateObjs(document.getElementById(\"id_panel_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."\"))) \n");
            fputs($this->_file,"        send".$tableModel."Register();\n");
            fputs($this->_file,"}\n");
            fputs($this->_file,"\n");
            fputs($this->_file,"function edit$tableModel(obj)\n");
            fputs($this->_file,"{\n");
            fputs($this->_file,"    if(document.getElementById('id_panel_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."').style.display=='none'){\n");
            fputs($this->_file,"        onOffPanel$tableModel();\n");
            fputs($this->_file,"    }\n");
            fputs($this->_file,"    document.getElementById('id_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."').value=obj.parentNode.parentNode.id;\n");
            fputs($this->_file,"    var oPanel=document.getElementById(\"id_panel_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."\");\n");

            $arr=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$i]['table_name']]);

            $k=0;

            fputs($this->_file,"    var oInputs=oPanel.getElementsByTagName(\"input\");\n");

            for ($j=0;$j<count($arr['aTableStructure']);$j++) {
                if ( !in_array( $arr['aTableStructure'][$j]['column_name'], [ $arr['aTableForeignKeysAssocMasterDetail'][0]['column_name'], $arr['aTableTablePrimaryKey'][0] ] )
                && !self::_inMatrix($arr['aTableStructure'][$j]['column_name'], $arr['aTableForeignKeysAssoc'] )
                && $arr['aTableStructure'][$j]['data_type'] !== 'text') {
                    fputs($this->_file,"    oInputs['".$arr['aTableStructure'][$j]['column_name']."'].value = obj.parentNode.parentNode.cells[".$k++."].innerHTML;\n");
                }
            }

            fputs($this->_file,"    var oSelects=oPanel.getElementsByTagName(\"select\");\n");

            for ($j=0;$j<count($arr['aTableStructure']);$j++) {
                if ( !in_array( $arr['aTableStructure'][$j]['column_name'], [ $arr['aTableForeignKeysAssocMasterDetail'][0]['column_name'], $arr['aTableTablePrimaryKey'][0] ] )
                && self::_inMatrix($arr['aTableStructure'][$j]['column_name'], $arr['aTableForeignKeysAssoc'] ) ) {
                    fputs($this->_file,"    oSelects['".$arr['aTableStructure'][$j]['column_name']."'].value = obj.parentNode.parentNode.cells[".$k++."].id;\n");
                }
            }

            fputs($this->_file,"    var oTextAreas=oPanel.getElementsByTagName(\"textarea\");\n");

            for ($j=0;$j<count($arr['aTableStructure']);$j++) {
                if ( !in_array( $arr['aTableStructure'][$j]['column_name'], [ $arr['aTableForeignKeysAssocMasterDetail'][0]['column_name'], $arr['aTableTablePrimaryKey'][0] ] )
                && !self::_inMatrix($arr['aTableStructure'][$j]['column_name'], $arr['aTableForeignKeysAssoc'] )
                && $arr['aTableStructure'][$j]['data_type'] === 'text') {
                    fputs($this->_file,"    oTextAreas['".$arr['aTableStructure'][$j]['column_name']."'].value = obj.parentNode.parentNode.cells[".$k++."].innerHTML;\n");
                }
            }

            fputs($this->_file,"}\n");
            fputs($this->_file,"\n");

            self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "val$tableModel.js" );

        }

    }

    private function _bldFileJsReqTabJsAuxAjax()
    {

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){

            $tableModel=$this->_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']);

            self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "req$tableModel.js" );

            fputs($this->_file,"function send".$tableModel."Register() \n");
            fputs($this->_file,"{\n");
            fputs($this->_file,"    var responseAjax = (response) =>\n");
            fputs($this->_file,"    {\n");
            fputs($this->_file,"        msjAdmin(response);\n");
            fputs($this->_file,"        if (inArray(response, ['C', 'A'])){\n");
            fputs($this->_file,"            send".$tableModel."Get();\n");
            fputs($this->_file,"            onOffPanel$tableModel();\n");
            fputs($this->_file,"        }\n");
            fputs($this->_file,"    }\n");
            fputs($this->_file,"    var sIdVal = document.getElementById('id').value;\n");
            fputs($this->_file,"    var oPanel = document.getElementById('id_panel_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."');\n");
            fputs($this->_file,"    var reqs = b64EncodeUnicode('id_".$this->_arr['table']
                ."') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);\n");
            fputs($this->_file,"    var ajax = new sendAjax();\n");
            fputs($this->_file,"    ajax.method = 'POST';\n");
            fputs($this->_file,"    ajax.url = \"../../../\" + ".$this->_dirLog." + \"/reqs/control/"
                .$this->_arr['table']."/".$this->_arr['aTableDetailOfMaster'][$i]['table_name'].".php/"
                    .self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'],1)."Register\";\n");
            fputs($this->_file,"    ajax.data = reqs;\n");
            fputs($this->_file,"    ajax.funResponse = responseAjax;\n");
            fputs($this->_file,"    ajax.b64 = true;\n");
            fputs($this->_file,"    ajax.send();\n");
            fputs($this->_file,"}\n");
            fputs($this->_file,"\n");
            fputs($this->_file,"function send".$tableModel."Get(bAsync)\n");
            fputs($this->_file,"{\n");
            fputs($this->_file,"    var responseAjax = (response) =>\n");
            fputs($this->_file,"    {\n");
            fputs($this->_file,"        fillTab$tableModel(response);\n");
            fputs($this->_file,"        valBtnClose();\n");
            fputs($this->_file,"    }\n");
            fputs($this->_file,"    var reqs = b64EncodeUnicode(\"".$this->_arr['aTableTablePrimaryKey'][0]
                ."\") + \"=\" + b64EncodeUnicode(document.getElementById(\"".$this->_arr['aTableTablePrimaryKey'][0]."\").value);\n");
            fputs($this->_file,"    var ajax = new sendAjax();\n");
            fputs($this->_file,"    ajax.method = 'POST';\n");
            fputs($this->_file,"    ajax.url = \"../../../\" + ".$this->_dirLog." + \"/reqs/control/"
            .$this->_arr['table']."/".$this->_arr['aTableDetailOfMaster'][$i]['table_name'].".php/"
            .self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'],1)."Get\";\n");
            fputs($this->_file,"    ajax.data = reqs;\n");
            fputs($this->_file,"    ajax.funResponse = responseAjax;\n");
            fputs($this->_file,"    ajax.dataType = 'json';\n");
            fputs($this->_file,"    ajax.async = bAsync;\n");
            fputs($this->_file,"    ajax.b64 = true;\n");
            fputs($this->_file,"    ajax.send();\n");
            fputs($this->_file,"}\n\n");
            fputs($this->_file,"function send".$tableModel."Delete(id)\n");
            fputs($this->_file,"{\n");
            fputs($this->_file,"    confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {\n");
            fputs($this->_file,"        if (ok) {\n");
            fputs($this->_file,"            var responseAjax = (response) =>\n");
            fputs($this->_file,"            {\n");
            fputs($this->_file,"                (response != 'B') ? msjAdmin(response) : send".$tableModel."Get();\n");
            fputs($this->_file,"            }\n");
            fputs($this->_file,"            var ajax = new sendAjax();\n");
            fputs($this->_file,"            ajax.method = 'POST';\n");
            fputs($this->_file,"            ajax.url = \"../../../\" + ".$this->_dirLog." + \"/reqs/control/"
                .$this->_arr['table']."/".$this->_arr['aTableDetailOfMaster'][$i]['table_name'].".php/"
                    .self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'],1)."Delete\";\n");
            fputs($this->_file,"            ajax.data = b64EncodeUnicode(\"id\")+\"=\"+b64EncodeUnicode(id);\n");
            fputs($this->_file,"            ajax.funResponse = responseAjax;\n");
            fputs($this->_file,"            ajax.b64 = true;\n");
            fputs($this->_file,"            ajax.send();\n");
            fputs($this->_file,"        }\n");
            fputs($this->_file,"    });\n");
            fputs($this->_file,"}\n");
            fputs($this->_file,"\n");

            self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "req$tableModel.js" );

        }

    }

    private function _bldFileJsTblTabJsAuxAjax()
    {

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){

            $tableModel=$this->_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']);

            self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "tab$tableModel.js" );

            fputs($this->_file,"function fillTab$tableModel(aObjs)\n");
            fputs($this->_file,"{\n\n");
            fputs($this->_file,"    var tbl = document.getElementById('id_tab_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."');\n");
            fputs($this->_file,"    deleteAllRowsTable('id_tab_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."',1);\n\n");
            fputs($this->_file,"    if (!tbl.tBodies[0]) {\n");
            fputs($this->_file,"        var tbody = document.createElement('tbody');\n");
            fputs($this->_file,"        tbody.setAttribute('style', 'display: block');\n");
            fputs($this->_file,"        tbody.setAttribute('style', 'width: 100%');\n");
            fputs($this->_file,"        tbl.appendChild(tbody);\n");
            fputs($this->_file,"    }\n\n");
            fputs($this->_file,"    if (aObjs.length !== 0) {\n");
            fputs($this->_file,"        for (var i = 0; i < aObjs.length; i++)\n");
            fputs($this->_file,"            tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));\n");
            fputs($this->_file,"        //setAttributeTD('id_tab_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."','align','left', 1, 3, 1, aObjs.length+1);\n");
            fputs($this->_file,"        paintTRsClearDark('id_tab_".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."');\n");
            fputs($this->_file,"    }\n\n");
            fputs($this->_file,"    function fillTabAux(row)\n");
            fputs($this->_file,"    {\n\n");
            fputs($this->_file,"        var oTr = document.createElement('tr');\n");
            fputs($this->_file,"        oTr.id = row.id;\n");
            fputs($this->_file,"        var aTd = [];\n\n");

            $arr=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$i]['table_name']]);

            $k = 0;
            for ( $j = 0; $j < count( $arr['aTableStructure'] ) ; $j++ ){
                if ( !in_array( $arr['aTableStructure'][$j]['column_name'], [ $arr['aTableForeignKeysAssocMasterDetail'][0]['column_name'], $arr['aTableTablePrimaryKey'][0] ] )
                && !self::_inMatrix($arr['aTableStructure'][$j]['column_name'], $arr['aTableForeignKeysAssoc'] )
                && $arr['aTableStructure'][$j]['data_type'] !== 'text') {
                    fputs($this->_file,"        aTd[$k] = document.createElement('td');\n");
                    fputs($this->_file,"        aTd[$k].appendChild(document.createTextNode(row.".$arr['aTableStructure'][$j]['column_name']."));\n");
                    fputs($this->_file,"        oTr.appendChild(aTd[".$k++."]);\n\n");
                }
            }

            for ( $j = 0; $j < count( $arr['aTableForeignKeysAssoc']); $j++ ) {
                 fputs($this->_file,"        aTd[$k] = document.createElement('td');\n");
                 fputs($this->_file,"        aTd[$k].id = row.".$arr['aTableForeignKeysAssoc'][$j]['column_name']."\n");
                 fputs($this->_file,"        aTd[$k].appendChild(document.createTextNode(row.des_".$arr['aTableForeignKeysAssoc'][$j]['foreign_table_name']."));\n");
                 fputs($this->_file,"        oTr.appendChild(aTd[".$k++."]);\n\n");
            }

            for ( $j = 0; $j < count( $arr['aTableStructure'] ) ; $j++ ){
                if ( !in_array( $arr['aTableStructure'][$j]['column_name'], [ $arr['aTableForeignKeysAssocMasterDetail'][0]['column_name'], $arr['aTableTablePrimaryKey'][0] ] )
                && !self::_inMatrix($arr['aTableStructure'][$j]['column_name'], $arr['aTableForeignKeysAssoc'] )
                && $arr['aTableStructure'][$j]['data_type'] === 'text') {
                    fputs($this->_file,"        aTd[$k] = document.createElement('td');\n");
                    fputs($this->_file,"        aTd[$k].appendChild(document.createTextNode(row.".$arr['aTableStructure'][$j]['column_name']."));\n");
                    fputs($this->_file,"        oTr.appendChild(aTd[".$k++."]);\n\n");
                }
            }

            fputs($this->_file,"        var oImgEdit = document.createElement('img');\n");
            fputs($this->_file,"        oImgEdit.setAttribute('class', 'accion');\n");
            fputs($this->_file,"        oImgEdit.setAttribute('src', '../../../../../'+appOrg+'/img/edit.png');\n");
            fputs($this->_file,"        oImgEdit.setAttribute('title', 'Editar datos...');\n");
            fputs($this->_file,"        oImgEdit.setAttribute('onclick',\"edit$tableModel(this);valBtnClose();\");\n");
            fputs($this->_file,"        aTd[$k] = document.createElement('td');\n");
            fputs($this->_file,"        aTd[$k].align = 'center';\n");
            fputs($this->_file,"        aTd[$k].appendChild(oImgEdit);\n\n");
            fputs($this->_file,"        var oImgDelete = document.createElement('img');\n");
            fputs($this->_file,"        oImgDelete.setAttribute('class', 'accion');\n");
            fputs($this->_file,"        oImgDelete.setAttribute('src', '../../../../../'+appOrg+'/img/cross.png');\n");
            fputs($this->_file,"        oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');\n");
            fputs($this->_file,"        oImgDelete.setAttribute('onclick',\"send".$tableModel."Delete(\"+row.id+\");valBtnClose();\");\n");
            fputs($this->_file,"        aTd[$k].appendChild(oImgDelete);\n");
            fputs($this->_file,"        oTr.appendChild(aTd[$k]);\n\n");
            fputs($this->_file,"        return oTr;\n\n");
            fputs($this->_file,"    }\n\n");
            fputs($this->_file,"}");

            self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "tab$tableModel.js" );

        }

    }

    private function _bldFileJsIndexPhpAuxAjax()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "index.php" );

        fputs($this->_file,"<?php\n");
        fputs($this->_file,"session_start();");
        fputs($this->_file,"header(\"Location: ../../../../\".strtolower(\$_SESSION['FDSoil']).\"/admin_session_closed/\");\n\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "index.php" );

    }

}
