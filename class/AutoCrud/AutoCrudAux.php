<?php

trait AutoCrudAux
{

    private function _bldArrDet($aParams)
    {

        echo "Buscando información de tabla '".$aParams[2]."' del esquema '".$aParams[0]."'...<br>";

        $argument = [];

        $argument['schema']=$aParams[0];
        $argument['table_master'] = $aParams[1];
        $argument['table_detail'] = $aParams[2];
        $argument['table'] = $aParams[2];

        $arr['schema'] = $aParams[0];
        $arr['table_master'] = $aParams[1];
        $arr['table_detail'] = $aParams[2];
        $arr['aTableStructure'] = [];
        $arr['aTableTablePrimaryKey'] = [];
        $arr['aTableForeignKeysAssoc'] = [];
        $arr['aTableUniqueConstraint'] = [];
        $arr['aTableForeignKeysAssocMasterDetail'] = [];

        $arr['aTableStructure'] = self::_getTableStructure($argument);
        $arr['aTableTablePrimaryKey'] = self::_getTablePrimaryKey($argument);
        $arr['aTableForeignKeysAssoc'] = self::_getTableDetailForeignKeysAssoc($argument);
        $arr['aTableForeignKeysAssocMasterDetail'] = self::_getTableForeignKeysAssocMasterDetail($argument);
        $arr['aTableUniqueConstraint'] = self::_getTableUniqueConstraint($argument);

        echo "Información de tabla '".$aParams[2]."' del esquema '".$aParams[0]."' buscada...<br>";

        return $arr;

    }

    private function _convertArrAsocSchemaTable($arrOld)
    {

        $arrNew = [];

        for ($i=0;$i<count($arrOld);$i++){
            $arrNew[$i]['schema']=$arrOld[$i][0];
            $arrNew[$i]['table']=$arrOld[$i][1];
        }

        return $arrNew;

    }

    private function _dirBld($path)
    {

        if (!file_exists("../../../".$this->_dir."/".$path) &&
            !is_dir("../../../".$this->_dir."/".$path)){
                echo "- Construyendo Directorio <strong>$path</strong>...<br>";
                mkdir("../../../".$this->_dir."/".$path);
                chmod("../../../".$this->_dir."/".$path, 0777);
        }

        echo "--> Directorio <strong>'$path'</strong> construido...<br>";

    }

    private function _dirLog($dir)
    {

        $resp = "";

        if ( $dir === $_SESSION['FDSoil'] )
            $resp = "FDSoil";
        else if ( $dir === $_SESSION['appOrg'] )
            $resp = "appOrg";
        else if ( $dir === $_SESSION['myApp'] )
            $resp = "myApp";

        return $resp;

    }

    private function _fileBldStart( $path, $file)
    {

        echo "- Creando el archivo <strong>'$file'</strong>...<br>";

        if (file_exists("../../../".$this->_dir.$path.$file) &&
            is_file("../../../".$this->_dir.$path.$file) && $this->_arr['production']){
            die("Error: El archivo <strong>$file</strong> ya existe...");
        }

        $this->_file = fopen("../../../".$this->_dir.$path.$file, "w+")
                or die("Problemas en la creaci&oacute;n del archivo <strong>$file</strong>...");

    }

    private function _fileBldEnd( $path, $file)
    {

        fclose($this->_file);
        chmod("../../../".$this->_dir.$path.$file,0777);
        echo "--> El archivo <strong>'$file'</strong> Construido...<br>";

    }

    private function _foreignKeyNoReply($foreignTableName)
    {
        $resp = true;
        for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++)
            if ($this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']==$foreignTableName){
                $resp = false;
                break;
            }
        return $resp;
    }

    private function _inMatrix($str, $matrix)
    {

        $resp=false;

        for ($i=0;$i<count($matrix);$i++)
            if ($str==$matrix[$i]['column_name'])
                $resp=true;

        return $resp;

    }

    private function _numXCha($num)
    {

        $resp="";

        switch ($num) {
            case  0: $resp='A'; break;
            case  1: $resp='B'; break;
            case  2: $resp='C'; break;
            case  3: $resp='D'; break;
            case  4: $resp='E'; break;
            case  5: $resp='F'; break;
            case  6: $resp='G'; break;
            case  7: $resp='H'; break;
            case  8: $resp='I'; break;
            case  9: $resp='J'; break;
            case 10: $resp='K'; break;
        }

        return $resp;

    }

    private function _strCamel($str, $nPos = 0)
    {

        $arr = explode("_", $str);
        $str = '';

        if (!in_array($nPos, [0,1]))
            die("Error: El parámetro \$nPos en strCamel() debe ser: 0 ó 1 ...");

        if ($nPos>count($arr) && count($arr)>0)
            die("Error: El parámetro \$nPos en strCamel() es mayor al largo de cadena...");

        for ($i=0;$i<count($arr);$i++)
            $str .= ($i >= $nPos) ? strtoupper(substr($arr[$i],0,1)).substr($arr[$i],1,strlen($arr[$i])) : $arr[$i];

        return $str;

    }

    private function _strLabel($str)
    {

        $arr = explode("_", $str);
        $str = '';

        for ($i=0;$i<count($arr);$i++)
            $str .= strtoupper(substr($arr[$i],0,1)).substr($arr[$i],1,strlen($arr[$i]))." ";

        return substr($str,0,strlen($str)-1);

    }

}
