<?php
namespace FDSoil;

include_once('../../../'.$_SESSION['FDSoil'].'/class/AutoCrud/AutoCrudAux.php');
include_once('../../../'.$_SESSION['FDSoil'].'/class/AutoCrud/AutoCrudDb.php');
include_once('../../../'.$_SESSION['FDSoil'].'/class/AutoCrud/AutoCrudPL.php');
include_once('../../../'.$_SESSION['FDSoil'].'/class/AutoCrud/AutoCrudSQL.php');
include_once('../../../'.$_SESSION['FDSoil'].'/class/AutoCrud/AutoCrudModel.php');
include_once('../../../'.$_SESSION['FDSoil'].'/class/AutoCrud/AutoCrudModule.php');
include_once('../../../'.$_SESSION['FDSoil'].'/class/AutoCrud/AutoCrudModuleAux.php');
include_once('../../../'.$_SESSION['FDSoil'].'/class/AutoCrud/AutoCrudModuleAuxAjax.php');
include_once('../../../'.$_SESSION['FDSoil'].'/class/AutoCrud/AutoCrudReqs.php');

use \FDSoil\DbFunc as DbFunc;

class AutoCrud extends DbFunc
{

    use \AutoCrudAux,
        \AutoCrudDb,
        \AutoCrudPL,
        \AutoCrudSQL,
        \AutoCrudModel,
        \AutoCrudModule,
        \AutoCrudModuleAux,
        \AutoCrudModuleAuxAjax,
        \AutoCrudReqs;

    private $_arr;
    private $_file;
    private $_dir;
    private $_dirLog;
    private $_tableModel;
    private $_tableMetodo;

    public function bldAutoCrud()
    {

        echo "- <strong>'Inicio del Proceso...'</strong><br>";

        $arr = [];
        $arr [0]['schema']=$_POST['dbSchema'];
        $arr [0]['table']=$_POST['dbTable'];
        //$arr = self::_convertArrAsocSchemaTable(json_decode($_POST['arrayChk']));

        //echo '<pre>';var_dump($arr);echo '</pre>';die();

        for ($i=0;$i<count($arr);$i++)
            self::_bldAutoCrudAux($arr[$i]);

        die("--><strong>'Fin del Proceso...'</strong>");

    }

    private function _bldAutoCrudAux($aParams)
    {

        echo "- Construyendo módulo de tabla <strong>'".$aParams['table']."'</strong> del esquema <strong>'".$aParams['schema']."'</strong>...<br>";

        self::_bldConstruct($aParams);
        self::_bldPL();
        self::_bldSQL();
        self::_bldModel();
        self::_bldModule();

        if (count($this->_arr['aTableDetailOfMaster']) === 0)
            self::_bldModuleAux();
        else{
            self::_bldModuleAuxAjax();
            self::_bldReqs();
        }

        echo "--> Módulo de tabla <strong>'".$this->_arr['table']."'</strong> del esquema <strong>'".$this->_arr['schema']
            ."'</strong> construido...<br>";

    }

    private function _bldConstruct($aParams)
    {

        echo "- Buscando información de tabla <strong>'".$aParams['table']."'</strong> del esquema <strong>'".$aParams['schema']."'</strong>...<br>";

        $this->_dir = $_POST['dir'];
        $this->_dirLog = self::_dirLog($_POST['dir']);
        $this->_tableModel = self::_strCamel($aParams['table']);
        $this->_tableMetodo = self::_strCamel($aParams['table'], 1);
        $this->_arr = [];
        $this->_arr['schema'] = $aParams['schema'];
        $this->_arr['table'] = $aParams['table'];
        $this->_arr['aTableStructure'] = [];
        $this->_arr['aTableForeignKeysAssoc'] = [];
        $this->_arr['aTableUniqueConstraint'] = [];
        $this->_arr['aTableStructure'] = self::_getTableStructure($aParams);
        $this->_arr['aTableTablePrimaryKey'] = self::_getTablePrimaryKey($aParams);
        $this->_arr['aTableForeignKeysAssoc'] = self::_getTableMasterForeignKeysAssoc($aParams);
        $this->_arr['aTableUniqueConstraint'] = self::_getTableUniqueConstraint($aParams);
        $this->_arr['aTableDetailOfMaster'] = self::_getTableDetailOfMaster($aParams);
        $this->_arr['production']=false;

        echo "--> Información de tabla <strong>'".$aParams['table']."'</strong> del esquema <strong>'".$aParams['schema']."'</strong> buscada...<br>";

    }

}
