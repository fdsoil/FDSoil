<?php

trait AutoCrudModel
{

    private function _bldModel()
    {
        self::_fileBldStart( "/class/".$this->_tableModel."/", $this->_tableModel.".php" );

        self::_bldModelMaster();

        fputs($this->_file,"}\n\n");

        self::_fileBldEnd( "/class/".$this->_tableModel."/", $this->_tableModel.".php" );

        self::_bldModelDetail();

    }

    private function _bldModelMaster()
    {
        $alias=count($this->_arr['aTableForeignKeysAssoc'])>0?'A.':'';
        $tableMetodo = '';

        fputs($this->_file, "<?php\n");
        fputs($this->_file, "namespace ".$this->_dirLog.";\n\n");

        fputs($this->_file, "use \\".$_SESSION['FDSoil']."\Func as Func;\n");
        fputs($this->_file, "use \\".$_SESSION['FDSoil']."\DbFunc as DbFunc;\n\n");

        fputs($this->_file, "/** $this->_tableModel: ");
        fputs($this->_file, "Clase para actualizar y consultar la tabla '".$this->_arr['table']."'.\n");
        fputs($this->_file, "* Utiliza '\\".$_SESSION['FDSoil']."\Func' y '\\".$_SESSION['FDSoil']."\DbFunc' respectivamente.*/\n");
        fputs($this->_file, "class $this->_tableModel\n{\n\n");

        fputs($this->_file, "    /** Arreglo asociativo que contiene los correspondientes nombres de\n");
        fputs($this->_file, "    * campos y sus características respectivas. Dichos campos son los\n");
        fputs($this->_file, "    * únicos valores permitidos por la clase $this->_tableModel a traves de la\n");
        fputs($this->_file, "    * variable \$_POST, para actualizar la tabla '".$this->_arr['table']."'.*/\n");
        fputs($this->_file, "    static private \$_aValReqs = [\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){

            $label=self::_strLabel($this->_arr['aTableStructure'][$i]['column_name']);

            fputs($this->_file, str_repeat(" ",8)."\"".$this->_arr['aTableStructure'][$i]['column_name']."\" => [\n");
            fputs($this->_file, str_repeat(" ",12)."\"label\" => \"".$label."\",\n");
            fputs($this->_file, str_repeat(" ",12)."\"required\" => false\n");
            fputs($this->_file, str_repeat(" ",8)."]");

            if ($i<(count($this->_arr['aTableStructure'])-1))
                fputs($this->_file, ",\n");
            else
                fputs($this->_file, "\n");

        }

        fputs($this->_file, "    ];\n\n");

        fputs($this->_file, "    /** Devuelve la ruta en que están ubicados los archivos .sql de la clase $this->_tableModel.\n");
        fputs($this->_file, "    * Descripción: Devuelve la ruta en que están ubicados los .sql (querys) de la clase $this->_tableModel.\n");
        fputs($this->_file, "    * @return string La ruta en que están ubicados los archivos .sql (querys) de la clase $this->_tableModel.*/\n");
        fputs($this->_file, "    private function _path() { ");
        fputs($this->_file, "return '../../../'.\$_SESSION['".$this->_dirLog."'].'/class/".$this->_tableModel."/sql/".$this->_arr['table']."/';");
        fputs($this->_file, " }\n\n");

        fputs($this->_file, "    /** Obtener registro(s) de la tabla '".$this->_arr['table']."'.\n");
        fputs($this->_file, "    * Descripción: Obtener registro(s) de la tabla '".$this->_arr['table']."'.\n");
        fputs($this->_file, "    * Si el parámetro \$label trae el argumento 'LIST', devuelve todos los registros de la tabla '".$this->_arr['table']."'.\n");
        fputs($this->_file, "    * De lo contrario, si el parámetro \$label trae el argumento 'REGIST', devuelve un solo registro,\n");
        fputs($this->_file, "    * siempre y cuando el valor de \$_POST['id'] coincida con el ID principal de algún registro asociado.\n");
        fputs($this->_file, "    * Nota: Requiere el correspondiente valor \$_POST['".$this->_arr['aTableTablePrimaryKey'][0]."'] del registro específico a consultar.\n");
        fputs($this->_file, "    * @param string \$label Con sólo dos (2) posibles valores ('LIST' o 'REGIST').\n");
        fputs($this->_file, "    * @return result Resultado con registro(s) de la tabla '".$this->_arr['table']."'.*/\n");
        fputs($this->_file, "    public function ".$this->_tableMetodo."Get(\$label)\n");
        fputs($this->_file, "    {\n");
        fputs($this->_file, str_repeat(" ",8)."switch (\$label) {\n");
        fputs($this->_file, str_repeat(" ",12)."case 'LIST':\n");
        fputs($this->_file, str_repeat(" ",16)."\$arr['where'] = '';\n");
        fputs($this->_file, str_repeat(" ",16)."break;\n");
        fputs($this->_file, str_repeat(" ",12)."case 'REGIST':\n");
        fputs($this->_file, str_repeat(" ",16)."\$arr['".$this->_arr['aTableTablePrimaryKey'][0]);
        fputs($this->_file, "'] = \$_POST['".$this->_arr['aTableTablePrimaryKey'][0]."'];\n");
        fputs($this->_file, str_repeat(" ",16)."\$arr['where'] = Func::replace_data(\$arr, ' WHERE $alias");
        fputs($this->_file, $this->_arr['aTableTablePrimaryKey'][0]." = {fld:".$this->_arr['aTableTablePrimaryKey'][0]."} ');\n");
        fputs($this->_file, str_repeat(" ",16)."break;\n");
        fputs($this->_file, str_repeat(" ",8)."}\n");
        fputs($this->_file, str_repeat(" ",8)."return DbFunc::exeQryFile(self::_path().'".$this->_arr['table']."_get_select.sql', \$arr);\n");
        fputs($this->_file, "    }\n\n");

        fputs($this->_file, "    /** Actualiza registro de la tabla '".$this->_arr['table']."'.\n");
        fputs($this->_file, "    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla '".$this->_arr['table']."'.\n");
        fputs($this->_file, "    * Nota: Requiere de los respectivos valores procedentes del \$_POST para actualizar el registro.*/\n");
        fputs($this->_file, "    public function ".$this->_tableMetodo."Register()\n    {\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            if ($this->_arr['aTableStructure'][$i]['data_type']==='boolean'){
                    fputs($this->_file, str_repeat(" ",8)."\$_POST['".$this->_arr['aTableStructure'][$i]['column_name']."'] = ");
                    fputs($this->_file,"array_key_exists(\"".$this->_arr['aTableStructure'][$i]['column_name']."\",\$_POST) ? 't' : 'f';\n");
            }
        }

        fputs($this->_file, str_repeat(" ",8)."\$aMsjReqs = Func::valReqs(\$_POST, self::\$_aValReqs);\n");
        fputs($this->_file, str_repeat(" ",8)."if (!\$aMsjReqs){\n");
        fputs($this->_file, str_repeat(" ",12)."\$_POST = Func::formatReqs(\$_POST, self::\$_aValReqs);\n");
        fputs($this->_file, str_repeat(" ",12)."\$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'".$this->_arr['table']);
        fputs($this->_file,"_register_pl.sql',\$_POST, true, 'ACTUALIZANDO REGISTRO'));\n");
        fputs($this->_file, str_repeat(" ",12)."\$msj = \$row[0];\n");

        if (count($this->_arr['aTableDetailOfMaster']) === 0)
            fputs($this->_file, str_repeat(" ",12)."\$np = 2;\n");

        fputs($this->_file, str_repeat(" ",8)."} else {\n");
        fputs($this->_file, str_repeat(" ",12)."\$_SESSION['messages'] = \$aMsjReqs;\n");
        fputs($this->_file, str_repeat(" ",12)."\$msj = 'N';\n");

        if (count($this->_arr['aTableDetailOfMaster']) === 0)
            fputs($this->_file, str_repeat(" ",12)."\$np = 1;\n");

        fputs($this->_file, str_repeat(" ",8)."}\n");

        if (count($this->_arr['aTableDetailOfMaster']) === 0)
            fputs($this->_file, str_repeat(" ",8)."Func::adminMsj(\$msj,\$np);\n");
        else
            fputs($this->_file, str_repeat(" ",8)."return \$msj;\n");

        fputs($this->_file, "    }\n\n");

        fputs($this->_file, "    /** Elimina registro de la tabla '".$this->_arr['table']."'.\n");
        fputs($this->_file, "    * Descripción: Realiza eliminación del registro ( delete ) de la tabla '".$this->_arr['table']."'.\n");
        fputs($this->_file, "    * Nota: Requiere el valor \$_POST['".$this->_arr['aTableTablePrimaryKey'][0]."'] para buscar y eliminar registro en base de datos.*/ \n");
        fputs($this->_file, "    public function ".$this->_tableMetodo."Delete()\n    {\n");
        fputs($this->_file, str_repeat(" ",8)."\$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'".$this->_arr['table']);
        fputs($this->_file,"_delete_pl.sql', \$_POST, true, 'ELIMINANDO REGISTRO'));\n");
        fputs($this->_file, str_repeat(" ",8)."if (\$row[0]!='B')\n");
        fputs($this->_file, str_repeat(" ",12)."Func::adminMsj(\$row[0],1);\n");
        fputs($this->_file, str_repeat(" ",8)."else\n");
        fputs($this->_file, str_repeat(" ",12)."header(\"Location: \".\$_SERVER['HTTP_REFERER']);\n");
        fputs($this->_file, "    }\n\n");

        for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){
            $tableMetodo = self::_strCamel($this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name'], 1);
     
        fputs($this->_file, "    /** Lista de tabla foránea '".$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."'.\n");
        fputs($this->_file, "    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea '".$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."'.\n");
        fputs($this->_file, "    * @return result Resultado de la consulta de la de tabla foránea '".$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."'.*/ \n");
            fputs($this->_file, "    public function ".$tableMetodo."List()\n");
            fputs($this->_file, "    {\n");
            fputs($this->_file, str_repeat(" ",8)."return DbFunc::exeQryFile(self::_path().'".
                $this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."_list_select.sql', \$_POST);\n");
            fputs($this->_file, "    }\n\n");
        }

    }

    private function _bldModelDetail()
    {

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++) {

            self::_fileBldStart( "/class/".$this->_tableModel."/", 
            self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']).".php" );

            fputs($this->_file, "<?php\n");

            fputs($this->_file, "namespace ".$this->_dirLog."\\".$this->_tableModel.";\n\n");

            fputs($this->_file, "use \\".$_SESSION['FDSoil']."\Func as Func;\n");
            fputs($this->_file, "use \\".$_SESSION['FDSoil']."\DbFunc as DbFunc;\n\n");

            fputs($this->_file, "/** ".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']).": ");
            fputs($this->_file, "Clase para actualizar y consultar la tabla '".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'.\n");
            fputs($this->_file, "* Utiliza '\\".$_SESSION['FDSoil']."\Func' y '\\".$_SESSION['FDSoil']."\DbFunc' respectivamente.*/\n");
            fputs($this->_file, "class ".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'])."\n{\n\n");

            fputs($this->_file, "    /** Devuelve la ruta en que están ubicados los archivos .sql de la clase ".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']).".\n");
            fputs($this->_file, "    * Descripción: Devuelve la ruta en que están ubicados los .sql (querys) de la clase ".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']).".\n");
            fputs($this->_file, "    * @return string La ruta en que están ubicados los archivos .sql (querys) de la clase ".self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']).".*/\n");

            fputs($this->_file, "    private function _path() { ");
            fputs($this->_file, "return '../../../'.\$_SESSION['".$this->_dirLog."'].'/class/".$this->_tableModel."/sql/".
                $this->_arr['aTableDetailOfMaster'][$i]['table_name']."/'; }\n\n");

            $arrDet=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$i]['table_name']]);

            $tableMetodo = self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'], 1);

            fputs($this->_file, "    /** Actualiza registro de la tabla '".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'.\n");
            fputs($this->_file, "    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla '".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'.\n");
            fputs($this->_file, "    * Nota: Requiere de los respectivos valores procedentes del \$_POST para actualizar el registro.*/\n");
            fputs($this->_file, "    public function ".$tableMetodo."Register()\n    {\n");

            for ( $j=0 ; $j < count($arrDet['aTableStructure']); $j++ ){
                if ($arrDet['aTableStructure'][$j]['data_type']==='boolean'){
                         fputs($this->_file, str_repeat(" ",8)."\$_POST['".$arrDet['aTableStructure'][$j]['column_name']."'] = ");
                         fputs($this->_file,"array_key_exists(\"".$arrDet['aTableStructure'][$j]['column_name']."\",\$_POST) ? 't' : 'f';\n");
                }
            }

            //fputs($this->_file, "        //\$aValReqs = \$this->valReqs(\$_POST, \$this->aValReqs);\n");
            //fputs($this->_file, "        //if (!\$aValReqs) {\n");
            //fputs($this->_file, "            //\$_POST = \$this->formatReqs(\$_POST, \$this->aValReqs);\n");
            fputs($this->_file, "        \$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'"                
                .$this->_arr['aTableDetailOfMaster'][$i]['table_name']."_register_pl.sql',\$_POST, true, 'ACTUALIZANDO REGISTRO'));\n");
            //fputs($this->_file, "        \$msj = \$row[0];\n");
            //fputs($this->_file, "        //} else {\n");
            //fputs($this->_file, "            //\$_SESSION['messages'] = \$aValReqs;\n");
            //fputs($this->_file, "            //\$msj = 'N';\n");
            //fputs($this->_file, "        //}\n");
            //fputs($this->_file, "        return \$msj;\n");
            fputs($this->_file, "        return \$row[0];\n");
            fputs($this->_file, "    }\n");
            fputs($this->_file, "\n");

            fputs($this->_file, "    /** Obtener registro(s) de la tabla '".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'.\n");
            fputs($this->_file, "    * Descripción: Obtener registro(s) de la tabla '".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'.\n");
            //fputs($this->_file, "    * Nota: Requiere el correspondiente valor \$_POST['".$this->_arr['aTableTablePrimaryKey'][0]."'] del registro específico a consultar.\n");
            fputs($this->_file, "    * Nota: Requiere el correspondiente valor \$_POST identificativo del registro específico a consultar.\n");
            fputs($this->_file, "    * @return result Resultado con registro(s) de la tabla '".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'.*/\n");

            fputs($this->_file, "    public function ".$tableMetodo."Get()\n");
            fputs($this->_file, "    {\n");
            fputs($this->_file, "        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'"
                .$this->_arr['aTableDetailOfMaster'][$i]['table_name']."_get_select.sql', \$_POST));\n");
            fputs($this->_file, "    }\n");
            fputs($this->_file, "\n");

            fputs($this->_file, "    /** Elimina registro de la tabla '".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'.\n");
            fputs($this->_file, "    * Descripción: Realiza eliminación del registro ( delete ) de la tabla '".$this->_arr['aTableDetailOfMaster'][$i]['table_name']."'.\n");
            fputs($this->_file, "    * Nota: Requiere el valor \$_POST identificativo para buscar y eliminar registro en base de datos.*/ \n");
            fputs($this->_file, "    public function ".$tableMetodo."Delete()\n    {\n");
            fputs($this->_file, str_repeat(" ",8)."\$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'"
                .$this->_arr['aTableDetailOfMaster'][$i]['table_name']);
            fputs($this->_file,"_delete_pl.sql', \$_POST, true, 'ELIMINANDO REGISTRO'));\n");
            fputs($this->_file, str_repeat(" ",8)."return \$row[0];\n");
            fputs($this->_file, "    }\n\n");


            $arr=[];
            //echo '<pre>';var_dump($this->_arr['aTableDetailOfMaster']);echo '</pre>';die();
            //for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){
                $arr=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$i]['table_name']]);

                for ($j=0;$j<count($arr['aTableForeignKeysAssoc']);$j++){
                    //if (self::_foreignKeyNoReply($arr['aTableForeignKeysAssoc'][$j]['foreign_table_name'])) {
                        $tableMetodo = self::_strCamel($arr['aTableForeignKeysAssoc'][$j]['foreign_table_name'], 1);

                        fputs($this->_file, "    /** Lista de tabla foránea '".$arr['aTableForeignKeysAssoc'][$j]['foreign_table_name']."'.\n");
                        fputs($this->_file, "    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea '".$arr['aTableForeignKeysAssoc'][$j]['foreign_table_name']."'.\n");
                        fputs($this->_file, "    * @return result Resultado de la consulta de la de tabla foránea '".$arr['aTableForeignKeysAssoc'][$j]['foreign_table_name']."'.*/ \n");


                        fputs($this->_file, "    public function ".$tableMetodo."List() { ");

                        fputs($this->_file, "return DbFunc::exeQryFile(self::_path().'".
                            $arr['aTableForeignKeysAssoc'][$j]['foreign_table_name']."_list_select.sql', \$_POST); }\n\n");
                    //}
                }
            //}

            fputs($this->_file, "}\n\n");

            self::_fileBldEnd( "/class/".$this->_tableModel."/", self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name']).".php" );

        }
    }

}
