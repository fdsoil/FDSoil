<?php

trait AutoCrudPL
{

    private function _bldPL()
    {
        self::_dirBld("/class/".$this->_tableModel);
        self::_dirBld("/class/".$this->_tableModel."/sql/");
        self::_fileBldStart( "/class/".$this->_tableModel."/sql/", $this->_arr['table']."_create_pl.sql" );
        self::_bldPLMaster();
        self::_bldPLDetail();
        self::_fileBldEnd("/class/".$this->_tableModel."/sql/", $this->_arr['table']."_create_pl.sql");
    }

    private function _bldPLMaster()
    {

        fputs($this->_file, "-- Function: ".$this->_arr['schema'].".".$this->_arr['table']."_register(");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            fputs($this->_file, $this->_arr['aTableStructure'][$i]['data_type']);
            if ($i<(count($this->_arr['aTableStructure'])-1))
                fputs($this->_file, ", ");
            else
                fputs($this->_file, ")\n\n");
        }

        fputs($this->_file, "-- DROP FUNCTION ".$this->_arr['schema'].".".$this->_arr['table']."_register(");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            fputs($this->_file, $this->_arr['aTableStructure'][$i]['data_type']);
            if ($i<(count($this->_arr['aTableStructure'])-1))
                fputs($this->_file, ", ");
            else
                fputs($this->_file, ");\n\n");
        }

        fputs($this->_file, "CREATE OR REPLACE FUNCTION ".$this->_arr['schema'].".".$this->_arr['table']."_register(");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            fputs($this->_file, "i_".$this->_arr['aTableStructure'][$i]['column_name']." ".$this->_arr['aTableStructure'][$i]['data_type']);
            if ($i<(count($this->_arr['aTableStructure'])-1))
                fputs($this->_file, ", ");
            else
                fputs($this->_file, ")\n");
        }

        if (count($this->_arr['aTableDetailOfMaster']) === 0)
            fputs($this->_file, "  RETURNS character AS\n");
        else
            fputs($this->_file, "  RETURNS json AS\n");

        fputs($this->_file, "\$BODY\$\n");
        fputs($this->_file, "DECLARE\n");
        fputs($this->_file, str_repeat(" ",8)."v_existe boolean;\n");

        if (count($this->_arr['aTableDetailOfMaster']) === 0) {
            fputs($this->_file, str_repeat(" ",8)."o_return character;\n");
        } else {
            fputs($this->_file, str_repeat(" ",8)."v_".$this->_arr['aTableTablePrimaryKey'][0]." integer;\n");
            fputs($this->_file, str_repeat(" ",8)."o_return json;\n");
        }

        fputs($this->_file, "BEGIN\n");

        if (count($this->_arr['aTableDetailOfMaster']) === 0)
            fputs($this->_file, str_repeat(" ",8)."o_return:='';\n");
        else
            fputs($this->_file, str_repeat(" ",8)."o_return:=array_to_json(array['']);\n");

        fputs($this->_file, str_repeat(" ",8)."IF i_".$this->_arr['aTableTablePrimaryKey'][0]."=0 THEN\n");
        fputs($this->_file, str_repeat(" ",16)."SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM ");
        fputs($this->_file, $this->_arr['schema'].".".$this->_arr['table']."\n");
        fputs($this->_file, str_repeat(" ",24)."WHERE ");

        if ($this->_arr['aTableUniqueConstraint']===[])
            die("Error: La tabla <strong>'".$this->_arr['table']."'</strong> del esquema <strong>'".$this->_arr['schema'].
                "'</strong> debe tener campo(s) único(s) en base de datos...");

        for ($i=0;$i<count($this->_arr['aTableUniqueConstraint']);$i++){
            fputs($this->_file, $this->_arr['aTableUniqueConstraint'][$i]['column_name']."=i_".$this->_arr['aTableUniqueConstraint'][$i]['column_name']);
            if ($i<(count($this->_arr['aTableUniqueConstraint'])-1))
                fputs($this->_file, "\n".str_repeat(" ",40)."AND ");
            else
                fputs($this->_file, ";\n");
        }

        fputs($this->_file, str_repeat(" ",16)."IF  v_existe='f' THEN\n");
        fputs($this->_file, str_repeat(" ",24)."INSERT INTO ".$this->_arr['schema'].".".$this->_arr['table']."(\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            if ($this->_arr['aTableStructure'][$i]['column_name']!=$this->_arr['aTableTablePrimaryKey'][0]){
                fputs($this->_file, str_repeat(" ",32).$this->_arr['aTableStructure'][$i]['column_name']);
                if ($i<(count($this->_arr['aTableStructure'])-1))
                    fputs($this->_file, ",\n");
                else
                    fputs($this->_file, ")\n");
            }
        }

        fputs($this->_file, str_repeat(" ",24)."VALUES (\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){

            if ($this->_arr['aTableStructure'][$i]['column_name']!=$this->_arr['aTableTablePrimaryKey'][0]){
                fputs($this->_file, str_repeat(" ",32)."i_".$this->_arr['aTableStructure'][$i]['column_name']);
                if ($i<(count($this->_arr['aTableStructure'])-1))
                    fputs($this->_file, ",\n");
                else
                    fputs($this->_file, ");\n");
            }
        }


        if (count($this->_arr['aTableDetailOfMaster']) === 0) {

            fputs($this->_file, str_repeat(" ",24)."o_return:= 'C';\n");

        } else {

            fputs($this->_file, str_repeat(" ",24)."SELECT max(".$this->_arr['aTableTablePrimaryKey'][0].") INTO v_".$this->_arr['aTableTablePrimaryKey'][0]." FROM ");
            fputs($this->_file, $this->_arr['schema'].".".$this->_arr['table']."\n");
            fputs($this->_file, str_repeat(" ",32)."WHERE ");

            for ($i=0;$i<count($this->_arr['aTableUniqueConstraint']);$i++){

                fputs($this->_file, $this->_arr['aTableUniqueConstraint'][$i]['column_name']."=i_".$this->_arr['aTableUniqueConstraint'][$i]['column_name']);

                if ($i<(count($this->_arr['aTableUniqueConstraint'])-1))
                    fputs($this->_file, "\n".str_repeat(" ",40)."AND ");
                else
                    fputs($this->_file, ";\n");

            }

            fputs($this->_file, str_repeat(" ",24)."o_return:= array_to_json(array['C', v_".$this->_arr['aTableTablePrimaryKey'][0]."::character varying]);\n");

        }

        fputs($this->_file, str_repeat(" ",16)."ELSE\n");

        if (count($this->_arr['aTableDetailOfMaster']) === 0)
            fputs($this->_file, str_repeat(" ",24)."o_return:= 'T';\n");
        else
            fputs($this->_file, str_repeat(" ",24)."o_return:= array_to_json(array['T']);\n");

        fputs($this->_file, str_repeat(" ",16)."END IF;\n");
        fputs($this->_file, str_repeat(" ",8)."ELSE\n");
        fputs($this->_file, str_repeat(" ",16)."UPDATE ".$this->_arr['schema'].".".$this->_arr['table']." SET\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            if ($this->_arr['aTableStructure'][$i]['column_name']!=$this->_arr['aTableTablePrimaryKey'][0] &&
                (!self::_inMatrix($this->_arr['aTableStructure'][$i]['column_name'], $this->_arr['aTableUniqueConstraint']))){
                    fputs($this->_file, str_repeat(" ",24).$this->_arr['aTableStructure'][$i]['column_name']."=i_".
                    $this->_arr['aTableStructure'][$i]['column_name']);
                    if ($i<(count($this->_arr['aTableStructure'])-1))
                        fputs($this->_file, ",\n");
                    else
                        fputs($this->_file, "\n");
            }
        }

        if ($this->_arr['aTableTablePrimaryKey']===[])
            die("Error: La tabla '".$this->_arr['table']."' del esquema '".$this->_arr['schema']."' debe tener clave principal en base de datos...");

        fputs($this->_file, str_repeat(" ",16)."WHERE ".$this->_arr['aTableTablePrimaryKey'][0]."=i_".$this->_arr['aTableTablePrimaryKey'][0].";\n");

        if (count($this->_arr['aTableDetailOfMaster']) === 0)
            fputs($this->_file, str_repeat(" ",24)."o_return:= 'A';\n");
        else
            fputs($this->_file, str_repeat(" ",24)."o_return:= array_to_json(array['A']);\n");

        fputs($this->_file, str_repeat(" ",8)."END IF;\n");
        fputs($this->_file, str_repeat(" ",8)."RETURN o_return;\n");
        fputs($this->_file, "END;\n");
        fputs($this->_file, "\$BODY\$\n");
        fputs($this->_file, "  LANGUAGE plpgsql VOLATILE\n");
        fputs($this->_file, "  COST 100;\n");
        fputs($this->_file, "ALTER FUNCTION ".$this->_arr['schema'].".".$this->_arr['table']."_register(");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            fputs($this->_file, $this->_arr['aTableStructure'][$i]['data_type']);
            if ($i<(count($this->_arr['aTableStructure'])-1))
                fputs($this->_file, ", ");
            else
                fputs($this->_file, ")\n");
        }

        fputs($this->_file, "  OWNER TO postgres;\n\n");

        fputs($this->_file, "-- Function: ".$this->_arr['schema'].".".$this->_arr['table']."_delete(integer)\n\n");
        fputs($this->_file, "-- DROP FUNCTION ".$this->_arr['schema'].".".$this->_arr['table']."_delete(integer);\n\n");
        fputs($this->_file, "CREATE OR REPLACE FUNCTION ".$this->_arr['schema'].".".$this->_arr['table']);
        fputs($this->_file, "_delete(i_".$this->_arr['aTableTablePrimaryKey'][0]." integer)\n");
        fputs($this->_file, "  RETURNS character AS\n");
        fputs($this->_file, "\$BODY\$\n");
        fputs($this->_file, "DECLARE\n");
        fputs($this->_file, str_repeat(" ",8)."o_return character;\n");
        fputs($this->_file, "BEGIN\n");
        fputs($this->_file, str_repeat(" ",8)."o_return:='Z';\n");
        fputs($this->_file, str_repeat(" ",8)."DELETE FROM ".$this->_arr['schema'].".".$this->_arr['table']);
        fputs($this->_file, " WHERE ".$this->_arr['aTableTablePrimaryKey'][0]."=i_".$this->_arr['aTableTablePrimaryKey'][0].";\n");
        fputs($this->_file, str_repeat(" ",8)."o_return:='B';\n");
        fputs($this->_file, str_repeat(" ",8)."RETURN o_return;\n");
        fputs($this->_file, "END;\n");
        fputs($this->_file, "\$BODY\$\n");
        fputs($this->_file, "  LANGUAGE plpgsql VOLATILE\n");
        fputs($this->_file, "  COST 100;\n");
        fputs($this->_file, "ALTER FUNCTION ".$this->_arr['schema'].".".$this->_arr['table']."_delete(integer)\n");
        fputs($this->_file, "  OWNER TO postgres;\n\n");

    }

    private function _bldPLDetail()
    {

        for ($i=0;$i<count($this->_arr['aTableDetailOfMaster']);$i++){

            $arrDet=self::_bldArrDet([$this->_arr['schema'], $this->_arr['table'] , $this->_arr['aTableDetailOfMaster'][$i]['table_name']]);

            fputs($this->_file, "-- Function: ".$arrDet['schema'].".".$arrDet['table_detail']."_register(");

            for ( $j = 0 ; $j < count( $arrDet['aTableStructure'] ) ; $j++){
                 fputs($this->_file, $arrDet['aTableStructure'][$j]['data_type']);
                 if ( $j < (count($arrDet['aTableStructure'])-1) )
                     fputs($this->_file, ", ");
                 else
                     fputs($this->_file, ")\n\n");
            }

            fputs($this->_file, "-- DROP FUNCTION ".$arrDet['schema'].".".$arrDet['table_detail']."_register(");

            for ( $j = 0; $j < count( $arrDet['aTableStructure'] ); $j++ ){
                fputs($this->_file, $arrDet['aTableStructure'][$j]['data_type']);
                if ( $j < (count($arrDet['aTableStructure'])-1) )
                    fputs($this->_file, ", ");
                else
                    fputs($this->_file, ");\n\n");
            }

            fputs($this->_file, "CREATE OR REPLACE FUNCTION ".$arrDet['schema'].".".$arrDet['table_detail']."_register(");

            for ( $j = 0; $j < count( $arrDet['aTableStructure'] ); $j++ ){
                fputs($this->_file, "i_".$arrDet['aTableStructure'][$j]['column_name']." ".$arrDet['aTableStructure'][$j]['data_type']);
                if ( $j < (count($arrDet['aTableStructure'])-1) )
                    fputs($this->_file, ", ");
                else
                    fputs($this->_file, ")\n");
                }

                fputs($this->_file, "  RETURNS character AS\n");
                fputs($this->_file, "\$BODY\$\n");
                fputs($this->_file, "DECLARE\n");
                fputs($this->_file, str_repeat(" ",8)."v_existe boolean;\n");
                fputs($this->_file, str_repeat(" ",8)."o_return character;\n");
                fputs($this->_file, "BEGIN\n");
                fputs($this->_file, str_repeat(" ",8)."o_return:='';\n");
                fputs($this->_file, str_repeat(" ",8)."IF i_".$arrDet['aTableTablePrimaryKey'][0]."=0 THEN\n");

                fputs($this->_file, str_repeat(" ",16)."INSERT INTO ".$arrDet['schema'].".".$arrDet['table_detail']."(\n");

                for ( $j = 0; $j < count($arrDet['aTableStructure']); $j++){
                    if ($arrDet['aTableStructure'][$j]['column_name'] != $arrDet['aTableTablePrimaryKey'][0]){
                        fputs($this->_file, str_repeat(" ",24).$arrDet['aTableStructure'][$j]['column_name']);
                        if ( $j < ( count($arrDet['aTableStructure'])-1 ) )
                            fputs($this->_file, ",\n");
                        else
                            fputs($this->_file, ")\n");
                    }
                }

                fputs($this->_file, str_repeat(" ",16)."VALUES (\n");

                for ( $j = 0; $j < count( $arrDet['aTableStructure'] ); $j++ ){

                    if ($arrDet['aTableStructure'][$j]['column_name'] != $arrDet['aTableTablePrimaryKey'][0]){
                        fputs($this->_file, str_repeat(" ",24)."i_".$arrDet['aTableStructure'][$j]['column_name']);
                        if ( $j < ( count( $arrDet['aTableStructure'] )-1) )
                            fputs($this->_file, ",\n");
                        else
                            fputs($this->_file, ");\n");
                    }
                }

                fputs($this->_file, str_repeat(" ",16)."o_return:= 'C';\n");
                fputs($this->_file, str_repeat(" ",8)."ELSE\n");
                fputs($this->_file, str_repeat(" ",16)."UPDATE ".$arrDet['schema'].".".$arrDet['table_detail']." SET\n");

                for ( $j=0; $j < count( $arrDet['aTableStructure'] ); $j++ ){
                    if ( $arrDet['aTableStructure'][$j]['column_name'] != $arrDet['aTableTablePrimaryKey'][0] &&
                        ( !self::_inMatrix($arrDet['aTableStructure'][$j]['column_name'], $arrDet['aTableUniqueConstraint'] ) ) ){
                            fputs($this->_file, str_repeat(" ",24).$arrDet['aTableStructure'][$j]['column_name']."=i_".
                            $arrDet['aTableStructure'][$j]['column_name']);
                            if ( $j < ( count( $arrDet['aTableStructure'] )-1 ) )
                                fputs($this->_file, ",\n");
                            else
                                fputs($this->_file, "\n");
                    }
                }

                if ($arrDet['aTableTablePrimaryKey']===[])
                    die("Error: La tabla '".$arrDet['table_detail']."' del esquema '".$arrDet['schema']."' debe tener clave principal en base de datos...");

                fputs($this->_file, str_repeat(" ",16)."WHERE ".$arrDet['aTableTablePrimaryKey'][0]."=i_".$arrDet['aTableTablePrimaryKey'][0].";\n");
                fputs($this->_file, str_repeat(" ",16)."o_return:= 'A';\n");
                fputs($this->_file, str_repeat(" ",8)."END IF;\n");
                fputs($this->_file, str_repeat(" ",8)."RETURN o_return;\n");
                fputs($this->_file, "END;\n");
                fputs($this->_file, "\$BODY\$\n");
                fputs($this->_file, "  LANGUAGE plpgsql VOLATILE\n");
                fputs($this->_file, "  COST 100;\n");
                fputs($this->_file, "ALTER FUNCTION ".$arrDet['schema'].".".$arrDet['table_detail']."_register(");

                for ( $j = 0; $j < count( $arrDet['aTableStructure'] ); $j++ ){
                    fputs($this->_file, $arrDet['aTableStructure'][$j]['data_type']);
                    if ($j < ( count( $arrDet['aTableStructure'] )-1 ) )
                        fputs($this->_file, ", ");
                    else
                        fputs($this->_file, ")\n");
                }

                fputs($this->_file, "  OWNER TO postgres;\n\n");

                fputs($this->_file, "-- Function: ".$arrDet['schema'].".".$arrDet['table_detail']."_delete(integer)\n\n");
                fputs($this->_file, "-- DROP FUNCTION ".$arrDet['schema'].".".$arrDet['table_detail']."_delete(integer);\n\n");
                fputs($this->_file, "CREATE OR REPLACE FUNCTION ".$arrDet['schema'].".".$arrDet['table_detail']);
                fputs($this->_file, "_delete(i_".$arrDet['aTableTablePrimaryKey'][0]." integer)\n");
                fputs($this->_file, "  RETURNS character AS\n");
                fputs($this->_file, "\$BODY\$\n");
                fputs($this->_file, "DECLARE\n");
                fputs($this->_file, str_repeat(" ",8)."o_return character;\n");
                fputs($this->_file, "BEGIN\n");
                fputs($this->_file, str_repeat(" ",8)."o_return:='Z';\n");
                fputs($this->_file, str_repeat(" ",8)."DELETE FROM ".$arrDet['schema'].".".$arrDet['table_detail']);
                fputs($this->_file, " WHERE ".$arrDet['aTableTablePrimaryKey'][0]."=i_".$arrDet['aTableTablePrimaryKey'][0].";\n");
                fputs($this->_file, str_repeat(" ",8)."o_return:='B';\n");
                fputs($this->_file, str_repeat(" ",8)."RETURN o_return;\n");
                fputs($this->_file, "END;\n");
                fputs($this->_file, "\$BODY\$\n");
                fputs($this->_file, "  LANGUAGE plpgsql VOLATILE\n");
                fputs($this->_file, "  COST 100;\n");
                fputs($this->_file, "ALTER FUNCTION ".$arrDet['schema'].".".$arrDet['table_detail']."_delete(integer)\n");
                fputs($this->_file, "  OWNER TO postgres;\n\n");

        }

    }

}
