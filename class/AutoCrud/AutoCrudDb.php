<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

trait AutoCrudDb
{

    private function _path() { return "../../../".$_SESSION['FDSoil']."/class/AutoCrud/sql/"; }

    public function getDbNamesList()
    { 
        return DbFunc::fetchAllRow(DbFunc::exeQryFile(self::_path()."get_dbnames_list.sql", null));
    }

    public function getSchemasOfBd()
    {
        return DbFunc::exeQryFile(self::_path()."get_schemas_of_bd_select.sql", null);
    }

    public function getTablesOfBd()
    {
        return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::_path()."get_tables_of_bd_select.sql", $_POST)));
    }

    /*public function getSchemasAndTablesOfBd()
    {
        return json_encode(DbFunc::fetchAllRow(DbFunc::exeQryFile(self::_path()."get_schemas_and_tables_of_bd_select.sql", null)));
    }*/

    private function _getTableStructure($arr)
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'get_table_structure_select.sql', $arr));
    }

    private function _getTablePrimaryKey($arr)
    {
        return DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'get_table_primary_key_select.sql', $arr));
    }

    private function _getTableDetailForeignKeysAssoc($arr)
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'get_table_detail_foreign_keys_assoc_select.sql', $arr));
    }

    private function _getTableMasterForeignKeysAssoc($arr)
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'get_table_master_foreign_keys_assoc_select.sql', $arr));
    }

    private function _getTableForeignKeysAssocMasterDetail($arr)
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'get_table_foreign_keys_assoc_master_detail_select.sql', $arr));
    }

    private function _getTableUniqueConstraint($arr)
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'get_table_unique_constraint_get_select.sql', $arr));
    }

    private function _getTableDetailOfMaster($arr)
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'get_table_detail_of_master_select.sql',$arr));
    }

}
