<?php
trait FuncReqs
{
    /**
    * FunReqs
    *
    * Este Trait es un validador de las entradas de datos al servidor (provenientes del cliente).
    * 
    * @author Ernesto Jiménez <fdsoil123@gmail.com>
    * @version 1.0
    * GENERAL INFORMATION:
    * 
    * label -> [label of caption]
    *
    * format -> [lowercase, uppercase, integer, float, float3d]
    *
    * required -> [true, false]
    *
    * constrain -> [ number, only_letter, email, textarea, code, fullname, username, date, firm_name, ced, password ] 
    *
    * [length , length-min, length-max, length-min-max]
    *
    * [quantity-min, quantity-max, quantity-min-max]
    *
    * [date-min, date-max, date-min-max]
    *
    * [time-min, time-max, time-min-max]
    * isset($var)
    */
    public function formatReqs( $aReq, $aVal)
    {

        foreach ($aReq as $rKey => $rVal){
            if (array_key_exists($rKey, $aVal)){           
                foreach ($aVal as $vKey => $vVal){
                    if ($rKey===$vKey && isset($vVal['format'])){
                        if ($vVal['format']==='lowercase'){
                            $aReq[$rKey] = strtolower($rVal);
                        }else if ($vVal['format']==='uppercase'){
                            $aReq[$rKey] = strtoupper($rVal);
                        }else if ($vVal['format']==='integer'){
                            $aReq[$rKey] = $this->quita_formato_numerico($rVal);                            
                            $aReq[$rKey] = number_format($aReq[$rKey], 0, ',', '.');
                        }else if ($vVal['format']==='float'){
                            $aReq[$rKey] = $this->quita_formato_numerico($rVal);
                            $aReq[$rKey] = number_format($aReq[$rKey], 2, ',', '.');
                        }else if ($vVal['format']==='float3d'){
                            $aReq[$rKey] = $this->quita_formato_numerico($rVal);
                            $aReq[$rKey] = number_format($aReq[$rKey], 3, ',', '.');
                        }
                    }
                }
            }
        }

        return $aReq;

    }

    public function valReqs( $aReq, $aVal)//                                                
    {

        $aValReqsMsg = [];
        $aTypeOfExpReg = array(                                 
                               "number"      => "/^[0-9\.,]+$/",
                               "letter_only" => "/^[a-zA-Z]+$/",
                               "code"        => "/^[0-9a-zA-Z]+$/",
                               "doc_num"     => "/^[0-9a-zA-Z.-()\/]+$/", 
                               "fullname"    => "/^[a-zA-Z\s.,ÑñÁÉÍÓÚáéíóú]+$/",
                               "username"    => "/^[0-9a-zA-Z._]+$/", //"textarea"    => "/^[0-9a-zA-Z\s.,,ÑñÁÉÍÓÚáéíóú()-°#\/\"]+$/",
                               "textarea"    => "/^[0-9a-zA-ZÑñÁÉÍÓÚáéíóú\s.,;:°!#$%\/()=¿?¡*-]+$/",//"&+
                               "date"        => "/^\d{4}[\/-]\d{2}[\/-]\d{2}$|^\d{2}[\/-]\d{2}[\/-]\d{4}$/",
                               "firm_name"   => "/^[0-9a-zA-Z\s.,ÑñÁÉÍÓÚáéíóú()&]+$/",
                               "email"       => "/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/",
                               "ced"         => "/^[vepVEP]{1}[0-9]+$/",
                               "password"    => "/^[0-9a-zA-Z\$%¿|#)!@(¬]{1,}$/", //"doc_num"     => "/^[0-9a-zA-Z.()-\/]+$/"  
                               "doc_num"     => "/^[0-9a-zA-Z.\/()°#-]+$/"                                          
                                );
        foreach ($aReq as $rKey => $rVal){
            if (array_key_exists($rKey, $aVal)){           
                foreach ($aVal as $vKey => $vVal){                    
                    if ($rKey===$vKey){
                        if ($vVal['required'] && empty($rVal)){ 
                            array_push($aValReqsMsg,"Disculpe: El contenido del campo '<b>".$vVal['label'].
                            "</b>' es requerido."); 
                        }else{   
                            foreach ($aTypeOfExpReg as $tKey => $tVal){
                                if (isset($vVal['constrain']) && $rKey===$vKey && $vVal['constrain']===$tKey){
                                    if ($rVal && !preg_match($tVal,$rVal)){
                                        array_push($aValReqsMsg,"Disculpe: El contenido del campo '<b>".
                                        $vVal['label']."</b>' es inválido."); 
                                    }
                                }
                            }
                        }
                        if (isset($vVal['length'])
                        && strlen($rVal)!=$vVal['length']){
                            array_push($aValReqsMsg,"Disculpe: El largo del campo '<b>".$vVal['label'].
                            "</b>' debe ser exactamente igual a <b>".$vVal['length']."</b> dígito(s)."); 
                        }else if (isset($vVal['length-max'])
                        && strlen($rVal)>$vVal['length-max']){
                            array_push($aValReqsMsg,"Disculpe: El largo del campo '<b>".$vVal['label'].
                            "</b>' debe ser menor o igual a <b>".$vVal['length-max']."</b> dígito(s)."); 
                        }else if (isset($vVal['length-min'])
                        && strlen($rVal)<$vVal['length-min']){
                            array_push($aValReqsMsg,"Disculpe: El largo del campo '<b>".$vVal['label'].
                            "</b>' debe ser mayor o igual a <b>".$vVal['length-min']."</b> dígito(s)."); 
                        }else if (isset($vVal['length-min-max'])){
                            $arr = explode( "-", $vVal['length-min-max']);
                            if (strlen($rVal)<$arr[0] || strlen($rVal)>$arr[1]){   
                                array_push($aValReqsMsg,"Disculpe: El largo del campo '<b>".$vVal['label'].
                                "</b>' debe ser entre <b>".str_replace("-", " y ", $vVal['length-min-max'])."</b> dígito(s).");  
                            }   
                        }else if (isset($vVal['quantity-min']) 
                        && (self::quita_formato_numerico($rVal)*1)<($vVal['quantity-min']*1)){
                            array_push($aValReqsMsg,"Disculpe: La cantidad del campo '<b>".$vVal['label'].
                            "</b>' debe ser mínimo <b>".$vVal['quantity-min']."</b>.");
                        }else if (isset($vVal['quantity-max'])
                        && (self::quita_formato_numerico($rVal)*1)>($vVal['quantity-max']*1)){
                            array_push($aValReqsMsg,"Disculpe: La cantidad del campo '<b>".$vVal['label'].
                            "</b>' debe ser máximo <b>".$vVal['quantity-max']."</b>.");
                        }else if (isset($vVal['quantity-min-max'])){
                            $arr = explode( "-", $vVal['quantity-min-max']);
                            if ((self::quita_formato_numerico($rVal)*1)<($arr[0]*1) 
                            || (self::quita_formato_numerico($rVal)*1)>($arr[1]*1)){   
                                array_push($aValReqsMsg,"Disculpe: La cantidad del campo '<b>".$vVal['label'].
                                "</b>' debe ser entre <b>".str_replace("-", " y ", $vVal['quantity-min-max'])."</b>.");  
                            }                          
                        }else if (isset($vVal['date-min'])
                        && (str_replace("-", "", $rVal)*1)<(str_replace("-", "", $vVal['date-min'])*1)){
                            array_push($aValReqsMsg,"Disculpe: La fecha del campo '<b>".$vVal['label'].
                            "</b>' debe ser mayor o igual a <b>".$vVal['date-min']."</b>."); 
                        }else if (isset($vVal['date-max'])
                        && (str_replace("-", "", $rVal)*1)>(str_replace("-", "", $vVal['date-max'])*1)){
                            array_push($aValReqsMsg,"Disculpe: La fecha del campo '<b>".$vVal['label'].
                            "</b>' debe ser menor o igual a <b>".$vVal['date-max']."</b>."); 
                        }else if (isset($vVal['date-min-max'])){
                            $arr = explode( "/", $vVal['date-min-max']);
                            if ((str_replace("-", "", $rVal)*1)<(str_replace("-", "", $arr[0])*1) 
                            || (str_replace("-", "", $rVal)*1)>(str_replace("-", "", $arr[1])*1)){   
                                array_push($aValReqsMsg,"Disculpe: La fecha del campo '<b>".$vVal['label'].
                                "</b>' debe ser entre <b>".str_replace("/", " y ", $vVal['date-min-max'])."</b>.");  
                            }   
                        }else if (isset($vVal['time-min'])
                        && (str_replace(":", "", $rVal)*1)<(str_replace(":", "", $vVal['time-min'])*1)){
                            array_push($aValReqsMsg,"Disculpe: La hora del campo '<b>".$vVal['label'].
                            "</b>' debe ser mayor o igual a <b>".$vVal['time-min']."</b>."); 
                        }else if (isset($vVal['time-max'])
                        && (str_replace(":", "", $rVal)*1)>(str_replace(":", "", $vVal['time-max'])*1)){
                            array_push($aValReqsMsg,"Disculpe: La hora del campo '<b>".$vVal['label'].
                            "</b>' debe ser menor o igual a <b>".$vVal['time-max']."</b>."); 
                        }else if (isset($vVal['time-min-max'])){
                            $arr = explode( "/", $vVal['time-min-max']);
                            if ((str_replace(":", "", $rVal)*1)<(str_replace(":", "", $arr[0])*1) 
                            || (str_replace(":", "", $rVal)*1)>(str_replace(":", "", $arr[1])*1)){   
                                array_push($aValReqsMsg,"Disculpe: La hora del campo '<b>".$vVal['label'].
                                "</b>' debe ser entre <b>".str_replace("/", " y ", $vVal['time-min-max'])."</b>.");  
                            }   
                        } 
                    }
                }
            }else{
                die("<br><center>Error: No existe el campo con el nombre:<b>'".$rKey."'</b>.");
            }
        }

        return $aValReqsMsg;                      

    }

}

/* End of the Trait */
