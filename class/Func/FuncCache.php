<?php
trait FuncCache
{
    /**
    * Esta Clase es una clase intermediaria entre pear-cache y la aplicacion
    * 
    * @author Gregorio Bolivar <gbolivar@mppi.gob.ve>
    * @version 1.0
    * 
    */
    public $cache;
    /**
     * Contructor 
     */
    public function initCache() {
        try {            
            $appName=end(explode('/',substr($_SERVER['DOCUMENT_ROOT'],1)));
            $this->cache = new Cache('file', array('cache_dir' => '/tmp/'.$appName.'/'));
        } catch (Exception $e) {
            die('Error: Debes proceder a instalar <b>pear</b>, debes ejecutar: <b><i>apt-get install php-pear</i></b>.');  
        }
    }

    /**
     * Method encargado de guardar la informacion en cache
     * 
     * @param string $key Este es el identificador del cache
     * @param string|interger $value Contenido de lo que se desea guardar en el cache
     */
    public function setCache($key, $value) {
        self::initCache();
        $this->cache->save($key, $value);
    }

    /**
     * Method encargado de extraer la informacion en cache
     * 
     * @param string $key Este es el identificador del cache
     * @return string|interger
     */
    public function getCache($key) {  
        self::initCache();
        if ($this->cache->isCached($key)) {           
            return $this->cache->get($key);
        } else {
            die('Variable (' . $key . '), no se encuentra registrada.');
        }
    }

    /**
     * Method encargado de eliminar la informacion en cache
     * 
     * @param string $key Esta es el identificador del cache
     */
    public function rmCache($key) {
        self::initCache();
        if ($this->cache->isCached($key)) {
            $this->cache->remove($key);
        } else {
            die('!Error, Variable (' . $key . '), no se encuentra registrada. ');
        }
    }

}

