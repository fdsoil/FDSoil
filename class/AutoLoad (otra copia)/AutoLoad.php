<?php
namespace FDSoil;

class AutoLoad
{
  
    public function autoRequireOnce()
    {
        spl_autoload_register(function ($sNameSpaceClass)
        {
            $aNameSpaceClass=explode('\\',$sNameSpaceClass);           
            $classNamePHP = self::classNamePHP( $aNameSpaceClass ) ;
            if (!file_exists($classNamePHP)) {
                die(
                    "<center>La clase: <strong>'".
                    $classNamePHP.
                    "'</strong> con espacio de nombre ".
                    $sNameSpaceClass.
                    " no existe. </center>"
                );
            }
            require_once $classNamePHP;
        });
    }

    private function className($a)
    {
        $resp="";
        $len=count($a);
        if ($len==2)
            $resp=$a[1]."/".$a[1];
        else if ($len > 2) {
            for ($i=1;$i<$len;$i++)
               $resp.=$a[$i]."/";
            $resp = substr($resp, 0,strlen($resp) -1);
        }
        return $resp;
    }

    private function classNamePHP($arr)
    {
        return '../../../'.$_SESSION[$arr[0]].'/class/'.self::className($arr).".php";
    } 

}

