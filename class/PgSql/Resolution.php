<?php
namespace FDSoil\PgSql;
/** Resolución de PgSql
* @author Ernesto Jiménez <fdsoil123@gmail.com>
*/
class Resolution implements \FDSoil\DbFunc\Interfaz
{

    public function fetchArray($result) { return pg_fetch_array($result); }

    public function fetchAssoc($result) { return pg_fetch_assoc($result); } 
    
    public function fetchRow($result) { return pg_fetch_row($result); }

    public function fetchResult($result) { return pg_fetch_result($result); }

    public function fetchAll($result) { return pg_fetch_all($result); }

    public function numFields($result) { return pg_num_fields($result); }

    public function numRows($result) { return pg_num_rows($result); }

    public function affectedRows($result) { return pg_affected_rows($result); }

}

