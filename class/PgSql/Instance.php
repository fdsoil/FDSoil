<?php
namespace FDSoil\PgSql;
/** Instancia de PgSql
* @author Ernesto Jiménez <fdsoil123@gmail.com>
*/

class Instance implements \FDSoil\DataBase\Interfaz
{
    
    private $conexion;

    function cerrar()
    { 
        if (!empty($this->conexion)) 
	    @pg_close($this->conexion);	        
    }

    function conectar( $db = null, $persistence = null)
    {
        require '../../../'.$_SESSION['myApp'].'/config/db.php';
        $conexion= 'pg_'.$persistence.'connect';
        $this->conexion = $conexion("host=$servidor port=$port dbname=$dbname user=$usuario password=$password");
        return (!$this->conexion) ? false : $this->conexion;
    }
        
    function ejecutar($sql)
    {
        if ($this->conexion)
            return pg_query($sql);
    }

}

