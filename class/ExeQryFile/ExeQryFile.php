<?php
namespace FDSoil;

require_once("ExeQryFileAux.php");

class ExeQryFile
{

    use \ExeQryFileAux;

    private $_obj;
    /** Constructor.
    * Descripción: Construye la conexión a la base de datos.
    * Devuelve TRUE en caso de éxito o FALSE en caso de error.*/
    function __construct($db=null, $persistence=null)
    {
        //echo $db;die();
        $this->_obj = new \FDSoil\DataBase();
        $this->_obj->conectar($db, $persistence);
    }

    /** Ejecuta un query almacenado en un archivo plano.
    * Descripción: Ejecuta un query o sentencia SQL almacenado en un archivo de texto o plano.
    *
    * Nota: El Query puede recibir parametro(s)
    *
    * @param $fileSql Ubicación física más nombre del archivo donde está almacenado el query o sentencia SQL
    * @param $array Arreglo de índice asociativo (etiquetas) y valores a remplazar en el query o sentencia(s) SQL
    * @param $doAudit Recibe TRUE si el query o sentencia SQL debe ser auditado
    * @param $coment Si el query sentencia SQL es auditado, debe recibir un comentario
    * @return $return Devuelve un resultado de la ejecución del query o sentencia SQL.*/
    function exeQryFile($fileSql, $array=[], $doAudit=false, $coment='')
    {
        $qry = self::bldQryFile($fileSql, $array);
        $return = $this->_obj->ejecutar( $qry);
        if ( $return && $_SESSION['audit'] && $doAudit)
            \FDSoil\Audit::doAudit($qry,$coment);
        return $return;
    }

    /** Constructor.
    * Descripción: Construye la conexión a la base de datos.
    * Devuelve TRUE en caso de éxito o FALSE en caso de error.*/
    function __destruct()
    {
        //$this->_obj->cerrar();
    }

}

