<?php
trait ExeQryFileAux
{

    /** Construye un query a partir de un archivo plano y un arreglo de parametros.
    * Descripción: Construir un query (instrucción SQL) a partir de un archivo txt y un arreglo de parametros.
    * Este método o función recibe el nombre y la ubicación de un archivo plano,
    * el cual contiene un query que puede contener etiquetas para ser remplazados por valores.
    * Los parametros son envidos en un arreglo asociativo, sus indices funcionan como etiquetas.
    *
    * Nota: Su ventaja es que permite tener separado el código de programación de las instrucciones SQL.
    * @param string $fileSql Ruta de un archivo plano más su respectivo nombre (paht/nameName.ext)
    * @param array $array Arreglo asociativo (indices que funcionan como etiquetas) con valores (parametros)
    * @return string $string Devuelve una en cadena de texto con valores reemplazados (query, instrucción SQL).*/
    function bldQryFile($fileSql, $array)
    {
        if (file_exists($fileSql)) {
            $Qry = \FDSoil\Func::read_txt($fileSql);
            if (is_array($array))
                $Qry = \FDSoil\Func::replace_data($array, $Qry);
            return $Qry;
        } else
            die("<br/><center>Error: El archivo  <b>[".$fileSql.']</b> no fue encontrado, <br/>, revisar el nombre o la ruta del mismo.');
    }

    /** Muestra query almacenado en archivo plano y detiene la acción.
    * Descripción: Mostrar query o sentencia(s) de SQL almacenado en archivo plano y detiene la ejecución del programa.
    * @param string $fileSql La ruta concatenada con el nombre del archivo plano
    * @param array $array Arreglo de índice asociativo (etiquetas) y valores a remplazar en el query o sentencia(s) SQL
    * @return Cadena de caracteres representando el query con valores remplazados.*/
    function seeQryFile($fileSql, $array)
    {
        die(self::printQryFile($fileSql, $array));
    }

    /** Imprime query almacenado en archivo plano.
    * Descripción: Imprimir un query o sentencia(s) de SQL almacenado en archivo plano.
    * @param string $fileSql La ruta concatenada con el nombre del archivo plano
    * @param array $array Arreglo de índice asociativo (etiquetas) y valores a remplazar en el query o sentencia(s) SQL
    * @return string Cadena de caracteres representando el query con valores remplazados.*/
    function printQryFile($fileSql, $array)
    {
        return self::bldQryFile($fileSql, $array);
    }

}

