<?php
namespace FDSoil;

class AutoLoad
{

    static private $_aNameSpaceClass = [];
  
    public function autoRequireOnce()
    {
        spl_autoload_register(function ($sNameSpaceClass)
        {
            self::$_aNameSpaceClass = explode('\\',$sNameSpaceClass);
            $classNamePHP = self::classNamePHP() ;
            if (!file_exists($classNamePHP)) {
                die(
                    "<center>La clase: <strong>'".
                    $classNamePHP.
                    "'</strong> con espacio de nombre ".
                    $sNameSpaceClass.
                    " no existe. </center>"
                );
            }
            require_once $classNamePHP;
        });
    }

    private function className()
    {
        $resp = "";
        $len = count( self::$_aNameSpaceClass );
        if ( $len == 2 )
            $resp = self::$_aNameSpaceClass[1] ."/" . self::$_aNameSpaceClass[1];
        else if ( $len > 2 ) {
            for ( $i = 1; $i < $len; $i++ )
               $resp .= self::$_aNameSpaceClass[$i] . "/";
            $resp = substr( $resp, 0, strlen( $resp ) -1 );
        }
        return $resp;
    }

    private function classNamePHP()
    {
        return '../../../' . $_SESSION[ self::$_aNameSpaceClass[0] ] . '/class/' . self::className() . ".php";
    } 

}

