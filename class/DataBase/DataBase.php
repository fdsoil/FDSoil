<?php
namespace FDSoil;

class DataBase
{
    private $_obj;

    public function conectar($db=null, $persistente=null)
    {   
        require ('../../../'.$_SESSION['myApp'].'/config/db.php'); 
        $obj="\FDSoil\\".$dbms.'\\Instance';
        $this->_obj = new $obj();
        return $this->_obj->conectar($db, $persistente);
    }

    public function ejecutar($sql) { return $this->_obj->ejecutar($sql); }

    public function cerrar() { return $this->_obj->cerrar(); }

}

