<?php
namespace FDSoil\DataBase;

interface Interfaz
{
    public function conectar($db=null, $persistente=null);
    public function cerrar();
    public function ejecutar($sql);
}

