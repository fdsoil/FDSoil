<?php
trait ControllPksAux
{
    /**
    * ControlAux
    *
    * @author Ernesto Jiménez <fdsoil123@gmail.com>
    * @version 3.0
    * Auxiliar del Controlador Frontal:
    * 
    */

    private function reqsURI()
    {

/*

array(6) {
  [0]=> ""
  [1]=> "appOrg"
  [2]=> "pickList"
  [3]=> "control"
  [4]=> "tel_cod_area_nac"
  [5]=> ""
}

*/


         $aResp = [];
         $aResp['path'] = '../../../../..';
         $aResp['dir'] = self::asDirPhy($this->_aURI[$this->_nPos-2]);
         $aResp['file'] = '';
         $aResp['params'] = [];
         $j = 0;
         //echo $this->_nPos;die();
         //$aResp['file'] = '/'.$this->_aURI[$this->_nPos+2];
         $aResp['file'] = '/index.php';
         //$aResp['params'][0] = $this->_aURI[$this->_nPos+3];
         for ( $i = $this->_nPos+2 ; $i < count($this->_aURI) ; $i++ ) {
             $aResp['params'][$j++] = $this->_aURI[$i];
             if ( $i > 4)
                 $aResp['path'] .= '/..';
         }
         return $aResp;
    }                                                                                         
                                                                                                                      
    private function asDirPhy($dir)
    {    
         $resp = "";    
         if ( $dir === $_SESSION['FDSoil'] )
            $resp = "FDSoil";
         else if ( $dir === $_SESSION['appOrg'] )
            $resp = "appOrg";
         else if ( $dir === $_SESSION['myApp'] )
            $resp = "myApp";
         return $_SESSION[$resp];
    }

    private function formBegin()
    {
        $view="../../../".$_SESSION['FDSoil']."/html/";
        $view.="pickList.html";
        $this->_oLayout = new \FDSoil\XTemplate($view);      
        //self::noScript($this->_oLayout);
        \FDSoil\Func::appShowId($this->_oLayout);
    }

    private function formEnd()
    {
        $this->_oLayout->assign('PATH', $this->_aReqs['path']); 
        $this->_oLayout->assign('DIR', $this->_aReqs['dir']); 
        //$this->_oLayout->assign('MODULE', $this->_module); 
        //$this->_oLayout->assign('USER_DATA', $this->_aView['userData']);
        //if ( $this->_aView['load'] != [] )
        //    $this->_oLayout->assign('LOAD', $this->_aView['load']);
        $this->_oLayout->assign('INCLUDE', $this->_aView['include']);
        $this->_oLayout->assign('CONTENT', $this->_aView['content']);
        $this->_oLayout->parse('main'); 
        $this->_oLayout->out('main');
    }                                                                                                                  
                                                                                                                             
    private function noScript($xtpl)                                                                                         
    {                                                                                                                        
        $noScript="<noscript><p>Para poder visualizar esta página web, necesitas tener activado JavaScript.</p></noscript>"; 
        $xtpl->assign('NOSCRIPT', $noScript);                                                                                
    }              

}

/* End of the Trait */
