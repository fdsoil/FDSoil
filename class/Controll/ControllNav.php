<?php
namespace FDSoil\Controll;

//require_once('../../../'.$_SESSION['FDSoil'].'/class/DbFunc/DbFunc.php');
require_once('../../../'.$_SESSION['FDSoil'].'/class/AutoLoad/AutoLoad.php');
require_once('../../../'.$_SESSION['FDSoil'].'/class/Controll/ControllNavAux.php');

class ControllNav
{
    /**
    * Controll
    *
    * @author Ernesto Jiménez <fdsoil123@gmail.com>
    * @version 3.0
    * Controlador Frontal:
    * 
    */

    use \ControllNavAux;

    private $_path;
    private $_aURI;
    private $_nPos;
    private $_module;
    private $_aFilesName;
    private $_aReqs;
    private $_oLayout;
    private $_aView = [ "userData" => null, 
                        "load"     => null, 
                         "include" => null, 
                         "content" => null ];

    public function __construct($arr)
    {
        $this->_path       = $arr['path'];
        $this->_aFilesName = $arr['filesName'];
        $this->_aURI       = explode('/',$_SERVER['REQUEST_URI']);
        $this->_nPos       = 1;
        $this->_module     = $this->_aURI[$this->_nPos+1];
        $this->_aReqs      = self::reqsURI();
    }

    public function execute()
    {
       if ( file_exists( $this->_path.$this->_module.$this->_aReqs['file']) && $this->_module!= 'control' ) {
           header('Content-type: text/html; charset=utf-8');
           \FDSoil\AutoLoad::autoRequireOnce();
           \FDSoil\Audit::validateAll();
           if ($this->_aReqs['file'] === '/index.php')
               self::formBegin();
           require_once( $this->_path.$this->_module.$this->_aReqs['file'] );
           $oSub = new \SubIndex();
           $this->_aView = $oSub->execute($this->_aReqs);
           if ( $this->_aReqs['file'] === '/index.php' )
               self::formEnd();
       }else
           die("<br>No es ruta valida");
    }                                                                                                                                       

}

