<?php
namespace FDSoil\Controll;

require_once('../../../'.$_SESSION['FDSoil'].'/class/DbFunc/DbFunc.php');
require_once('../../../'.$_SESSION['FDSoil'].'/class/AutoLoad/AutoLoad.php');
require_once('../../../'.$_SESSION['FDSoil'].'/class/Controll/ControllRepAux.php');

class ControllRep
{
    /**
    * Controll
    *
    * @author Ernesto Jiménez <fdsoil123@gmail.com>
    * @version 3.0
    * Controlador Frontal:
    * 
    */

    use \ControllRepAux;

    private $_path;
    private $_aURI;
    private $_nPos;
    private $_module;
    private $_aFilesName;
    private $_aReqs;

    public function __construct($arr)
    {
        $this->_path       = $arr['path'];
        $this->_aFilesName = $arr['filesName'];
        $this->_aURI       = explode('/',$_SERVER['REQUEST_URI']);
        $this->_nPos       = 3;
        $this->_module     = $this->_aURI[$this->_nPos];
        $this->_aReqs      = self::reqsURI();
    }

    public function execute()
    {
       //echo $this->_path.$this->_aReqs['file'];die();
       if ( file_exists( $this->_path.$this->_aReqs['file'])) {
           header('Content-type: text/html; charset=utf-8');
           \FDSoil\AutoLoad::autoRequireOnce();
           \FDSoil\Audit::validateAll();
           //echo $this->_path.$this->_module.$this->_aReqs['file'];die();
           require_once( $this->_path.$this->_aReqs['file'] );
           //new \SubIndex();
       } else
           die("<br>No es ruta valida");
    }                                                                                                                                       

}

