<?php
trait ControllRqsAux
{
    /**
    * ControlAux
    *
    * @author Ernesto Jiménez <fdsoil123@gmail.com>
    * @version 3.0
    * Auxiliar del Controlador Frontal:
    * 
    */

    private function reqsURI()
    {
         $aResp = [];
         $aResp['path'] = '../../../../..';
         $aResp['dir'] = self::asDirPhy($this->_aURI[$this->_nPos-2]);
         $aResp['file'] = '';
         $aResp['params'] = [];
         $aResp['file'] = '/'.$this->_aURI[$this->_nPos+2];
         $aResp['params'][0] = $this->_aURI[$this->_nPos+3];
         return $aResp;
    }                                                                                         
                                                                                                                      
    private function asDirPhy($dir)
    {    
         $resp = "";    
         if ( $dir === $_SESSION['FDSoil'] )
            $resp = "FDSoil";
         else if ( $dir === $_SESSION['appOrg'] )
            $resp = "appOrg";
         else if ( $dir === $_SESSION['myApp'] )
            $resp = "myApp";
         return $_SESSION[$resp];
    }

}

/* End of the Trait */
