<?php
namespace FDSoil;

class AutoLoad
{
  
    public function autoRequireOnce()
    {
        spl_autoload_register(function ($sNameSpaceClass)
        {
            $aNameSpaceClass=explode('\\',$sNameSpaceClass);           
            $classNamePHP = self::classNamePHP( $aNameSpaceClass ) ;
            if (!file_exists($classNamePHP)) {
                die(
                    "<center>La clase: <strong>'".
                    $classNamePHP.
                    "'</strong> con espacio de nombre ".
                    $sNameSpaceClass.
                    " no existe. </center>"
                );
            }
            require_once $classNamePHP;
        });
    }

    private function classNamePHPAux($arr)
    {
        $resp="";
        $len=count($arr);
        if ($len==2)
            $resp=$arr[1]."/";
        else if($len==3) {
            $resp=$arr[1]."/".$arr[2]."/";
        } else if ($len==4){
            //for ($i=1;$i<$len;$i++)
                //$resp.=$arr[$i]."/";
            $resp.=$arr[1]."/".$arr[2]."/".$arr[3]."/";
            //return substr($resp, 0,strlen($resp) -1);

        }

        
        //for ($i=1;$i<$len;$i++)
        //   $resp.=$arr[$i]."/";
        //return substr($resp, 0,strlen($resp) -1);
        return $resp;
    }

    private function classNamePHP($aNameSpaceClass)
    {
        //$len=count($aNameSpaceClass);
        //echo $len;die();//.' '.var_dump($aNameSpaceClass);die();
        $a=self::classNamePHPAux($aNameSpaceClass);       
        return '../../../'.$_SESSION[$aNameSpaceClass[0]].'/class/'.$a.end($aNameSpaceClass).'.php';


        //return '../../../'.$_SESSION[$aNameSpaceClass[0]].'/class/'.$a.end($aNameSpaceClass).'.php';
        //return '../../../'.$_SESSION[$aNameSpaceClass[0]].'/class/'.$aNameSpaceClass[1].'/'.end($aNameSpaceClass).'.php';
    } 
}

